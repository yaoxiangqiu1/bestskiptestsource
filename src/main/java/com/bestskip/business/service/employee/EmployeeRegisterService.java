package com.bestskip.business.web.service.employee;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bestskip.business.web.bean.employeeManager.EmployeeSearchBean;
import com.bestskip.business.web.bean.employeeManager.EmployeeSearchResultBean;
import com.bestskip.business.web.bean.fileManager.FileInfo;
import com.bestskip.business.web.bean.user.UserInfo;
import com.bestskip.business.web.repository.model.MShainKihonJoho;
import com.bestskip.business.web.repository.model.MShainKoyokeiyakuJoho;

public interface EmployeeRegisterService {

    boolean saveEmployeeInfo(List<FileInfo> multipartFiles,
                             MShainKihonJoho mShainKihonJoho,
                             MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho,
                             UserInfo userInfo,
                             String keiyakuFlg,
                             Date beforeTekiyoEndDay,
                             String status,
                             String changeText) throws Exception;
    boolean checkName(String shainNo, String sei, String mei);

    MShainKihonJoho getShinKihonJoho(String shainNo);

    MShainKoyokeiyakuJoho getShinKoyokeiyakuJoho(String shainNo);

    MShainKoyokeiyakuJoho getMaeShainKoyokeiyakuJoho(String bpShainNo);

    List<EmployeeSearchResultBean> getEmployeeList(EmployeeSearchBean searchBean,
                                                   UserInfo userInfo);

    List<Map<String, Object>> getMShainKoyokeiyakuJoho(String shainNo);

    Map<String, Object> getYosanJisekiCount(String shainNo, Date tekiyoStartDay);
}
