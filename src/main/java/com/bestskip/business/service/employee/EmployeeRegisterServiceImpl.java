package com.bestskip.business.web.service.employee;

import java.util.*;

import com.bestskip.business.web.bean.employeeManager.EmployeeChangeInfoBean;
import com.bestskip.business.web.bean.employeeManager.EmployeeSearchBean;
import com.bestskip.business.web.bean.employeeManager.EmployeeSearchResultBean;
import com.bestskip.business.web.bean.fileManager.FileInfo;
import com.bestskip.business.web.common.*;
import com.bestskip.business.web.form.ordersResults.OrdersResultsRegisterForm;
import com.bestskip.business.web.mapper.employee.EmployeeMapper;
import com.bestskip.business.web.repository.mapper.*;
import com.bestskip.business.web.repository.model.*;
import com.bestskip.business.web.service.common.CommonService;
import com.bestskip.business.web.util.FileUploadDownload;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import com.bestskip.business.web.base.SqlMapper;
import com.bestskip.business.web.bean.user.UserInfo;
import org.springframework.util.StringUtils;

@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeRegisterServiceImpl implements EmployeeRegisterService {

	@Autowired
	private MShainKihonJohoMapper mShainKihonJohoMapper;
	@Autowired
	private MShainKoyokeiyakuJohoMapper mShainKoyokeiyakuJohoMapper;
	@Autowired
	private TKuuyoinjohoMapper tKuuyoinjohoMapper;
	@Autowired
	private MCodeMapper mCodeMapper;
	@Autowired
	private SqlMapper sqlMapper;
	@Autowired
	private EmployeeMapper employeeMapper;
	@Autowired
	private TOshiraseKanriMapper tOshiraseKanriMapper;
	@Autowired
	private TUploadInfoKanriMapper tUploadInfoKanriMapper;
	@Autowired
	private TShainAnkenkanrenJohoMapper tShainAnkenkanrenJohoMapper;
	@Autowired
	private SendMail sendMail;
	@Autowired
	private CommonService commonService;

	@Value("${soumu.mail.address}")
	private String MAILADDRESS_SOUMU;

	@Value("${eigyo.mail.address}")
	private String MAILADDRESS_EIGYO;
	/**
	 * 社員情報保存処理
	 * */
	public boolean saveEmployeeInfo(List<FileInfo> multipartFiles,
									MShainKihonJoho mShainKihonJoho,
									MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho,
									UserInfo userInfo,
									String keiyakuFlg,
									Date beforeTekiyoEndDay,
									String status,
									String changeText) throws BusinessException {
		Date now = new Date();

		// 社員No存在する場合(社員情報更新)
		if (!StringUtils.isEmpty(mShainKihonJoho.getShainNo())) {

			// アップロードファイル存在する場合
			if (multipartFiles.size() > 0) {
				String uploadCode = Constants.STRING_EMPTY;
				MShainKihonJoho oldShainKihonJoho = mShainKihonJohoMapper.selectByPrimaryKey(mShainKihonJoho.getShainNo());
				if (oldShainKihonJoho != null) {
					uploadCode = oldShainKihonJoho.getShoruiPathCode();
				}

				// アップロードコードを取得する
				uploadCode = getUploadCode(uploadCode);
				// ファイルアップロード処理(複数)
				uploadFiles(multipartFiles, uploadCode, userInfo.getId());

				// アップロードコード
				mShainKihonJoho.setShoruiPathCode(uploadCode);
			}

			// 更新条件設定
			MShainKihonJohoExample example = new MShainKihonJohoExample();
			MShainKihonJohoExample.Criteria criteria = example.createCriteria();
			criteria.andShainNoEqualTo(mShainKihonJoho.getShainNo());
			criteria.andKoshinKaisuEqualTo(mShainKihonJoho.getKoshinKaisu());

			// 更新回数
			mShainKihonJoho.setKoshinKaisu(mShainKihonJoho.getKoshinKaisu() + 1);
			mShainKihonJoho.setKoshimbi(now);
			mShainKihonJoho.setKoshinsha(userInfo.getId());

			// 社員情報を更新する
			int updateCount = mShainKihonJohoMapper.updateByExampleSelective(mShainKihonJoho, example);
			if (updateCount == 0) {
				throw new BusinessException(Constants.ECP_01, "");
			}

			// 社員雇用契約情報の更新
			// 契約内容変更による期間再設定の場合
			if (Constants.KEIYAKU_KIKAN_UPDATE.equals(keiyakuFlg)) {

				// 変更有の場合
				if (Constants.STATUS_HAS_CHANGE.equals(status)) {
					// 該当更新
					MShainKoyokeiyakuJoho oldShainKoyokeiyakuJoho;
					MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
					mShainKoyokeiyakuJohoKey.setShainNo(mShainKoyokeiyakuJoho.getShainNo());
					mShainKoyokeiyakuJohoKey.setEdaban(mShainKoyokeiyakuJoho.getEdaban());
					oldShainKoyokeiyakuJoho = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);

					// 退職の場合
					if (Constants.STATUS_SHUGYO_TAISHOKU.equals(mShainKoyokeiyakuJoho.getShugyoJokyoFlag())) {
						// 新規作成しない
						mShainKoyokeiyakuJoho.setTekiyoStartDay(oldShainKoyokeiyakuJoho.getTekiyoStartDay());
						mShainKoyokeiyakuJoho.setTekiyoEndDay(mShainKihonJoho.getTaishokuDay());

						// 前の契約情報更新処理
						updateKoyokeiyakuJoho(mShainKoyokeiyakuJoho, userInfo.getId());
					} else {
						oldShainKoyokeiyakuJoho.setTekiyoEndDay(beforeTekiyoEndDay);

						// 前の契約情報更新処理
						updateKoyokeiyakuJoho(oldShainKoyokeiyakuJoho, userInfo.getId());
					}

					//退職の場合、空要員情報をクリア
					if (Constants.STATUS_SHUGYO_TAISHOKU.equals(mShainKoyokeiyakuJoho.getShugyoJokyoFlag())){
						TKuuyoinjohoExample example2 = new TKuuyoinjohoExample();
						TKuuyoinjohoExample.Criteria criteria2 = example2.createCriteria();
						criteria2.andYoinnNoEqualTo(mShainKoyokeiyakuJoho.getShainNo());
						tKuuyoinjohoMapper.deleteByExample(example2);
						sendShainInfoMail(userInfo, mShainKoyokeiyakuJoho,mShainKihonJoho,"1");
						return true;

					} else {
						// 空要員情報作成
						TKuuyoinjoho tKuuyoinjoho = new TKuuyoinjoho();
						TKuuyoinjohoKey tKuuyoinjohoKey = new TKuuyoinjohoKey();
						tKuuyoinjohoKey.setYoinnNo(mShainKoyokeiyakuJoho.getShainNo());
						tKuuyoinjohoKey.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
						tKuuyoinjoho = tKuuyoinjohoMapper.selectByPrimaryKey(tKuuyoinjohoKey);

						if(tKuuyoinjoho == null){
							tKuuyoinjoho = new TKuuyoinjoho();
							tKuuyoinjoho.setYoinnNo(mShainKoyokeiyakuJoho.getShainNo());
							tKuuyoinjoho.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
							tKuuyoinjoho.setYoinKbn(mShainKoyokeiyakuJoho.getKoyokeitai());
							tKuuyoinjoho.setShozokuGaishaCode(Constants.JISHA_KAISHA_CD);
							tKuuyoinjoho.setYoteiSankakuStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
							tKuuyoinjoho.setKakuhojokyoFlag(Constants.STATUS_KAKUHO_SUMI);
							tKuuyoinjoho.setKeyakuEdaban(mShainKoyokeiyakuJoho.getEdaban());
							tKuuyoinjoho.setEigyoKeikaJokyo("");
							tKuuyoinjoho.setKoshinKaisu(0L);
							tKuuyoinjoho.setTorokubi(now);
							tKuuyoinjoho.setTorokusha(userInfo.getId());
							tKuuyoinjohoMapper.insert(tKuuyoinjoho);
						} else {

							tKuuyoinjoho.setKeyakuEdaban(mShainKoyokeiyakuJoho.getEdaban());
							tKuuyoinjoho.setYoteiSankakuStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
							tKuuyoinjoho.setKakuhojokyoFlag(Constants.STATUS_KAKUHO_SUMI);
							tKuuyoinjoho.setYoinKbn(mShainKoyokeiyakuJoho.getKoyokeitai());
							tKuuyoinjoho.setKoshinKaisu(tKuuyoinjoho.getKoshinKaisu()+ 1L);
							tKuuyoinjoho.setKoshimbi(now);
							tKuuyoinjoho.setKoshinsha(userInfo.getId());
							tKuuyoinjohoMapper.updateByPrimaryKey(tKuuyoinjoho);
						}
					}

					// 新規追加
					short edaban = getMaxEdaban(mShainKihonJoho.getShainNo());

					mShainKoyokeiyakuJoho.setEdaban((short) (edaban + 1));
					mShainKoyokeiyakuJoho.setTetsudukiHenkoFlag("1");

					//更新回数、更新者、更新日時を設定
					mShainKoyokeiyakuJoho.setKoshinKaisu(0L);
					mShainKoyokeiyakuJoho.setTorokubi(now);
					mShainKoyokeiyakuJoho.setTorokusha(userInfo.getId());

					// 精算無し場合
					if(Constants.TANKA_KBN_JIKAN.equals(mShainKoyokeiyakuJoho.getTankaKbn())
							|| "2".equals(mShainKoyokeiyakuJoho.getSeisanFlag())) {
						mShainKoyokeiyakuJoho.setSeisanjikanEnd(null);
						mShainKoyokeiyakuJoho.setSeisanjikanStart(null);
						mShainKoyokeiyakuJoho.setSeisanKaitannka(null);
						mShainKoyokeiyakuJoho.setSeisanJouitannka(null);
					}

					//社員雇用契約情報を登録する
					mShainKoyokeiyakuJohoMapper.insert(mShainKoyokeiyakuJoho);

					//退職、休職又は復帰の場合、関係者にメールを送信する
					if(Constants.STATUS_SHUGYO_TAISHOKU.equals(mShainKoyokeiyakuJoho.getShugyoJokyoFlag())
							|| (!Constants.STATUS_SHUGYO_ZAISHOKU.equals(oldShainKoyokeiyakuJoho.getShugyoJokyoFlag())
							&& Constants.STATUS_SHUGYO_ZAISHOKU.equals(mShainKoyokeiyakuJoho.getShugyoJokyoFlag()))
							|| "1".equals(mShainKoyokeiyakuJoho.getShugyoJokyoFlag())) {
						sendShainInfoMail(userInfo,mShainKoyokeiyakuJoho,mShainKihonJoho,"1");
					}else{
						//それ以外の項目値の変更ある場合
						if (!StringUtils.isEmpty(changeText)){
							sendShainChangeInfoMail(userInfo,oldShainKoyokeiyakuJoho,mShainKoyokeiyakuJoho,mShainKihonJoho);
						}
					}
					// 変更なし（レコードあり）
				} else if (Constants.STATUS_NO_CHANGE_DATA.equals(status)) {
					// 空要員情報更新対象
					updateKuuyoinjoho(
							mShainKihonJoho.getShainNo(),
							userInfo.getId(),
							mShainKoyokeiyakuJoho);

					// 適用開始日が入力する場合更新
					if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getTekiyoStartDay())) {

						// 前のレコードを更新
						// 契約情報の前のレコードを取得
						MShainKoyokeiyakuJoho mMaeShainKoyokeiyakuJoho = this.getMaeShainKoyokeiyakuJoho(mShainKihonJoho.getShainNo());

						// 個別項目設定
						mMaeShainKoyokeiyakuJoho.setTekiyoEndDay(beforeTekiyoEndDay);

						// 前の契約情報更新処理
						updateKoyokeiyakuJoho(mMaeShainKoyokeiyakuJoho, userInfo.getId());

						// 現在のレコードを更新
						// 契約情報更新処理
						updateKoyokeiyakuJoho(mShainKoyokeiyakuJoho, userInfo.getId());
					}

					// 変更なし（レコードなし）
				} else if (Constants.STATUS_NO_CHANGE_NO_DATA.equals(status)) {

					// 適用開始日が入力する場合更新
					if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getTekiyoStartDay())) {

						// 空要員情報更新対象
						updateKuuyoinjoho(
								mShainKihonJoho.getShainNo(),
								userInfo.getId(),
								mShainKoyokeiyakuJoho);
						// 現在のレコードを更新
						// 契約情報更新処理
						updateKoyokeiyakuJoho(mShainKoyokeiyakuJoho, userInfo.getId());
					}
				}
				// 期間の内容変更の場合(登録ミスの訂正)
			} else if (Constants.KEIYAKU_NAIYOU_UPDATE.equals(keiyakuFlg)) {

				// 雇用形態が変更した場合、空要員情報に更新
				if(changeText.contains("雇用形態")) {

					MShainKoyokeiyakuJoho oldShainKoyokeiyakuJoho;
					MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
					mShainKoyokeiyakuJohoKey.setShainNo(mShainKoyokeiyakuJoho.getShainNo());
					mShainKoyokeiyakuJohoKey.setEdaban(mShainKoyokeiyakuJoho.getEdaban());
					oldShainKoyokeiyakuJoho = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);
					mShainKoyokeiyakuJoho.setTekiyoStartDay(oldShainKoyokeiyakuJoho.getTekiyoStartDay());

					TKuuyoinjoho kuuyoinjoho = new TKuuyoinjoho();
					TKuuyoinjohoKey key = new TKuuyoinjohoKey();
					key.setYoinnNo(mShainKoyokeiyakuJoho.getShainNo());
					key.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
					kuuyoinjoho = tKuuyoinjohoMapper.selectByPrimaryKey(key);
					if(kuuyoinjoho != null){
						kuuyoinjoho.setYoinKbn(mShainKoyokeiyakuJoho.getKoyokeitai());
						kuuyoinjoho.setKoshimbi(now);
						kuuyoinjoho.setKoshinsha(userInfo.getId());
						kuuyoinjoho.setKoshinKaisu(kuuyoinjoho.getKoshinKaisu() + 1);
						updateCount = tKuuyoinjohoMapper.updateByPrimaryKey(kuuyoinjoho);
					}
				}

				// 適用日更新しないようにNULLを設定
				mShainKoyokeiyakuJoho.setTekiyoStartDay(null);
				mShainKoyokeiyakuJoho.setTekiyoEndDay(null);

				// 契約情報更新処理
				updateKoyokeiyakuJoho(mShainKoyokeiyakuJoho, userInfo.getId());

				// 精算無し場合
				if(Constants.TANKA_KBN_JIKAN.equals(mShainKoyokeiyakuJoho.getTankaKbn())
						|| "2".equals(mShainKoyokeiyakuJoho.getSeisanFlag())) {
					MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho2;
					MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
					mShainKoyokeiyakuJohoKey.setShainNo(mShainKoyokeiyakuJoho.getShainNo());
					mShainKoyokeiyakuJohoKey.setEdaban(mShainKoyokeiyakuJoho.getEdaban());
					mShainKoyokeiyakuJoho2 = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);
					mShainKoyokeiyakuJoho2.setSeisanjikanEnd(null);
					mShainKoyokeiyakuJoho2.setSeisanjikanStart(null);
					mShainKoyokeiyakuJoho2.setSeisanKaitannka(null);
					mShainKoyokeiyakuJoho2.setSeisanJouitannka(null);
					mShainKoyokeiyakuJohoMapper.updateByPrimaryKey(mShainKoyokeiyakuJoho2);
				}
				// 変更無しの場合
			} else {
				return true;
			}

			// 契約情報変更成功の場合、お知らせに登録
			if (!StringUtils.isEmpty(changeText)) {

				// お知らせシーケンスコードを発番
				Map<String, Object> rest = sqlMapper.find("select nextval('oshirase_no_seq')");
				// シーケンス番号取得
				int seq = Integer.parseInt(rest.get("nextval").toString());
				String oshiraseNo = Constants.OSHIRASE_NO + String.format("%013d", seq);
				TOshiraseKanri tOshiraseKanri = new TOshiraseKanri();
				tOshiraseKanri.setOshiraseNo(oshiraseNo);
				tOshiraseKanri.setOshiraseKbn(Constants.OSHIRASE_KBN4);
				tOshiraseKanri.setTekiyoStartDay(now);
				tOshiraseKanri.setTekiyoEndDay(DateUtils.addMonths(now, 1));
				tOshiraseKanri.setMotoShokushuFlag(userInfo.getShokushuFlag());
				//総務or営業部下→営業上司→営業上司の上司
				String shokushuList = Constants.SHOKUSHU_CD_JINJI+","+Constants.SHOKUSHU_CD_EIGYO;
				tOshiraseKanri.setSakiShokushuFlagList(shokushuList);
				tOshiraseKanri.setSakiSoshikiCode(userInfo.getSoshikiCode());
				//お知らせ内容
				tOshiraseKanri.setOshiraseNaiyou("社員名："+mShainKihonJoho.getSeiKanji()+mShainKihonJoho.getMeiKanji()+changeText);
				tOshiraseKanri.setOshiraseKey1(mShainKihonJoho.getShainNo());
				tOshiraseKanri.setOshiraseKey2("");
				tOshiraseKanri.setJuyodo("1");
				tOshiraseKanri.setTorokubi(now);
				tOshiraseKanri.setTorokusha(userInfo.getId());
				//営業or管理職or経営職→総務or営業or管理職or経営職のお知らせ登録
				tOshiraseKanriMapper.insert(tOshiraseKanri);

				//見積原価が変更した場合、受発注予算管理の該当する原価を更新する
				if(changeText.contains("見積原価変更") || changeText.contains("単価区分")) {

					// 契約内容変更の時、適用開始日を再取得必要
					if(Constants.KEIYAKU_NAIYOU_UPDATE.equals(keiyakuFlg)) {
						MShainKoyokeiyakuJoho oldShainKoyokeiyakuJoho;
						MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
						mShainKoyokeiyakuJohoKey.setShainNo(mShainKoyokeiyakuJoho.getShainNo());
						mShainKoyokeiyakuJohoKey.setEdaban(mShainKoyokeiyakuJoho.getEdaban());
						oldShainKoyokeiyakuJoho = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);
						mShainKoyokeiyakuJoho.setTekiyoStartDay(oldShainKoyokeiyakuJoho.getTekiyoStartDay());
						mShainKoyokeiyakuJoho.setTekiyoEndDay(oldShainKoyokeiyakuJoho.getTekiyoEndDay());
					}

					// 受発注予算管理の原価単価を更新する
					updateYosankanri(mShainKoyokeiyakuJoho, userInfo);
				}
			}

			// 社員No存在しない場合（新規社員）
		} else {

			// アップロードファイル存在する場合
			if (multipartFiles.size() > 0) {
				// アップロードコードを取得する
				String uploadCode = getUploadCode(mShainKihonJoho.getShoruiPathCode());
				// ファイルアップロード処理(複数)
				uploadFiles(multipartFiles, uploadCode, userInfo.getId());
				// アップロードコード
				mShainKihonJoho.setShoruiPathCode(uploadCode);
			}

			// シーケンスコードを発番
			Map<String, Object> row = sqlMapper.find("select nextval('shain_no_seq')");
			// シーケンス番号取得
			int index = Integer.parseInt(row.get("nextval").toString());
			//	    社員No生成
			String shainNo = Constants.SHAIN_NO + String.format("%05d", index);
			// 更新回数、登録者、登録日時、更新者、更新日時を設定

			// 社員基本情報の作成
			mShainKihonJoho.setShainNo(shainNo);
			mShainKihonJoho.setShozokuGaishaCode(Constants.JISHA_KAISHA_CD);
			mShainKihonJoho.setKoshinKaisu(0L);
			mShainKihonJoho.setTorokubi(now);
			mShainKihonJoho.setTorokusha(userInfo.getId());

			//初期パスワード設定
			String ankoPw ="";
			try {
				ankoPw = CipherManager.encrypt(Constants.PASSWORD, Constants.KEY, Constants.ALGORITHM);
			}catch (Exception e){
				return false;
			}

			mShainKihonJoho.setPassword(ankoPw);
			mShainKihonJohoMapper.insert(mShainKihonJoho);

			// 社員雇用契約情報の作成
			mShainKoyokeiyakuJoho.setShainNo(shainNo);
			mShainKoyokeiyakuJoho.setEdaban(Short.valueOf("1"));
			mShainKoyokeiyakuJoho.setShugyoJokyoFlag(Constants.STATUS_SHUGYO_ZAISHOKU);
			mShainKoyokeiyakuJoho.setTetsudukiHenkoFlag("1");
			mShainKoyokeiyakuJoho.setKoshinKaisu(0L);
			mShainKoyokeiyakuJoho.setTorokubi(now);
			mShainKoyokeiyakuJoho.setTorokusha(userInfo.getId());

			// 精算無し場合
			if(Constants.TANKA_KBN_JIKAN.equals(mShainKoyokeiyakuJoho.getTankaKbn())
					|| "2".equals(mShainKoyokeiyakuJoho.getSeisanFlag())) {
				mShainKoyokeiyakuJoho.setSeisanjikanEnd(null);
				mShainKoyokeiyakuJoho.setSeisanjikanStart(null);
				mShainKoyokeiyakuJoho.setSeisanKaitannka(null);
				mShainKoyokeiyakuJoho.setSeisanJouitannka(null);
			}

			mShainKoyokeiyakuJohoMapper.insert(mShainKoyokeiyakuJoho);
			//新入社員の場合
			sendShainInfoMail(userInfo,mShainKoyokeiyakuJoho,mShainKihonJoho,"2");
			// 空要員情報作成
			TKuuyoinjoho tKuuyoinjoho = new TKuuyoinjoho();
			tKuuyoinjoho.setYoinnNo(shainNo);
			tKuuyoinjoho.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
			tKuuyoinjoho.setYoinKbn(mShainKoyokeiyakuJoho.getKoyokeitai());
			tKuuyoinjoho.setShozokuGaishaCode(Constants.JISHA_KAISHA_CD);
			tKuuyoinjoho.setYoteiSankakuStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
			tKuuyoinjoho.setKakuhojokyoFlag(Constants.STATUS_KAKUHO_SUMI);
			tKuuyoinjoho.setKeyakuEdaban(mShainKoyokeiyakuJoho.getEdaban());
			tKuuyoinjoho.setEigyoKeikaJokyo("");
			tKuuyoinjoho.setKoshinKaisu(0L);
			tKuuyoinjoho.setTorokubi(now);
			tKuuyoinjoho.setTorokusha(userInfo.getId());
			tKuuyoinjohoMapper.insert(tKuuyoinjoho);

			//お知らせ登録
			// お知らせシーケンスコードを発番
			Map<String, Object> rest = sqlMapper.find("select nextval('oshirase_no_seq')");
			// シーケンス番号取得
			int seq = Integer.parseInt(rest.get("nextval").toString());
			String oshiraseNo = Constants.OSHIRASE_NO + String.format("%013d", seq);
			TOshiraseKanri tOshiraseKanri = new TOshiraseKanri();
			tOshiraseKanri.setOshiraseNo(oshiraseNo);
			tOshiraseKanri.setOshiraseKbn(Constants.OSHIRASE_KBN1);
			tOshiraseKanri.setTekiyoStartDay(now);
			tOshiraseKanri.setTekiyoEndDay(DateUtils.addMonths(now, 1));
			tOshiraseKanri.setMotoShokushuFlag(userInfo.getShokushuFlag());
			//総務or営業or管理職or経営職
			String shokushuList = Constants.SHOKUSHU_CD_JINJI+","+Constants.SHOKUSHU_CD_EIGYO+","+
					Constants.SHOKUSHU_CD_KANLI+","+Constants.SHOKUSHU_CD_KEIEI;
			tOshiraseKanri.setSakiShokushuFlagList(shokushuList);
			tOshiraseKanri.setSakiSoshikiCode(userInfo.getSoshikiCode());
			//お知らせ内容
			tOshiraseKanri.setOshiraseNaiyou("社員名："+mShainKihonJoho.getSeiKanji()+mShainKihonJoho.getMeiKanji()+"は新規登録しました。");
			tOshiraseKanri.setOshiraseKey1(mShainKihonJoho.getShainNo());
			tOshiraseKanri.setOshiraseKey2("");
			tOshiraseKanri.setJuyodo("1");
			tOshiraseKanri.setTorokubi(now);
			tOshiraseKanri.setTorokusha(userInfo.getId());
			//営業or管理職or経営職→総務or営業or管理職or経営職のお知らせ登録
			tOshiraseKanriMapper.insert(tOshiraseKanri);
		}

		return true;
	}

	/**
	 * 社員情報一覧取得処理
	 * */
	public List<EmployeeSearchResultBean> getEmployeeList(EmployeeSearchBean searchBean,
														  UserInfo userInfo) {
		searchBean.setSoshikiCd(userInfo.getSoshikiCode());
		//役職コード設定
		searchBean.setYakushokuCd(userInfo.getYakushokuCd());
		//職種設定
		searchBean.setShokushuFlag(userInfo.getShokushuFlag());
		//役職レベル取得
		MCodeKey key = new MCodeKey();
		key.setEdaban(userInfo.getYakushokuCd());
		key.setCodeId(Constants.CD_C0010);
		MCode mCode = mCodeMapper.selectByPrimaryKey(key);
		//役職レベル設定
		if (mCode != null){
			searchBean.setYakushokuLever(mCode.getHyoujijun());
		}
		return employeeMapper.getEmployeeList(searchBean);
	}

	/**
	 * 社員基本情報取得処理
	 * */
	public MShainKihonJoho getShinKihonJoho(String shainNo){
		MShainKihonJoho mShainKihonJoho;
		mShainKihonJoho =mShainKihonJohoMapper.selectByPrimaryKey(shainNo);
		return mShainKihonJoho;
	}

	/**
	 * 社員契約情報情報取得処理
	 * */
	public MShainKoyokeiyakuJoho getShinKoyokeiyakuJoho(String shainNo) {

		// 新規追加
		short edaban = getMaxEdabanByTekiyoStartDay(shainNo);
		MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho;
		MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
		mShainKoyokeiyakuJohoKey.setShainNo(shainNo);
		mShainKoyokeiyakuJohoKey.setEdaban(edaban);
		mShainKoyokeiyakuJoho = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);

		return mShainKoyokeiyakuJoho;
	}

	/**
	 * 契約情報の前のレコードを取得
	 * */
	public MShainKoyokeiyakuJoho getMaeShainKoyokeiyakuJoho(String ShainNo) {

		MShainKoyokeiyakuJohoExample koyouExample = new MShainKoyokeiyakuJohoExample();
		MShainKoyokeiyakuJohoExample.Criteria koyouCriteria = koyouExample.createCriteria();
		koyouCriteria.andShainNoEqualTo(ShainNo);
		koyouExample.setOrderByClause("tekiyo_start_day desc");

		MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho;

		List<MShainKoyokeiyakuJoho> mShainKoyokeiyakulist = mShainKoyokeiyakuJohoMapper.selectByExample(koyouExample);

		if (mShainKoyokeiyakulist != null
				&& mShainKoyokeiyakulist.size() > 1) {
			mShainKoyokeiyakuJoho = mShainKoyokeiyakulist.get(1);
		} else {
			mShainKoyokeiyakuJoho = null;
		}

		return mShainKoyokeiyakuJoho;
	}

	/**
	 * 社員名重複チェック
	 * */
	public boolean checkName(String shainNo, String sei, String mei){
		List<MShainKihonJoho> mShainKihonJoho = new ArrayList<MShainKihonJoho>();
		MShainKihonJohoExample example = new MShainKihonJohoExample();
		MShainKihonJohoExample.Criteria criteria = example.createCriteria();
		criteria.andSeiKanjiEqualTo(sei.trim());
		criteria.andMeiKanjiEqualTo(mei.trim());
		if(shainNo != null && !shainNo.isEmpty()){
			criteria.andShainNoNotEqualTo(shainNo);
		}
		mShainKihonJoho = mShainKihonJohoMapper.selectByExample(example);
		return mShainKihonJoho.size() == 0;
	}
	private short getMaxEdaban(String shainNo) {
		// 新規追加
		String sql = "select max(edaban) as maxedaban from m_shain_koyokeiyaku_joho where shain_no = '"
				+ shainNo + "'";
		Map<String, Object> rset  = sqlMapper.find(sql);

		short edaban = 0;
		if (rset != null && rset.get("maxedaban") != null) {
			// 枝番取得
			edaban = Short.parseShort(rset.get("maxedaban").toString());
		}

		return edaban;
	}

	private short getMaxEdabanByTekiyoStartDay(String shainNo) {
		// 新規追加
		String sql = "select max(edaban) as maxedaban " +
				" from m_shain_koyokeiyaku_joho where shain_no = '" + shainNo + "'" +
				" and tekiyo_start_day = ( select max(tekiyo_start_day) from m_shain_koyokeiyaku_joho where shain_no = '" + shainNo + "')";
		Map<String, Object> rset  = sqlMapper.find(sql);

		short edaban = 0;
		if (rset != null && rset.get("maxedaban") != null) {
			// 枝番取得
			edaban = Short.parseShort(rset.get("maxedaban").toString());
		}

		return edaban;
	}

	/**
	 * 受発注予算管理の原価単価を更新する
	 * */
	private  boolean updateYosankanri(MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho, UserInfo userInfo) {

		Date nowDate = new Date();

		//適用開始日
		String startDay = Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay());
		//適用終了日
		String endDay = mShainKoyokeiyakuJoho.getTekiyoEndDay() == null? null:
				Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoEndDay());
		//見積原価
		Long genka = mShainKoyokeiyakuJoho.getMitsumoriGenka();
		//単価区分
		String tankaKbn = mShainKoyokeiyakuJoho.getTankaKbn();

		Map<String, Object> params = new HashMap<>();

		String sql =
				"DELETE\n" +
						"FROM\n" +
						"    t_juchukou_yosan_kanri\n" +
						"WHERE\n" +
						"    yoin_no = #{params.yoin_no}\n" +
						"AND sagyo_nengetsu >= #{params.startDay}\n";

		params.put("tankaKbn", tankaKbn);
		params.put("gennka_tanka", genka);
		params.put("koshimbi", nowDate);
		params.put("koshinsha", userInfo.getId());
		params.put("yoin_no", mShainKoyokeiyakuJoho.getShainNo());
		params.put("startDay", startDay);

		if(!StringUtils.isEmpty(endDay)) {
			sql += "AND sagyo_nengetsu <= #{params.endDay} ";
			params.put("endDay", endDay);
		}

		sqlMapper.update(sql, params);
		return true;
	}

	/**
	 * アップロードコードを取得する
	 * */
	private String getUploadCode(String uploadCode) {
		// 新規追加
		String resultUploadCode;

		if (StringUtils.isEmpty(uploadCode)) {
			// アップロードコードを発番
			Map<String, Object> rest = sqlMapper.find("select nextval('upload_code_seq')");
			// シーケンス番号取得
			int seq = Integer.parseInt(rest.get("nextval").toString());
			resultUploadCode = Constants.UPLOAD_NO + String.format("%013d", seq);
		} else {
			resultUploadCode = uploadCode;
		}

		return resultUploadCode;
	}

	/**
	 * アップロード最大枝番を取得する
	 * */
	private short getUploadMaxEdaban(String uploadCode) {
		// 新規追加
		String sql = "select max(edaban) as maxedaban from t_upload_info_kanri where upload_code = '"
				+ uploadCode + "'";
		Map<String, Object> rset  = sqlMapper.find(sql);

		short edaban;
		if (rset != null && rset.get("maxedaban") != null) {
			// 枝番取得
			edaban = (short) (Short.parseShort(rset.get("maxedaban").toString()) + 1);
		} else {
			edaban = 1;
		}

		return edaban;
	}

	/**
	 * ファイルアップロード処理(複数)
	 * */
	private void uploadFiles(List<FileInfo> multipartFiles, String uploadCode, String userId) {
		// エラーメッセージ
		String errMsg = Constants.STRING_EMPTY;
		List<String> errMsgList = new ArrayList<>();
		// フォルダー作成処理
		FileUploadDownload.createDirectory(Constants.RIREKI_UPLOADED_FOLDER);

		for (FileInfo file : multipartFiles) {
			// 選択する場合
			if (!StringUtils.isEmpty(file.getName())) {
				short edaban = getUploadMaxEdaban(uploadCode);
				String fileName = FileUploadDownload.getUploadFileName(uploadCode, edaban, file.getName());
				errMsg = FileUploadDownload.uploadFile(file, Constants.RIREKI_UPLOADED_FOLDER, fileName);

				// 正常アップロードする場合
				if (StringUtils.isEmpty(errMsg)) {
					// ファイルアップロード情報登録処理
					createTUploadInfoKanri(uploadCode, edaban, fileName, userId);
				} else {
					errMsgList.add(errMsg);
				}
			}
		}

		// エラーある場合
		if (errMsgList.size() > 0) {
			errMsg = String.join("<br />", errMsgList);
			throw new BusinessException(Constants.ECP_02, errMsg);
		}
	}

	/**
	 * ファイルアップロード情報登録処理
	 * */
	private void createTUploadInfoKanri(
			String uploadCode,
			short edaban,
			String fileName,
			String userId) {

		// システム日時取得
		Date now = new Date();

		TUploadInfoKanri tUploadInfoKanri = new TUploadInfoKanri();

		// ファイル情報
		tUploadInfoKanri.setUploadCode(uploadCode);
		tUploadInfoKanri.setEdaban(edaban);
		tUploadInfoKanri.setFileName(fileName);
		tUploadInfoKanri.setFilePath(Constants.RIREKI_UPLOADED_FOLDER);

		//更新回数、更新者、更新日時を設定
		tUploadInfoKanri.setKoshinKaisu(0L);
		tUploadInfoKanri.setTorokubi(now);
		tUploadInfoKanri.setTorokusha(userId);

		tUploadInfoKanriMapper.insert(tUploadInfoKanri);
	}

	/**
	 * 契約情報更新処理
	 * */
	private void updateKoyokeiyakuJoho(MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho, String userId) {

		// システム日時取得
		Date now = new Date();

		mShainKoyokeiyakuJoho.setKoshimbi(now);
		mShainKoyokeiyakuJoho.setKoshinsha(userId);

		// 更新条件設定
		MShainKoyokeiyakuJohoExample koyouExample = new MShainKoyokeiyakuJohoExample();
		MShainKoyokeiyakuJohoExample.Criteria koyouCriteria = koyouExample.createCriteria();
		koyouCriteria.andShainNoEqualTo(mShainKoyokeiyakuJoho.getShainNo());
		koyouCriteria.andEdabanEqualTo(mShainKoyokeiyakuJoho.getEdaban());
		koyouCriteria.andKoshinKaisuEqualTo(mShainKoyokeiyakuJoho.getKoshinKaisu());

		// 更新回数
		mShainKoyokeiyakuJoho.setKoshinKaisu(mShainKoyokeiyakuJoho.getKoshinKaisu() + 1);
		// 社員情報を更新する
		int updateNewKoyouCount = mShainKoyokeiyakuJohoMapper.updateByExampleSelective(mShainKoyokeiyakuJoho, koyouExample);
		// 更新失敗の場合
		if (updateNewKoyouCount == 0) {
			throw new BusinessException(Constants.ECP_01, "");
		}
	}

	/**
	 * 空要員情報を取得処理
	 * */
	private TKuuyoinjoho getTKuuyoinjoho(String bpShainNo,String startMonth) {
		TKuuyoinjohoKey key = new TKuuyoinjohoKey();
		key.setYoinnNo(bpShainNo);
		key.setKuuyoteiStartMonth(startMonth);
		return tKuuyoinjohoMapper.selectByPrimaryKey(key);
	}

	/**
	 * 社員案件関連情報を取得処理
	 * */
	private List<TShainAnkenkanrenJoho> getTShainAnkenkanrenJohoMapper(String ShainNo,String startMonth) {

		TShainAnkenkanrenJohoExample tShainAnkenkanrenJohoExample = new TShainAnkenkanrenJohoExample();
		TShainAnkenkanrenJohoExample.Criteria criteria = tShainAnkenkanrenJohoExample.createCriteria();
		criteria.andShainNoEqualTo(ShainNo);
		criteria.andKuuyoteiStartMonthEqualTo(startMonth);
		return tShainAnkenkanrenJohoMapper.selectByExample(tShainAnkenkanrenJohoExample);
	}

	/**
	 * 空要員情報更新
	 * */
	private void updateKuuyoinjoho(String shainNo, String userId, MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho) {

		// システム日時取得
		Date now = new Date();
		int updateCount = 0;
		//元の契約情報を取得
		MShainKoyokeiyakuJoho oldShainKoyokeiyakuJoho;
		MShainKoyokeiyakuJohoKey mShainKoyokeiyakuJohoKey = new MShainKoyokeiyakuJohoKey();
		mShainKoyokeiyakuJohoKey.setShainNo(mShainKoyokeiyakuJoho.getShainNo());
		mShainKoyokeiyakuJohoKey.setEdaban(mShainKoyokeiyakuJoho.getEdaban());
		oldShainKoyokeiyakuJoho = mShainKoyokeiyakuJohoMapper.selectByPrimaryKey(mShainKoyokeiyakuJohoKey);

		// 空要員情報を取得処理
		TKuuyoinjoho kuuyoinjoho = getTKuuyoinjoho(
				shainNo,
				Common.getStringDateYM(oldShainKoyokeiyakuJoho.getTekiyoStartDay()));

		// BP社員案件関連情報を取得処理
		List<TShainAnkenkanrenJoho> tShainAnkenkanrenJohoList = getTShainAnkenkanrenJohoMapper(shainNo,
				Common.getStringDateYM(oldShainKoyokeiyakuJoho.getTekiyoStartDay()));

		//空要員情報更新
		if(kuuyoinjoho != null &&
				kuuyoinjoho.getYoteiSankakuStartDay().compareTo(mShainKoyokeiyakuJoho.getTekiyoStartDay()) != 0) {

			// 変更後空要員存在チェック
			// 空要員情報を取得処理
			TKuuyoinjoho afterKuuyoinjoho = getTKuuyoinjoho(
					shainNo,
					Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));

			// 変更後空要員存在、削除後再追加
			if (afterKuuyoinjoho != null) {
				tKuuyoinjohoMapper.deleteByPrimaryKey(afterKuuyoinjoho);
			}

			TKuuyoinjohoExample tKuuyoinjohoExample = new TKuuyoinjohoExample();
			TKuuyoinjohoExample.Criteria criteria = tKuuyoinjohoExample.createCriteria();
			criteria.andYoinnNoEqualTo(shainNo);
			criteria.andKuuyoteiStartMonthEqualTo(kuuyoinjoho.getKuuyoteiStartMonth());
			kuuyoinjoho.setYoteiSankakuStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
			kuuyoinjoho.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
			kuuyoinjoho.setKoshimbi(now);
			kuuyoinjoho.setKoshinsha(userId);
			kuuyoinjoho.setKoshinKaisu(kuuyoinjoho.getKoshinKaisu() + 1);
			updateCount = tKuuyoinjohoMapper.updateByExample(kuuyoinjoho,tKuuyoinjohoExample);
		}

		tShainAnkenkanrenJohoList.forEach((info) ->{
			info.setSankakuStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
			info.setKuuyoteiStartMonth(Common.getStringDateYM(mShainKoyokeiyakuJoho.getTekiyoStartDay()));
			info.setKoshimbi(now);
			info.setKoshinsha(userId);
			info.setKoshinKaisu(info.getKoshinKaisu() + 1);
			int count = tShainAnkenkanrenJohoMapper.updateByPrimaryKey(info);
		});
	}

	/**
	 * 社員契約情報を取得処理
	 * */
	public List<Map<String, Object>> getMShainKoyokeiyakuJoho(String shainNo) {

		String sql = "SELECT\n" +
				"    skyj.edaban,\n" +
				"    to_char(skyj.tekiyo_start_day, 'yyyy-MM-dd') AS tekiyo_start_day,\n" +
				"    to_char(skyj.tekiyo_end_day, 'yyyy-MM-dd') AS tekiyo_end_day,\n" +
				"    c1.code_name AS koyokeitai_name,\n" +
				"    c2.code_name AS yakushokumei,\n" +
				"    c3.code_name AS shokushuname,\n" +
				"    CASE\n" +
				"        WHEN skyj.tanka_kbn = '2' THEN '人月'\n" +
				"        ELSE '時間'\n" +
				"     END AS tanka_kbn_name,\n" +
				"    to_char(skyj.gekkyu, 'FM9,999,999') AS gekkyu,\n" +
				"    to_char(skyj.mitsumori_genka, 'FM9,999,999') AS mitsumori_genka,\n" +
				"    c4.code_name AS shugyoname,\n" +
				"    skyj.koyokeitai,\n" +
				"    skyj.koseinenkin_flag,\n" +
				"    CASE\n" +
				"        WHEN skyj.seisan_flag = '1' THEN '有'\n" +
				"        ELSE '無'\n" +
				"     END AS seisan_name,\n" +
				"    CASE\n" +
				"        WHEN skyj.seisan_flag = '2' THEN '無'\n" +
				"        WHEN skyj.seisanjikan_start IS NOT NULL\n" +
				"        AND skyj.seisanjikan_end IS NOT NULL THEN skyj.seisanjikan_start || 'H~' || skyj.seisanjikan_end || 'H'\n" +
				"        WHEN skyj.seisanjikan_start IS NOT NULL THEN '開始:' || skyj.seisanjikan_start || 'H'\n" +
				"        WHEN skyj.seisanjikan_end IS NOT NULL THEN '終了:' || skyj.seisanjikan_end || 'H'\n" +
				"        ELSE '無'\n" +
				"    END AS seisanjikan,\n" +
				"    to_char(skyj.seisan_kaitannka, 'FM9,999,999') AS seisan_kaitannka,\n" +
				"    to_char(skyj.seisan_jouitannka, 'FM9,999,999') AS seisan_jouitannka,\n" +
				"    skyj.yobo\n" +
				"FROM\n" +
				"    m_shain_koyokeiyaku_joho skyj\n" +
				"    LEFT JOIN\n" +
				"        m_code c1\n" +
				"    ON  c1.code_id = 'C0001'\n" +
				"    AND c1.edaban = skyj.koyokeitai\n" +
				"    LEFT JOIN\n" +
				"        m_code c2\n" +
				"    ON  c2.code_id = 'C0010'\n" +
				"    AND c2.edaban = skyj.yakushoku_flag\n" +
				"    LEFT JOIN\n" +
				"        m_code c3\n" +
				"    ON  c3.code_id = 'C0009'\n" +
				"    AND c3.edaban = skyj.shokushu_flag\n" +
				"    LEFT JOIN\n" +
				"        m_code c4\n" +
				"    ON  c4.code_id = 'C0011'\n" +
				"    AND c4.edaban = skyj.shugyo_jokyo_flag\n" +
				" WHERE\n" +
				"    skyj.shain_no = #{params.shainNo}\n" +
				" ORDER BY skyj.edaban ASC\n";

		Map<String, Object> params = new HashMap<>();
		params.put("shainNo", shainNo);
		List<Map<String, Object>> rest  = sqlMapper.list(sql, params);
		return rest;
	}

	/**
	 * 案件実績が存在チェック
	 * */
	public Map<String, Object> getYosanJisekiCount(String shainNo, Date tekiyoStartDay) {

		// 始業月を取得
		String sagyoNengetsu = Common.getStringDateYM(tekiyoStartDay);

		String sql="SELECT\n" +
				"    COUNT(*) AS count\n" +
				"FROM\n" +
				"    t_juchukou_yosan_kanri\n" +
				"WHERE\n" +
				"    yoin_no = #{params.shain_no}\n" +
				"AND sagyo_nengetsu >= #{params.sagyo_nengetsu}\n" +
				"AND delete_flag IS NULL \n";

		Map<String, Object> params = new HashMap<>();
		params.put("shain_no", shainNo);
		params.put("sagyo_nengetsu", sagyoNengetsu);

		Map<String, Object> rest = sqlMapper.get(sql, params);
		return rest;
	}

	/**
	 *退職、休職、復帰の場合、関係者にメールを送信
	 * */
	private boolean sendShainInfoMail(UserInfo userInfo,
									  MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho,
									  MShainKihonJoho mShainKihonJoho,
									  String flg) {

		try {

			String title ="";
			String msg1 = "";
			//新入社員の場合
			if ("2".equals(flg)){
				title = "新入社員の連絡";
				msg1 = "は"+Common.dateToSting(mShainKoyokeiyakuJoho.getTekiyoStartDay(),"yyyy/MM/dd") +"に入社しました";
			}else {
				switch(mShainKoyokeiyakuJoho.getShugyoJokyoFlag()){
					case Constants.STATUS_SHUGYO_TAISHOKU:
						//退職
						title = "退職社員の連絡";
						msg1 = "から"+Common.dateToSting(mShainKoyokeiyakuJoho.getTekiyoEndDay(),"yyyy/MM/dd") +"まで退職の申込みがありました";
						break;
					case Constants.STATUS_SHUGYO_ZAISHOKU:
						//復帰
						title = "復帰社員の連絡";
						msg1 = "から"+Common.dateToSting(mShainKoyokeiyakuJoho.getTekiyoStartDay(),"yyyy/MM/dd") +"に復帰の申込みがありました";
						break;
					case "1":
						//休業
						title = "休業社員の連絡";
						msg1 = "から"+Common.dateToSting(mShainKoyokeiyakuJoho.getTekiyoStartDay(),"yyyy/MM/dd") +"に休職の申込みがありました";
						break;
				}

			}
			Context context = new Context();
			context.setVariable("msg1", msg1);
			context.setVariable("name", mShainKihonJoho.getSeiKanji() +"　"+ mShainKihonJoho.getMeiKanji());
			String all_name = commonService.getBushoName(userInfo.getSoshikiCode()) +"　"+ commonService.getYoinName(userInfo.getId());
			context.setVariable("all_name", all_name);
			String mailBody = sendMail.getMailBody("template3",context);

			String sendAddress = "";
			String ccAddress = "";
			//所属の営業メールアドレス取得
			sendAddress = getEigyouMails(mShainKihonJoho.getShainNo());
			//ccを徐、董、高松、beststaff
			ccAddress = getCcMails();
			boolean sendRest = sendMail.sendRawMail(title,
					mailBody,
					sendAddress,
					ccAddress,
					"0","0",null);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	/**
	 *「就業状況変更」以外の項目に変更ある場合、関係者にメールを送信
	 * */
	private boolean sendShainChangeInfoMail(UserInfo userInfo,
											MShainKoyokeiyakuJoho oldMShainKoyokeiyakuJoho,
											MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho,
											MShainKihonJoho mShainKihonJoho){

		try {

			//職種リスト取得
			Map<String, String> shokushuList = commonService.getSelectList(Constants.CD_C0009);
			//役職リスト取得
			Map<String, String> yakushokuList = commonService.getSelectList(Constants.CD_C0010);
			//雇用形態リスト取得
			Map<String, String> KoyokeitaiList = commonService.getSelectList(Constants.CD_C0001);
			// 厚生年金リスト取得
			Map<String, String> koseinenkinList = commonService.getSelectList(Constants.CD_C0008);



			Context context = new Context();
			context.setVariable("name", mShainKihonJoho.getSeiKanji() +"　"+ mShainKihonJoho.getMeiKanji());
			List<EmployeeChangeInfoBean> infoList = new ArrayList<EmployeeChangeInfoBean>();

			//職種
			if(!oldMShainKoyokeiyakuJoho.getShokushuFlag().equals(mShainKoyokeiyakuJoho.getShokushuFlag())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("職種");
				employeeChangeInfoBean.setOldValue(shokushuList.getOrDefault(oldMShainKoyokeiyakuJoho.getShokushuFlag(),"").toString());
				employeeChangeInfoBean.setNewValue(shokushuList.getOrDefault(mShainKoyokeiyakuJoho.getShokushuFlag(),"").toString());
				infoList.add(employeeChangeInfoBean);
			}
			//役職
			if(!oldMShainKoyokeiyakuJoho.getYakushokuFlag().equals(mShainKoyokeiyakuJoho.getYakushokuFlag())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("役職");
				employeeChangeInfoBean.setOldValue(yakushokuList.getOrDefault(oldMShainKoyokeiyakuJoho.getYakushokuFlag(),"").toString() );
				employeeChangeInfoBean.setNewValue(yakushokuList.getOrDefault(mShainKoyokeiyakuJoho.getYakushokuFlag(),"").toString());
				infoList.add(employeeChangeInfoBean);
			}

			//雇用形態
			if(!oldMShainKoyokeiyakuJoho.getKoyokeitai().equals(mShainKoyokeiyakuJoho.getKoyokeitai())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("雇用形態");
				employeeChangeInfoBean.setOldValue(KoyokeitaiList.getOrDefault(oldMShainKoyokeiyakuJoho.getKoyokeitai(),"").toString() );
				employeeChangeInfoBean.setNewValue(KoyokeitaiList.getOrDefault(mShainKoyokeiyakuJoho.getKoyokeitai(),"").toString());
				infoList.add(employeeChangeInfoBean);
			}

			// 正社員、契約社員
			if("1".equals(mShainKoyokeiyakuJoho.getKoyokeitai()) || "2".equals(mShainKoyokeiyakuJoho.getKoyokeitai())) {
				// 厚生年金有無
				if(!mShainKoyokeiyakuJoho.getKoseinenkinFlag().equals(oldMShainKoyokeiyakuJoho.getKoseinenkinFlag())){
					EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
					employeeChangeInfoBean.setTitle("厚生年金");
					employeeChangeInfoBean.setOldValue(
							koseinenkinList.getOrDefault(oldMShainKoyokeiyakuJoho.getKoseinenkinFlag(),"").toString() );
					employeeChangeInfoBean.setNewValue(
							koseinenkinList.getOrDefault(mShainKoyokeiyakuJoho.getKoseinenkinFlag(),"").toString());
					infoList.add(employeeChangeInfoBean);
				}
				// 個人事業主
			} else if ("3".equals(mShainKoyokeiyakuJoho.getKoyokeitai())) {
				// 税金計算
				if(!mShainKoyokeiyakuJoho.getKoseinenkinFlag().equals(oldMShainKoyokeiyakuJoho.getKoseinenkinFlag())){
					EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
					String oldValue = "";
					String newValue = "";
					switch (oldMShainKoyokeiyakuJoho.getKoseinenkinFlag()){
						case "1":
							oldValue = "税込み";
							break;
						case "2" :
							oldValue = "税抜き";
							break;
					}
					switch (mShainKoyokeiyakuJoho.getKoseinenkinFlag()){
						case "1":
							newValue = "税込み";
							break;
						case "2" :
							newValue = "税抜き";
							break;
					}
					employeeChangeInfoBean.setTitle("税金計算");
					employeeChangeInfoBean.setOldValue(oldValue);
					employeeChangeInfoBean.setNewValue(newValue);
					infoList.add(employeeChangeInfoBean);
				}
			}

			//単価区分
			if(!mShainKoyokeiyakuJoho.getTankaKbn().equals(oldMShainKoyokeiyakuJoho.getTankaKbn())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				String oldValue = "";
				String newValue = "";
				switch (oldMShainKoyokeiyakuJoho.getTankaKbn()){
					case "1":
						oldValue = "時間単価";
						break;
					case "2" :
						oldValue = "月単価";
						break;
				}
				switch (mShainKoyokeiyakuJoho.getTankaKbn()){
					case "1":
						newValue = "時間単価";
						break;
					case "2" :
						newValue = "月単価";
						break;
				}
				employeeChangeInfoBean.setTitle("単価区分");
				employeeChangeInfoBean.setOldValue(oldValue);
				employeeChangeInfoBean.setNewValue(newValue);
				infoList.add(employeeChangeInfoBean);
			}

			//月給
			if(!mShainKoyokeiyakuJoho.getGekkyu().equals(oldMShainKoyokeiyakuJoho.getGekkyu())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("月給");
				employeeChangeInfoBean.setOldValue(String.valueOf(oldMShainKoyokeiyakuJoho.getGekkyu()));
				employeeChangeInfoBean.setNewValue(String.valueOf(mShainKoyokeiyakuJoho.getGekkyu()));
				infoList.add(employeeChangeInfoBean);
			}

			//見積原価
			if(!mShainKoyokeiyakuJoho.getMitsumoriGenka().equals(oldMShainKoyokeiyakuJoho.getMitsumoriGenka())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("見積原価");
				employeeChangeInfoBean.setOldValue(String.valueOf(oldMShainKoyokeiyakuJoho.getMitsumoriGenka()));
				employeeChangeInfoBean.setNewValue(String.valueOf(mShainKoyokeiyakuJoho.getMitsumoriGenka()));
				infoList.add(employeeChangeInfoBean);
			}

			//精算時間（START）
			if(!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanjikanStart())
					&& !mShainKoyokeiyakuJoho.getSeisanjikanStart().equals(oldMShainKoyokeiyakuJoho.getSeisanjikanStart())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("精算時間（START）");
				employeeChangeInfoBean.setOldValue(String.valueOf(oldMShainKoyokeiyakuJoho.getSeisanjikanStart()));
				employeeChangeInfoBean.setNewValue(String.valueOf(mShainKoyokeiyakuJoho.getSeisanjikanStart()));
				infoList.add(employeeChangeInfoBean);
			}

			//精算時間（END）
			if(!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanjikanEnd())
					&& !mShainKoyokeiyakuJoho.getSeisanjikanEnd().equals(oldMShainKoyokeiyakuJoho.getSeisanjikanEnd())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("精算時間（END）");
				employeeChangeInfoBean.setOldValue(String.valueOf(oldMShainKoyokeiyakuJoho.getSeisanjikanEnd()));
				employeeChangeInfoBean.setNewValue(String.valueOf(mShainKoyokeiyakuJoho.getSeisanjikanEnd()));
				infoList.add(employeeChangeInfoBean);
			}

			//要望
			if(!mShainKoyokeiyakuJoho.getYobo().equals(oldMShainKoyokeiyakuJoho.getYobo())){
				EmployeeChangeInfoBean employeeChangeInfoBean = new EmployeeChangeInfoBean();
				employeeChangeInfoBean.setTitle("要望");
				employeeChangeInfoBean.setOldValue(String.valueOf(oldMShainKoyokeiyakuJoho.getYobo()));
				employeeChangeInfoBean.setNewValue(String.valueOf(mShainKoyokeiyakuJoho.getYobo()));
				infoList.add(employeeChangeInfoBean);
			}

			context.setVariable("infoList", infoList);
			String all_name = commonService.getBushoName(userInfo.getSoshikiCode()) +"　"+ commonService.getYoinName(userInfo.getId());
			context.setVariable("all_name", all_name);
			String mailBody = sendMail.getMailBody("template4",context);
			String sendAddress = "";
			String ccAddress = "";
			//所属の営業メールアドレス取得
			sendAddress = getEigyouMails(mShainKihonJoho.getShainNo());
			//ccを徐、董、高松、beststaff
			ccAddress = getCcMails();
			boolean sendRest = sendMail.sendRawMail("社員契約内容変更の連絡",
					mailBody,
					sendAddress,
					ccAddress,
					"0","0",null);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
	/**
	 *徐、董、高松、beststaffメールアドレス取得
	 * */
	private String getCcMails (){
		String mailAddress = MAILADDRESS_SOUMU;
		String sql = "select\n" +
				"    mail_address\n" +
				"from\n" +
				"    m_shain_kihon_joho\n" +
				"where\n" +
				"    shain_no in('T00001', 'T00002', 'T00003')\n" +
				"and COALESCE(mail_address, '') != ''";
		List<Map<String, Object>> list = sqlMapper.query(sql);
		for (int i = 0; i < list.size(); i++) {
			mailAddress += "," + list.get(i).getOrDefault("mail_address","");

		}
		return mailAddress;
	}
	/**
	 *所属の営業メールアドレス取得
	 * */
	private String getEigyouMails (String shainNo){
		String mailAddress = "";
		String sql = "select\n" +
				"    mk.shain_no,\n" +
				"    msj.mail_address\n" +
				"from\n" +
				"    m_shain_kihon_joho ms\n" +
				"    left join\n" +
				"        m_shain_kihon_joho msj\n" +
				"    on  ms.shozoku_soshiki_code = msj.shozoku_soshiki_code\n" +
				"    and ms.shozoku_gaisha_code = msj.shozoku_gaisha_code\n" +
				"    left join\n" +
				"        m_shain_koyokeiyaku_joho mk\n" +
				"    on  msj.shain_no = mk.shain_no\n" +
				"    and mk.tekiyo_start_day <= CURRENT_DATE\n" +
				"    and (\n" +
				"            mk.tekiyo_end_day IS NULL\n" +
				"        OR  mk.tekiyo_end_day >= CURRENT_DATE\n" +
				"        )\n" +
				"where\n" +
				"    ms.shain_no = '"+shainNo+"'\n" +
				"and COALESCE(msj.mail_address,'') != ''\n"+
				"and mk.shokushu_flag in('2', '4')";
		List<Map<String, Object>> list = sqlMapper.query(sql);
		for (int i = 0; i < list.size(); i++) {
			mailAddress += (i == 0 ? "":",") + list.get(i).getOrDefault("mail_address","");

		}
		return mailAddress;
	}
}
