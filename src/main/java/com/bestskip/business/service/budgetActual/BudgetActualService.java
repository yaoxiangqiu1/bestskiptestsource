package com.bestskip.business.web.service.budgetActual;

import java.util.List;
import java.util.Map;

public interface BudgetActualService {
    List<Map<String, Object>> getAnkenList(String bushoNo,
                                           String kokyakuCode,
                                           String shozokuCode,
                                           String haninFlg,
                                           String year,
                                           String nowFlag,
                                           String startYm,
                                           String endYm);
    List<Map<String, Object>> setYoinList(Map<String, Object> mapInfo, String[] showMonthList,String shozokuCode);
}
