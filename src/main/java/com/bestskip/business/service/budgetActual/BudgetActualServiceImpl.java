package com.bestskip.business.web.service.budgetActual;

import com.bestskip.business.web.base.SqlMapper;
import com.bestskip.business.web.common.Common;
import com.bestskip.business.web.common.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import java.time.LocalDate;
import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class BudgetActualServiceImpl implements BudgetActualService {
    @Autowired
    private SqlMapper sqlMapper;

    /**
     * 案件一覧取得
     * */
    public List<Map<String, Object>> getAnkenList(
            String bushoNo,
            String kokyakuCode,
            String shozokuCode,
            String haninFlg,
            String year,
            String nowFlag,
            String startYm,
            String endYm){

        List<Map<String, Object>> ankenList = new ArrayList<Map<String,Object>>();
        String DayFormart ="yyyyMM";
        String[] showMonthList;

        //案件一覧検索SQL
        String sql = "SELECT\n" +
                "    DISTINCT\n" +
                "    tabi.anken_no,\n" +
                "    tabi.edaban,\n" +
                "    ms.soshiki_name,\n" +
                "    tabi.koshin_kaisu,\n" +
                "    tabi.anken_name,\n" +
                "    tabi.ninzu,\n" +
                "    tabi.kosu,\n" +
                "    tabi.keiyaku_keitai_flag,\n" +
                "    tabi.kibo,\n" +
                "    tabi.sankaku_start_teibi,\n" +
                "    tabi.sankaku_end_teibi,\n" +
                "    mk.ryakusho,\n" +
                "    mc1.code_name AS juchu_jotai,\n" +
                "    CASE\n" +
                "        WHEN tjjk.yoinsu = tabi.ninzu THEN '募集済' ELSE'募集中'\n" +
                "    END AS yoin_jotai,\n" +
                "    mskj.shimei_sei_kanji || mskj.shimei_mei_kanji AS tantosha_name,\n" +
                "    mc2.code_name AS soshin_jotai,\n" +
                "    mc3.code_name AS keiyaku_keitai,\n" +
                "    mc4.code_name AS kokyaku_mendan,\n" +
                "    sagyo_basho\n" +
                "FROM\n" +
                "    t_juchukou_yosan_kanri ty\n" +
                "    LEFT JOIN\n" +
                "        t_anken_base_info tabi\n" +
                "    ON ty.anken_no = tabi.anken_no\n" +
                "    AND ty.edaban = tabi.edaban\n" +
                "    LEFT JOIN\n" +
                "        m_kaisha mk\n" +
                "    ON  tabi.juchusaki_gaisha_code = mk.kaisha_code\n" +
                "    LEFT JOIN\n" +
                "        m_soshiki ms\n" +
                "    ON  tabi.shozoku_soshiki_code = ms.soshiki_code\n" +
                "    LEFT JOIN\n" +
                "        m_soshiki_sekininsha mskj\n" +
                "    ON  mskj.torihikisaki_shain_no = tabi.juchusaki_tantosha_no\n" +
                "    LEFT JOIN\n" +
                "        m_code mc1\n" +
                "    ON  mc1.edaban = tabi.juchu_jotai_flag\n" +
                "    AND mc1.code_id = #{params.C0004}\n" +
                "    LEFT JOIN\n" +
                "        m_code mc2\n" +
                "    ON  mc2.edaban = tabi.ikkatsu_merusoshin_jotai_flag\n" +
                "    AND mc2.code_id = #{params.C0022}\n" +
                "    LEFT JOIN\n" +
                "        m_code mc3\n" +
                "    ON  mc3.edaban = tabi.keiyaku_keitai_flag\n" +
                "    AND mc3.code_id = #{params.C0014}\n" +
                "    LEFT JOIN\n" +
                "        m_code mc4\n" +
                "    ON  mc4.edaban = tabi.kokyaku_mendan_flag\n" +
                "    AND mc4.code_id = #{params.C0016}\n" +
                "    LEFT JOIN (\n" +
                "select\n" +
                "    anken_no,\n" +
                "    edaban,\n" +
                "    COUNT(shain_no) as yoinsu\n" +
                "from\n" +
                "    (\n" +
                "        select\n" +
                "            shain_no,\n" +
                "            anken_no,\n" +
                "            edaban\n" +
                "        from\n" +
                "            t_shain_ankenkanren_joho\n" +
                "        union all\n" +
                "        select\n" +
                "            bp_shain_no as shain_no,\n" +
                "            anken_no,\n" +
                "            edaban\n" +
                "        from\n" +
                "            t_bp_shain_hatchukanren_joho\n" +
                "    ) yoin\n" +
                "group by\n" +
                "    anken_no,\n" +
                "    edaban"+
                "        ) tjjk\n" +
                "    ON  tabi.anken_no = tjjk.anken_no\n" +
                "    AND  tabi.edaban = tjjk.edaban\n" +
                "WHERE\n" +
                "    (tabi.sakujyo_flag <> '1' OR tabi.sakujyo_flag = '0' OR tabi.sakujyo_flag IS NULL)\n"+
                "    AND tabi.shozoku_soshiki_code in(\n" +
                "        WITH RECURSIVE child(soshiki_level, soshiki_code, oya_soshiki_code, soshiki_name, allName) AS(\n" +
                "            SELECT\n" +
                "                oms.soshiki_level,\n" +
                "                oms.soshiki_code,\n" +
                "                oms.oya_soshiki_code,\n" +
                "                oms.soshiki_name,\n" +
                "                soshiki_name::TEXT AS soshiki_name\n" +
                "            FROM\n" +
                "                m_soshiki oms\n" +
                "            WHERE\n" +
                "                oms.soshiki_code = #{params.soshiki_code}\n" +
                "            UNION ALL\n" +
                "            SELECT\n" +
                "                ms.soshiki_level,\n" +
                "                ms.soshiki_code,\n" +
                "                ms.oya_soshiki_code,\n" +
                "                ms.soshiki_name,\n" +
                "                child.allName || '/' || ms.soshiki_name AS allName\n" +
                "            FROM\n" +
                "                m_soshiki ms,\n" +
                "                child\n" +
                "            WHERE\n" +
                "                ms.oya_soshiki_code = child.soshiki_code\n" +
                "            AND ms.soshiki_code != ms.oya_soshiki_code\n" +
                "        )\n" +
                "        SELECT\n" +
                "            soshiki_code\n" +
                "        FROM\n" +
                "            child\n" +
                "        ORDER BY\n" +
                "            soshiki_level\n" +
                "    )";
        Map<String, Object> params = new HashMap<>();
        params.put("soshiki_code", bushoNo);
        params.put("C0004", Constants.CD_C0004);
        params.put("C0022", Constants.CD_C0022);
        params.put("C0014", Constants.CD_C0014);
        params.put("C0016", Constants.CD_C0016);
        //--開始終了指定した場合
        if (!StringUtils.isEmpty(startYm)){
            sql +=  " AND ty.sagyo_nengetsu BETWEEN '" + startYm.replaceAll("/","") + "' \n"+
                    " AND '" + endYm.replaceAll("/","") + "'\n" ;
            //showMonthList
            long count = Common.monthDiff(startYm+"/01",endYm+"/01","yyyy/MM/dd")+1;
            showMonthList = Common.getMonthList(startYm.replaceAll("/",""),(int) count);
        }else if ("1".equals(nowFlag)) {//--現在から半年の場合
            //showMonthList
            showMonthList = Common.getMonthList(Common.getNow(LocalDate.now(),DayFormart),6);
            sql +=  " AND ty.sagyo_nengetsu BETWEEN TO_CHAR(CURRENT_DATE, 'yyyymm') \n"+
                    " AND TO_CHAR(CURRENT_DATE + INTERVAL '5 MONTH', 'yyyymm') \n" ;
        } else {//--対象年
            sql +=  " AND ty.sagyo_nengetsu BETWEEN '" + year + "04' \n"+
                    " AND '" + (Integer.parseInt(year) + 1) + "03'\n" ;
            showMonthList = Common.getMonthList(year+"04",12);
        }

        //顧客会社
        if(kokyakuCode != null && !kokyakuCode.isEmpty()){
            sql +=" AND tabi.juchusaki_gaisha_code = '"+kokyakuCode+"' ";
        }
        //失注と計画中案件除き
        sql += " AND tabi.juchu_jotai_flag in ('1','2','4') ";
        sql += "ORDER BY\n" +
                "    tabi.anken_no,\n" +
                "    tabi.edaban;";

        List<Map<String, Object>> rset  = sqlMapper.list(sql, params);

        String[] finalShowMonthList = showMonthList;
        rset.forEach(mapInfo -> {
            // 要員一覧取得
            List<Map<String, Object>> infoList = setYoinList(
                    mapInfo,
                    finalShowMonthList,
                    shozokuCode);

            if (infoList != null){
                ankenList.addAll(infoList);
            }
        });
        return  ankenList;
    }

    /**
     * 要員一覧取得
     * */
    public List<Map<String, Object>> setYoinList(
            Map<String, Object> mapInfo,
            String[] showMonthList,
            String shozokuCode) {

        List<Map<String, Object>> ankenList = new ArrayList<Map<String,Object>>();

        // 発注実績一覧共通SQL文
        String hachuJisekiSql = getOrderCommonSql();

        //受注高予算の要員一覧を取得
        String sql = "SELECT\n" +
                "    yoin_no,\n" +
                "    code_name,\n" +
                "    shain_name,\n" +
                "    shozoku_gaisha_code,\n" +
                "    kaisyaryakusho,\n" +
                "    kyuyo,\n" +
                "    array_agg(sagyo_nengetsu ORDER BY sagyo_nengetsu)::text[] AS month_list,\n" +
                "    array_agg(jichuko || ',' || genka || ',' || yosanarari ORDER BY sagyo_nengetsu) AS kosu_list,\n" +
                "    array_agg(hachu_gokei_kingaku_nuki ORDER BY sagyo_nengetsu)::text[] AS jiseki_hachu_list,\n" +
                "    array_agg(hachu_gokei_kingaku_nuki_flg ORDER BY sagyo_nengetsu)::text[] AS jiseki_hachu_flg_list\n" +
                "FROM\n" +
                "    (\n" +
                "        SELECT\n" +
                "            *,\n" +
                "            round(jichuko - genka, 0) AS yosanarari\n" +
                "        FROM\n" +
                "            (\n" +
                "                SELECT\n" +
                "                    yoin.shain_no AS yoin_no,\n" +
                "                    mc2.code_name,\n" +
                "                    yoin.shain_name,\n" +
                "                    yoin.shozoku_gaisha_code,\n" +
                "                    mk.ryakusho AS kaisyaryakusho,\n" +
                "                    tjyk.kyuyo,\n" +
                "                    tjyk.sagyo_nengetsu,\n" +
                "                    CASE tjyk.juchu_tanka_kbn\n" +
                "                        WHEN '1' THEN round(tjyk.juchu_kosu_jikan * tjyk.juchu_tanka, 0)\n" +
                "                        ELSE round(tjyk.juchu_kosu_ningetsu * tjyk.juchu_tanka, 0)\n" +
                "                    END AS jichuko,\n" +
                "                    CASE yoin.gennka_tanka_kbn\n" +
                "                        WHEN '1' THEN round(tjyk.gennka_kosu_jikan * yoin.gennka_tanka, 0)\n" +
                "                        ELSE round(tjyk.gennka_kosu_ningetsu * yoin.gennka_tanka, 0)\n" +
                "                    END AS genka,\n" +
                "                    ordertab.hachu_gokei_kingaku_nuki,\n" +
                "                    ordertab.hachu_gokei_kingaku_nuki_flg\n" +
                "                FROM\n" +
                "                    t_juchukou_yosan_kanri tjyk\n" +
                "                    LEFT JOIN\n" +
                "                    (\n" +
                "                        SELECT\n" +
                "                            tbshj.bp_shain_no AS shain_no,\n" +
                "                            tbshj.anken_no,\n" +
                "                            tbshj.edaban,\n" +
                "                            mbskj.shozoku_gaisha_code,\n" +
                "                            mbp.sei_kanji || mbp.mei_kanji AS shain_name,\n" +
                "                            mbskj.tanka_kbn AS gennka_tanka_kbn,\n" +
                "                            mbskj.tanka AS gennka_tanka,\n" +
                "                            tbshj.sankaku_start_day,\n" +
                "                            tbshj.sankaku_end_day,\n" +
                "                            mbskj.tekiyo_start_day,\n" +
                "                            mbskj.tekiyo_end_day,\n" +
                "                            tbshj.teianjokyo_flag\n" +
                "                        FROM\n" +
                "                            t_bp_shain_hatchukanren_joho tbshj\n" +
                "                            LEFT JOIN\n" +
                "                                m_bp_shain_kihon_joho mbp\n" +
                "                            ON  mbp.bp_shain_no = tbshj.bp_shain_no\n" +
                "                            LEFT JOIN\n" +
                "                                m_bp_shain_koyokeiyaku_joho mbskj\n" +
                "                            ON  tbshj.bp_shain_no = mbskj.bp_shain_no\n" +
                "                            AND #{params.end_day} >= to_char(mbskj.tekiyo_start_day, 'yyyyMM')\n" +
                "                            AND (\n" +
                "                                    mbskj.tekiyo_end_day IS NULL\n" +
                "                                OR  #{params.start_day} <= to_char(mbskj.tekiyo_end_day, 'yyyyMM')\n" +
                "                                )\n" +
                "                            AND tbshj.sankaku_end_day >= mbskj.tekiyo_start_day\n" +
                "                            AND (\n" +
                "                                    mbskj.tekiyo_end_day IS NULL\n" +
                "                                OR  tbshj.sankaku_start_day <= mbskj.tekiyo_end_day\n" +
                "                                )\n" +
                "                            UNION ALL\n" +
                "                            SELECT\n" +
                "                                tsaj.shain_no,\n" +
                "                                tsaj.anken_no,\n" +
                "                                tsaj.edaban,\n" +
                "                                ms.shozoku_gaisha_code,\n" +
                "                                ms.sei_kanji || ms.mei_kanji AS shain_name,\n" +
                "                                mskj.tanka_kbn AS gennka_tanka_kbn,\n" +
                "                                CASE\n" +
                "                                    WHEN mskj.koyokeitai = '3' AND mskj.koseinenkin_flag = '2' THEN mskj.gekkyu\n" +
                "                                    ELSE mskj.mitsumori_genka\n" +
                "                                END AS gennka_tanka,\n" +
                "                                tsaj.sankaku_start_day,\n" +
                "                                tsaj.sankaku_end_day,\n" +
                "                                mskj.tekiyo_start_day,\n" +
                "                                mskj.tekiyo_end_day,\n" +
                "                                tsaj.teianjokyo_flag\n" +
                "                            FROM\n" +
                "                                t_shain_ankenkanren_joho tsaj\n" +
                "                            LEFT JOIN\n" +
                "                                m_shain_kihon_joho ms\n" +
                "                            ON  tsaj.shain_no = ms.shain_no\n" +
                "                            LEFT JOIN\n" +
                "                                m_shain_koyokeiyaku_joho mskj\n" +
                "                            ON  tsaj.shain_no = mskj.shain_no\n" +
                "                            AND #{params.end_day} >= to_char(mskj.tekiyo_start_day, 'yyyyMM')\n" +
                "                            AND (\n" +
                "                                    mskj.tekiyo_end_day IS NULL\n" +
                "                                OR  #{params.start_day} <= to_char(mskj.tekiyo_end_day, 'yyyyMM')\n" +
                "                                )\n" +
                "                            AND tsaj.sankaku_end_day >= mskj.tekiyo_start_day\n" +
                "                            AND (\n" +
                "                                    mskj.tekiyo_end_day IS NULL\n" +
                "                                OR  tsaj.sankaku_start_day <= mskj.tekiyo_end_day\n" +
                "                                )\n" +
                "                    ) yoin\n" +
                "                    ON  yoin.anken_no = tjyk.anken_no\n" +
                "                    AND yoin.edaban = tjyk.edaban\n" +
                "                    AND yoin.shain_no = tjyk.yoin_no\n" +
                "                    AND tjyk.delete_flag IS NULL\n" +
                "                    AND tjyk.sagyo_nengetsu >= to_char(yoin.tekiyo_start_day, 'yyyyMM')\n" +
                "                    AND (\n" +
                "                            yoin.tekiyo_end_day IS NULL\n" +
                "                        OR  tjyk.sagyo_nengetsu <= to_char(yoin.tekiyo_end_day, 'yyyyMM')\n" +
                "                        )\n" +
                "                    AND tjyk.sagyo_nengetsu >= to_char(yoin.sankaku_start_day, 'yyyyMM')\n" +
                "                    AND (\n" +
                "                            yoin.sankaku_end_day IS NULL\n" +
                "                        OR  tjyk.sagyo_nengetsu <= to_char(yoin.sankaku_end_day, 'yyyyMM')\n" +
                "                        )\n" +
                "                    LEFT JOIN\n" +
                "                        m_kaisha mk\n" +
                "                    ON  yoin.shozoku_gaisha_code = mk.kaisha_code\n" +
                "                    LEFT JOIN\n" +
                hachuJisekiSql +
                "                    ON  tjyk.anken_no = ordertab.anken_no\n" +
                "                    AND tjyk.edaban = ordertab.edaban\n" +
                "                    AND tjyk.yoin_no = ordertab.yoin_no\n" +
                "                    AND tjyk.sagyo_nengetsu = ordertab.sagyo_nengetsu\n" +
                "                    LEFT JOIN\n" +
                "                        t_anken_base_info tabi\n" +
                "                    ON  tjyk.anken_no = tabi.anken_no\n" +
                "                    AND tjyk.edaban = tabi.edaban\n" +
                "                    LEFT JOIN\n" +
                "                        m_code mc\n" +
                "                    ON  tjyk.yoin_kbn = mc.edaban\n" +
                "                    AND mc.code_id = #{params.C0023}\n" +
                "                    LEFT JOIN\n" +
                "                        m_code mc2\n" +
                "                    ON  yoin.teianjokyo_flag = mc2.edaban\n" +
                "                    AND mc2.code_id = #{params.C0024}\n" +
                "                WHERE\n" +
                "                    yoin.anken_no = #{params.anken_no}\n" +
                "                AND yoin.edaban = #{params.edaban}\n" +
                "                AND to_char(yoin.sankaku_start_day,'yyyyMM') <= #{params.end_day}\n" +
                "                AND ( yoin.sankaku_end_day IS NULL OR (to_char(yoin.sankaku_end_day,'yyyyMM') >= #{params.start_day} ))\n" +
                "                ORDER BY\n" +
                "                    tjyk.yoin_no,\n" +
                "                    tjyk.sagyo_nengetsu\n" +
                "            ) a\n" +
                "    ) newTb\n" +
                "GROUP BY\n" +
                "    newTb.yoin_no,\n" +
                "    newTb.code_name,\n" +
                "    newTb.kaisyaryakusho,\n" +
                "    newTb.shozoku_gaisha_code,\n" +
                "    newTb.shain_name";

        Map<String, Object> params = new HashMap<>();
        params.put("anken_no", mapInfo.get("anken_no").toString());
        params.put("edaban", Integer.parseInt(mapInfo.get("edaban").toString()));
        params.put("C0023", Constants.CD_C0023);
        params.put("C0024", Constants.CD_C0024);
        params.put("end_day", showMonthList[showMonthList.length-1]);
        params.put("start_day", showMonthList[0]);
        List<Map<String, Object>> rest  = sqlMapper.list(sql, params);

        //所属会社
        if(shozokuCode !=null && !shozokuCode.isEmpty()) {
            boolean hasShozokuCode = false;
            for (int i = 0;i<rest.size();i++){
                String infoCode = rest.get(i).get("shozoku_gaisha_code") == null ? "" : rest.get(i).get("shozoku_gaisha_code").toString().trim();
                if(infoCode.equals(shozokuCode.trim())){
                    hasShozokuCode = true;
                    break;
                }
            }
            if(!hasShozokuCode) {
                return null;
            }
        }

        //月毎の必要人数
        int peopleCount = 0;
        String DayFormart ="yyyy-MM-dd";

        //契約形態が請負の場合
        if("1".equals(mapInfo.get("keiyaku_keitai_flag").toString())) {
            //案件開始終了日ある場合
            if (mapInfo.get("sankaku_end_teibi") != null && mapInfo.get("sankaku_start_teibi") != null) {
                //案件の予定日数を計算
                long dismDay = Common.dayDiff(mapInfo.get("sankaku_start_teibi").toString(),
                        mapInfo.get("sankaku_end_teibi").toString(),DayFormart);
                //工数
                double kosu = mapInfo.get("kosu") == null ? 0d :Double.parseDouble(mapInfo.get("kosu").toString());
                //月毎の必要人数を計算
                peopleCount =  (int) Math.ceil(kosu*1.0/(dismDay/30.0));
            }
            else {
                peopleCount =  mapInfo.get("ninzu") == null ? 0 : Double.valueOf(mapInfo.get("ninzu").toString()).intValue();
            }
        } else {
            peopleCount =  mapInfo.get("ninzu") == null ? 0 : Double.valueOf(mapInfo.get("ninzu").toString()).intValue();
        }

        if (rest.size() > peopleCount) {
            peopleCount = rest.size();
        }

        //最大要員数でループ
        for (int i = 0; i < peopleCount; i++) {

            List<Map<String, String>> resultList = new ArrayList<>();
            Map<String, Object> newMap = new HashMap<>(mapInfo);

            //案件体制から設定する
            if (rest.size() - i > 0) {

                //ダミー要員の場合
                if(rest.get(i).get("yoin_no").toString().contains(Constants.DUMMY_CD)) {
                    //要員名設定
                    newMap.put("name",Constants.DUMMY_NAME);
                    //要員会社名設定
                    newMap.put("kaisyaryakusho","");

                } else {//ダミー要員以外の場合

                    //要員名設定
                    newMap.put("name",rest.get(i).get("shain_name").toString());
                    //要員会社名設定
                    newMap.put("kaisyaryakusho",rest.get(i).get("kaisyaryakusho") == null ? "" : rest.get(i).get("kaisyaryakusho").toString());
                }

                //提案状況
                newMap.put("code_name",rest.get(i).get("code_name").toString());

                //要員の工数情報設定
                String[] monthList = (String[])rest.get(i).get("month_list");
                String[] kosiList = (String[])rest.get(i).get("kosu_list");
                String[] jisekiHachuList = (String[])rest.get(i).get("jiseki_hachu_list");
                String[] jisekiHachuFlgList = (String[])rest.get(i).get("jiseki_hachu_flg_list");

                //予算受注高合計
                long yosanJuchuSum = 0;
                //予算原価合計
                long yosanGenkaSum = 0;
                //予算粗利合計
                long yosanArariSum = 0;

                //実績発注額合計
                long jisekiHachuSum = 0;

                for (String s : showMonthList) {

                    Map<String, String> mapsInfo = new HashMap<>();

                    int a = monthList[0] == null ? -1 : Arrays.binarySearch(monthList, s);
                    if (a > -1) {
                        String[] toArray = kosiList[a] == null ? new String[]{"　", "　", "　"} : kosiList[a].split(",");

                        // 予算額
                        mapsInfo.put("yosan_juchu_value", toArray[0]);
                        mapsInfo.put("yosan_hachu_value", toArray[1]);
                        mapsInfo.put("yosan_cal_value", toArray[2]);

                        // 実績発注額
                        if (StringUtils.isEmpty(jisekiHachuList[a])) {
                            mapsInfo.put("jiseki_hachu_value", "　");
                        } else {
                            mapsInfo.put("jiseki_hachu_value", jisekiHachuList[a]);
                            // 合計計算
                            jisekiHachuSum += Long.parseLong(jisekiHachuList[a]);
                        }

                        // 実績発注額フラグ
                        if (StringUtils.isEmpty(jisekiHachuFlgList[a])) {
                            mapsInfo.put("jiseki_hachu_flg_value", "0");
                        } else {
                            mapsInfo.put("jiseki_hachu_flg_value", jisekiHachuFlgList[a]);
                        }

                        // 実績額（初期化のみ）
                        mapsInfo.put("jiseki_juchu_value", "　");
                        mapsInfo.put("jiseki_cal_value", "　");

                        yosanJuchuSum += Long.parseLong("　".equals(toArray[0]) ? "0" : toArray[0]);
                        yosanGenkaSum += Long.parseLong("　".equals(toArray[1]) ? "0" : toArray[1]);
                        yosanArariSum += Long.parseLong("　".equals(toArray[2]) ? "0" : toArray[2]);

                    } else {

                        // 予算額
                        mapsInfo.put("yosan_juchu_value", "　");
                        mapsInfo.put("yosan_hachu_value", "　");
                        mapsInfo.put("yosan_cal_value", "　");

                        // 実績額
                        mapsInfo.put("jiseki_juchu_value", "　");
                        mapsInfo.put("jiseki_hachu_value", "　");
                        mapsInfo.put("jiseki_cal_value", "　");
                    }

                    resultList.add(mapsInfo);
                }

                String yosanJuchu = yosanJuchuSum == 0 ? "　": String.valueOf(yosanJuchuSum);
                String yosanGenka = yosanGenkaSum == 0 ? "　": String.valueOf(yosanGenkaSum);
                String yosanArari = yosanArariSum == 0 ? "　": String.valueOf(yosanArariSum);
                String jisekiHachu = jisekiHachuSum == 0 ? "　": String.valueOf(jisekiHachuSum);

                Map<String, String> mapsInfo = new HashMap<>();
                // 予算額（横年度合計）
                mapsInfo.put("yosan_juchu_value", yosanJuchu);
                mapsInfo.put("yosan_hachu_value", yosanGenka);
                mapsInfo.put("yosan_cal_value", yosanArari);

                // 実績発注額（横年度合計）
                mapsInfo.put("jiseki_hachu_value", jisekiHachu);

                // 実績額（横年度合計）（初期化のみ）
                mapsInfo.put("jiseki_juchu_value", "　");
                mapsInfo.put("jiseki_cal_value", "　");
                resultList.add(mapsInfo);

                newMap.put("mounthList", resultList);

            } else {

                // 固定の「」を設定する
                newMap.put("name", "");

                for (int j = 0;j <= showMonthList.length; j++) {

                    Map<String, String> mapsInfo = new HashMap<>();
                    mapsInfo.put("yosan_juchu_value", "　");
                    mapsInfo.put("yosan_hachu_value", "　");
                    mapsInfo.put("yosan_cal_value", "　");
                    mapsInfo.put("jiseki_juchu_value", "　");
                    mapsInfo.put("jiseki_hachu_value", "　");
                    mapsInfo.put("jiseki_cal_value", "　");
                    resultList.add(mapsInfo);
                }

                newMap.put("mounthList", resultList);
            }

            // 最後のレコードの場合
            if (i == peopleCount - 1) {

                // 実績金額一覧を作成
                setJisekiKingakuInfo(
                        mapInfo.get("anken_no").toString(),
                        mapInfo.get("edaban").toString(),
                        showMonthList,
                        resultList);
            }

            // 最初のレコードの場合
            if (i == 0) {
                // セル結合フラグを設定
                newMap.put("cell_flg", "0");
            } else {
                // セル結合フラグを設定
                newMap.put("cell_flg", "1");
            }

            ankenList.add(newMap);

            //メモリー解放
            //resultList.clear();
        }

        return  ankenList;
    }

    /**
     * 発注実績一覧共通SQL文
     * */
    private String getOrderCommonSql() {

        // 発注実績工数により分別に計算共通SQL文(同じ人の複数案件の実績は1個案件で入力し、入力実績原価が工数によりそれぞれの案件に振り分け)
        String sqlDiff = getHachuDiffCommonSql();

        // サブ部
        String sqlSelectSub = "(\n" +
                "    SELECT\n" +
                "        ordertempc.anken_no,\n" +
                "        ordertempc.edaban,\n" +
                "        ordertempc.yoin_no,\n" +
                "        ordertempc.sagyo_nengetsu,\n" +
                "        CASE\n" +
                "            WHEN thjk.totsugose_jotai = '1' THEN thjk.gokei_kingaku_nuki\n" +
                "            WHEN ordertempc.sagyo_nengetsu < TO_CHAR(CURRENT_DATE, 'YYYYMM') THEN ordertempc.hachu_gokei_kingaku_nuki\n" +
                "        END AS hachu_gokei_kingaku_nuki,\n" +
                "        CASE\n" +
                "            WHEN thjk.totsugose_jotai = '1' THEN '0'\n" +
                "            WHEN ordertempc.sagyo_nengetsu < TO_CHAR(CURRENT_DATE, 'YYYYMM') THEN '1'\n" +
                "            ELSE '0'\n" +
                "        END AS hachu_gokei_kingaku_nuki_flg\n" +
                "    FROM\n"
                ;

        // 発注実績一覧共通SQLサブ文
        sqlSelectSub += getOrderCommonSubSql();

        sqlSelectSub +=
                "        LEFT JOIN\n" +
                        sqlDiff +
                        "        ON  thjk.anken_no = ordertempc.anken_no\n" +
                        "        AND thjk.edaban = ordertempc.edaban\n" +
                        "        AND thjk.yoin_no = ordertempc.yoin_no\n" +
                        "        AND thjk.sagyo_nengetsu = ordertempc.sagyo_nengetsu\n"
        ;

        sqlSelectSub +=
                " ) ordertab"
        ;

        return sqlSelectSub;
    }

    /**
     * 発注実績工数により分別に計算共通SQL文
     * */
    private String getHachuDiffCommonSql() {
        String sql;
        sql = "(\n" +
                "    SELECT\n" +
                "        sumvalue.yoin_no,\n" +
                "        sumvalue.sagyo_nengetsu,\n" +
                "        ttb.anken_no,\n" +
                "        ttb.edaban,\n" +
                "        tta.totsugose_jotai,\n" +
                "        CASE\n" +
                "            WHEN ttb.gennka_tanka_kbn = '2'\n" +
                "        AND sumvalue.gokei_kosu_ningetsu <> 0 THEN round((ttb.gennka_kosu_ningetsu / sumvalue.gokei_kosu_ningetsu) * sumvalue.gokei_kingaku_nuki, 0)\n" +
                "            WHEN ttb.gennka_tanka_kbn = '1'\n" +
                "        AND sumvalue.gokei_kosu_jikan <> 0 THEN round((ttc.kimmujikan / sumvalue.gokei_kosu_jikan) * sumvalue.gokei_kingaku_nuki, 0)\n" +
                "            ELSE tta.gokei_kingaku_nuki\n" +
                "        END AS gokei_kingaku_nuki\n" +
                "    FROM\n" +
                "        t_hattsu_jisseki_kanri tta\n" +
                "        LEFT JOIN\n" +
                "            t_juchukou_yosan_kanri ttb\n" +
                "        ON  tta.anken_no = ttb.anken_no\n" +
                "        AND tta.edaban = ttb.edaban\n" +
                "        AND tta.yoin_no = ttb.yoin_no\n" +
                "        AND tta.sagyo_nengetsu = ttb.sagyo_nengetsu\n" +
                "        LEFT JOIN\n" +
                "            t_yoinkimmu_joho ttc\n" +
                "        ON  tta.anken_no = ttc.anken_no\n" +
                "        AND tta.edaban = ttc.edaban\n" +
                "        AND tta.yoin_no = ttc.yoinn_no\n" +
                "        AND tta.sagyo_nengetsu = ttc.kuuyotei_start_month\n" +
                "        LEFT JOIN\n" +
                "            (\n" +
                "                SELECT\n" +
                "                    ta.yoin_no,\n" +
                "                    ta.sagyo_nengetsu,\n" +
                "                    ta.totsugose_jotai,\n" +
                "                    SUM(ta.gokei_kingaku_nuki) AS gokei_kingaku_nuki,\n" +
                "                    SUM(tb.gennka_kosu_ningetsu) AS gokei_kosu_ningetsu,\n" +
                "                    SUM(tc.kimmujikan) AS gokei_kosu_jikan\n" +
                "                FROM\n" +
                "                    t_hattsu_jisseki_kanri ta\n" +
                "                    LEFT JOIN\n" +
                "                        t_juchukou_yosan_kanri tb\n" +
                "                    ON  ta.anken_no = tb.anken_no\n" +
                "                    AND ta.edaban = tb.edaban\n" +
                "                    AND ta.yoin_no = tb.yoin_no\n" +
                "                    AND ta.sagyo_nengetsu = tb.sagyo_nengetsu\n" +
                "                    LEFT JOIN\n" +
                "                        t_yoinkimmu_joho tc\n" +
                "                    ON  ta.anken_no = tc.anken_no\n" +
                "                    AND ta.edaban = tc.edaban\n" +
                "                    AND ta.yoin_no = tc.yoinn_no\n" +
                "                    AND ta.sagyo_nengetsu = tc.kuuyotei_start_month\n" +
                "                WHERE\n" +
                "                    ta.totsugose_jotai = '1'\n" +
                "                GROUP BY\n" +
                "                    ta.yoin_no,\n" +
                "                    ta.sagyo_nengetsu,\n" +
                "                    ta.totsugose_jotai\n" +
                "            ) sumvalue\n" +
                "        ON  sumvalue.yoin_no = tta.yoin_no\n" +
                "        AND sumvalue.sagyo_nengetsu = tta.sagyo_nengetsu\n" +
                "    WHERE\n" +
                "        sumvalue.yoin_no IS NOT NULL\n" +
                "    AND tta.totsugose_jotai = '1'\n" +
                ") thjk";
        return sql;
    }

    /**
     * 発注実績一覧共通SQLサブ文
     * */
    private String getOrderCommonSubSql() {

        String sqlCommon =
                "(\n" +
                        "    SELECT\n" +
                        "        anken_no,\n" +
                        "        edaban,\n" +
                        "        anken_name,\n" +
                        "        yoin_no,\n" +
                        "        yoin_kbn,\n" +
                        "        zei_flg,\n" +
                        "        shain_name,\n" +
                        "        kaisha_code,\n" +
                        "        kaisha_name,\n" +
                        "        soshiki_name,\n" +
                        "        keiyakukeitai_name,\n" +
                        "        sankaku_start_day,\n" +
                        "        sankaku_end_day,\n" +
                        "        sagyo_nengetsu,\n" +
                        "        sagyo_nengetsu_name,\n" +
                        "        furikomi_yotei_search_month,\n" +
                        "        furikomi_yotei_month,\n" +
                        "        kimmujikan,\n" +
                        "        hachu_tanka_kbn,\n" +
                        "        hachu_tanka_kbn_name,\n" +
                        "        hachu_kosu,\n" +
                        "        hachu_tanka,\n" +
                        "        hachu_seisanjikan,\n" +
                        "        hachu_seisan_chouka,\n" +
                        "        hachu_seisan_koujyo,\n" +
                        "        hachu_tanka_total,\n" +
                        "        hachu_kabusoku_jikan,\n" +
                        "        hachu_kabusoku_seisan,\n" +
                        "        hachu_kotsuhi,\n" +
                        "        hachu_shukuhakuhi,\n" +
                        "        hachu_sonotakeihi,\n" +
                        "        teikiken_sum,\n" +
                        "        hachu_gokei_kingaku_nuki,\n" +
                        "        zeikin,\n" +
                        "        round(hachu_gokei_kingaku_nuki + zeikin, 0) AS hachu_gokei_kingaku_komi\n" +
                        "    FROM\n" +
                        "        (\n" +
                        "            SELECT\n" +
                        "                anken_no,\n" +
                        "                edaban,\n" +
                        "                anken_name,\n" +
                        "                yoin_no,\n" +
                        "                yoin_kbn,\n" +
                        "                zei_flg,\n" +
                        "                shain_name,\n" +
                        "                kaisha_code,\n" +
                        "                kaisha_name,\n" +
                        "                soshiki_name,\n" +
                        "                keiyakukeitai_name,\n" +
                        "                sankaku_start_day,\n" +
                        "                sankaku_end_day,\n" +
                        "                sagyo_nengetsu,\n" +
                        "                sagyo_nengetsu_name,\n" +
                        "                furikomi_yotei_search_month,\n" +
                        "                furikomi_yotei_month,\n" +
                        "                kimmujikan || 'H' AS kimmujikan,\n" +
                        "                hachu_tanka_kbn,\n" +
                        "                hachu_tanka_kbn_name,\n" +
                        "                hachu_kosu,\n" +
                        "                hachu_tanka,\n" +
                        "                hachu_seisanjikan,\n" +
                        "                hachu_seisan_chouka,\n" +
                        "                hachu_seisan_koujyo,\n" +
                        "                hachu_tanka_total,\n" +
                        "                hachu_kabusoku_jikan,\n" +
                        "                hachu_kabusoku_seisan,\n" +
                        "                hachu_kotsuhi,\n" +
                        "                hachu_shukuhakuhi,\n" +
                        "                hachu_sonotakeihi,\n" +
                        "                teikiken_sum,\n" +
                        "                CASE\n" +
                        "                    WHEN yoin_kbn = '3' AND zei_flg = '1' THEN round((hachu_tanka_total + coalesce(hachu_kabusoku_seisan, 0)) * (1 - shouhi_rate) + coalesce(teikiken_sum, 0) + coalesce(hachu_kotsuhi, 0) + coalesce(hachu_shukuhakuhi, 0) + coalesce(hachu_sonotakeihi, 0), 0)\n" +
                        "                    ELSE round(hachu_tanka_total + coalesce(hachu_kabusoku_seisan, 0) + coalesce(teikiken_sum, 0) + coalesce(hachu_kotsuhi, 0) + coalesce(hachu_shukuhakuhi, 0) + coalesce(hachu_sonotakeihi, 0), 0)\n" +
                        "                END AS hachu_gokei_kingaku_nuki,\n" +
                        "                CASE\n" +
                        "                    WHEN yoin_kbn = '1' OR yoin_kbn = '2' THEN 0\n" +
                        "                    ELSE round(round(hachu_tanka_total + coalesce(hachu_kabusoku_seisan, 0), 0) * shouhi_rate, 0)\n" +
                        "                END AS zeikin\n" +
                        "            FROM\n" +
                        "                (\n" +
                        "                    SELECT\n" +
                        "                        anken_no,\n" +
                        "                        edaban,\n" +
                        "                        anken_name,\n" +
                        "                        yoin_no,\n" +
                        "                        yoin_kbn,\n" +
                        "                        zei_flg,\n" +
                        "                        shain_name,\n" +
                        "                        kaisha_code,\n" +
                        "                        kaisha_name,\n" +
                        "                        soshiki_name,\n" +
                        "                        keiyakukeitai_name,\n" +
                        "                        sankaku_start_day,\n" +
                        "                        sankaku_end_day,\n" +
                        "                        sagyo_nengetsu,\n" +
                        "                        substr(sagyo_nengetsu, 3, 2) || '年' || substr(sagyo_nengetsu, 5, 2) || '月' AS sagyo_nengetsu_name,\n" +
                        "                        to_char(furikomi_yotei_day, 'yyyyMM') AS furikomi_yotei_search_month,\n" +
                        "                        to_char(furikomi_yotei_day, 'yy年MM月') AS furikomi_yotei_month,\n" +
                        "                        kimmujikan,\n" +
                        "                        hachu_tanka_kbn,\n" +
                        "                        CASE\n" +
                        "                            WHEN hachu_tanka_kbn = '2' THEN '人月'\n" +
                        "                            ELSE '時間'\n" +
                        "                        END AS hachu_tanka_kbn_name,\n" +
                        "                        hachu_kosu,\n" +
                        "                        hachu_tanka,\n" +
                        "                        hachu_seisanjikan,\n" +
                        "                        hachu_seisan_chouka,\n" +
                        "                        hachu_seisan_koujyo,\n" +
                        "                        hachu_tanka_total,\n" +
                        "                        hachu_kabusoku_jikan,\n" +
                        "                        CASE\n" +
                        "                            WHEN hachu_kabusoku_jikan < 0 THEN hachu_kabusoku_jikan * hachu_seisan_koujyo\n" +
                        "                            WHEN hachu_kabusoku_jikan > 0 THEN hachu_kabusoku_jikan * hachu_seisan_chouka\n" +
                        "                            ELSE 0\n" +
                        "                        END AS hachu_kabusoku_seisan,\n" +
                        "                        hachu_kotsuhi,\n" +
                        "                        hachu_shukuhakuhi,\n" +
                        "                        hachu_sonotakeihi,\n" +
                        "                        teikiken_sum,\n" +
                        "                        shouhi_rate\n" +
                        "                    FROM\n" +
                        "                        (\n" +
                        "                            SELECT\n" +
                        "                                tjyk.anken_no,\n" +
                        "                                tjyk.edaban,\n" +
                        "                                tabi.anken_name,\n" +
                        "                                tjyk.yoin_no,\n" +
                        "                                yoin.koyokeitai AS yoin_kbn,\n" +
                        "                                yoin.zei_flg,\n" +
                        "                                yoin.shain_name,\n" +
                        "                                yoin.shozoku_gaisha_code AS kaisha_code,\n" +
                        "                                mk.ryakusho AS kaisha_name,\n" +
                        "                                mssk.soshiki_name,\n" +
                        "                                mc.code_name AS keiyakukeitai_name,\n" +
                        "                                to_char(yoin.sankaku_start_day, 'yyyyMM') AS sankaku_start_day,\n" +
                        "                                to_char(yoin.sankaku_end_day, 'yyyyMM') AS sankaku_end_day,\n" +
                        "                                tjyk.sagyo_nengetsu,\n" +
                        "                                CASE\n" +
                        "                                    WHEN yoin.koyokeitai = '4'\n" +
                        "                                AND tabi.hatchu_shiharai_saikuru = 30 THEN (date_trunc('month', TO_DATE(tjyk.sagyo_nengetsu || '01', 'YYYYMMDD')) + interval '2 month' - interval '1 day')::date\n" +
                        "                                    WHEN yoin.koyokeitai = '4'\n" +
                        "                                AND tabi.hatchu_shiharai_saikuru = 60 THEN (date_trunc('month', TO_DATE(tjyk.sagyo_nengetsu || '01', 'YYYYMMDD')) + interval '3 month' - interval '1 day')::date\n" +
                        "                                    WHEN yoin.koyokeitai = '4'\n" +
                        "                                AND tabi.hatchu_shiharai_saikuru > 30\n" +
                        "                                AND tabi.hatchu_shiharai_saikuru < 60 THEN (date_trunc('month', TO_DATE(tjyk.sagyo_nengetsu || '01', 'YYYYMMDD')) + interval '2 month' - interval '1 day')::date + tabi.hatchu_shiharai_saikuru::integer\n" +
                        "                                    WHEN yoin.koyokeitai <> '4' THEN (date_trunc('month', TO_DATE(tjyk.sagyo_nengetsu || '01', 'YYYYMMDD')) + interval '2 month' - interval '1 day')::date\n" +
                        "                               END AS furikomi_yotei_day,\n" +
                        "                                tykj.kimmujikan,\n" +
                        "                                yoin.gennka_tanka_kbn AS hachu_tanka_kbn,\n" +
                        "                                CASE\n" +
                        "                                    WHEN yoin.gennka_tanka_kbn = '2' THEN tjyk.gennka_kosu_ningetsu\n" +
                        "                                    ELSE tjyk.gennka_kosu_jikan\n" +
                        "                                END AS hachu_kosu,\n" +
                        "                                yoin.gennka_tanka AS hachu_tanka,\n" +
                        "                                yoin.hachu_seisanjikan,\n" +
                        "                                yoin.hachu_seisan_chouka,\n" +
                        "                                yoin.hachu_seisan_koujyo,\n" +
                        "                                CASE\n" +
                        "                                    WHEN yoin.gennka_tanka_kbn = '2' THEN round(tjyk.gennka_kosu_ningetsu * yoin.gennka_tanka, 0)\n" +
                        "                                    ELSE round(tykj.kimmujikan * yoin.gennka_tanka, 0)\n" +
                        "                                END AS hachu_tanka_total,\n" +
                        "                                CASE\n" +
                        "                                    WHEN yoin.gennka_tanka_kbn = '2'\n" +
                        "                                AND yoin.hachu_seisan_flag = '1'\n" +
                        "                                AND yoin.koyokeitai <> '1' AND yoin.koyokeitai <> '2'\n" +
                        "                                AND tykj.kimmujikan < round((yoin.hachu_seisanjikan_start * tjyk.gennka_kosu_ningetsu), 0) THEN tykj.kimmujikan - round((yoin.hachu_seisanjikan_start * tjyk.gennka_kosu_ningetsu), 0)\n" +
                        "                                    WHEN yoin.gennka_tanka_kbn = '2'\n" +
                        "                                AND yoin.hachu_seisan_flag = '1'\n" +
                        "                                AND round((yoin.hachu_seisanjikan_end * tjyk.gennka_kosu_ningetsu), 0) < tykj.kimmujikan THEN tykj.kimmujikan - round((yoin.hachu_seisanjikan_end * tjyk.gennka_kosu_ningetsu), 0)\n" +
                        "                                    ELSE 0\n" +
                        "                                END AS hachu_kabusoku_jikan,\n" +
                        "                                kotsuhi.kotuhi_sum AS hachu_kotsuhi,\n" +
                        "                                tujm.hachu_shukuhakuhi AS hachu_shukuhakuhi,\n" +
                        "                                tujm.hachu_sonotakeihi AS hachu_sonotakeihi,\n" +
                        "                                teikiken.teikiken_sum,\n" +
                        "                                shouhi.shouhi_rate\n" +
                        "                            FROM\n" +
                        "                                t_juchukou_yosan_kanri tjyk\n" +
                        "                                LEFT JOIN\n" +
                        "                                    t_yoinkimmu_joho tykj\n" +
                        "                                ON  tjyk.anken_no = tykj.anken_no\n" +
                        "                                AND tjyk.edaban = tykj.edaban\n" +
                        "                                AND tjyk.yoin_no = tykj.yoinn_no\n" +
                        "                                AND tjyk.sagyo_nengetsu = tykj.kuuyotei_start_month\n" +
                        "                                LEFT JOIN\n" +
                        "                                    t_juchu_mitsumori_joho tujm\n" +
                        "                                ON  tjyk.anken_no = tujm.anken_no\n" +
                        "                                AND tjyk.edaban = tujm.edaban\n" +
                        "                                AND tjyk.yoin_no = tujm.yoinn_no\n" +
                        "                                AND tjyk.sagyo_nengetsu = tujm.sagyo_nengetsu\n" +
                        "                                LEFT JOIN\n" +
                        "                                    (\n" +
                        "                                        SELECT\n" +
                        "                                            anken_no,\n" +
                        "                                            edaban,\n" +
                        "                                            kimmujikan,\n" +
                        "                                            yoinn_no,\n" +
                        "                                            sagyo_month,\n" +
                        "                                            kotuhi_sum\n" +
                        "                                        FROM\n" +
                        "                                            (\n" +
                        "                                                SELECT\n" +
                        "                                                    tempb.anken_no,\n" +
                        "                                                    tempb.edaban,\n" +
                        "                                                    tempb.kimmujikan,\n" +
                        "                                                    tempa.yoinn_no,\n" +
                        "                                                    tempa.sagyo_month,\n" +
                        "                                                    tempa.kotuhi_sum,\n" +
                        "                                                    ROW_NUMBER() over(partition BY tempa.yoinn_no, tempa.sagyo_month ORDER BY tempb.kimmujikan DESC) rowId\n" +
                        "                                                FROM\n" +
                        "                                                    (\n" +
                        "                                                        SELECT\n" +
                        "                                                            yoinn_no,\n" +
                        "                                                            to_char(day, 'yyyyMM') AS sagyo_month,\n" +
                        "                                                            SUM(hiyo) AS kotuhi_sum\n" +
                        "                                                        FROM\n" +
                        "                                                            t_yoinkotsuhi_joho\n" +
                        "                                                        WHERE\n" +
                        "                                                            hiyo_code = 'H00001'\n" +
                        "                                                        GROUP BY\n" +
                        "                                                            yoinn_no,\n" +
                        "                                                            to_char(day, 'yyyyMM')\n" +
                        "                                                    ) tempa\n" +
                        "                                                    LEFT JOIN\n" +
                        "                                                        t_yoinkimmu_joho tempb\n" +
                        "                                                    ON  tempb.yoinn_no = tempa.yoinn_no\n" +
                        "                                                    AND tempb.kuuyotei_start_month = tempa.sagyo_month\n" +
                        "                                            ) tempc\n" +
                        "                                        WHERE\n" +
                        "                                            rowId = 1\n" +
                        "                                    ) kotsuhi\n" +
                        "                                ON  tjyk.anken_no = kotsuhi.anken_no\n" +
                        "                                AND tjyk.edaban = kotsuhi.edaban\n" +
                        "                                AND tjyk.yoin_no = kotsuhi.yoinn_no\n" +
                        "                                AND tjyk.sagyo_nengetsu = kotsuhi.sagyo_month\n" +
                        "                                LEFT JOIN\n" +
                        "                                    (\n" +
                        "                                        SELECT\n" +
                        "                                            anken_no,\n" +
                        "                                            edaban,\n" +
                        "                                            kimmujikan,\n" +
                        "                                            yoinn_no,\n" +
                        "                                            sagyo_month,\n" +
                        "                                            teikiken_sum\n" +
                        "                                        FROM\n" +
                        "                                            (\n" +
                        "                                                SELECT\n" +
                        "                                                    teikib.anken_no,\n" +
                        "                                                    teikib.edaban,\n" +
                        "                                                    teikib.kimmujikan,\n" +
                        "                                                    teikia.yoinn_no,\n" +
                        "                                                    teikia.sagyo_month,\n" +
                        "                                                    teikia.teikiken_sum,\n" +
                        "                                                    ROW_NUMBER() over(partition BY teikia.yoinn_no, teikia.sagyo_month ORDER BY teikib.kimmujikan DESC) rowId\n" +
                        "                                                FROM\n" +
                        "                                                    (\n" +
                        "                                                        SELECT\n" +
                        "                                                            yoinn_no,\n" +
                        "                                                            MONTH AS sagyo_month,\n" +
                        "                                                            SUM(hiyo) AS teikiken_sum\n" +
                        "                                                        FROM\n" +
                        "                                                            t_yointeikiken_joho\n" +
                        "                                                        WHERE\n" +
                        "                                                            hiyo_code = 'H00002'\n" +
                        "                                                        GROUP BY\n" +
                        "                                                            yoinn_no,\n" +
                        "                                                            MONTH\n" +
                        "                                                    ) teikia\n" +
                        "                                                    LEFT JOIN\n" +
                        "                                                        t_yoinkimmu_joho teikib\n" +
                        "                                                    ON  teikib.yoinn_no = teikia.yoinn_no\n" +
                        "                                                    AND teikib.kuuyotei_start_month = teikia.sagyo_month\n" +
                        "                                            ) tempc\n" +
                        "                                        WHERE\n" +
                        "                                            rowId = 1\n" +
                        "                                    ) teikiken\n" +
                        "                                ON  tjyk.anken_no = teikiken.anken_no\n" +
                        "                                AND tjyk.edaban = teikiken.edaban\n" +
                        "                                AND tjyk.yoin_no = teikiken.yoinn_no\n" +
                        "                                AND tjyk.sagyo_nengetsu = teikiken.sagyo_month\n" +
                        "                                LEFT JOIN\n" +
                        "                                    m_shouhizei shouhi\n" +
                        "                                ON  to_char(shouhi.shouhi_start_day, 'yyyyMM') <= tjyk.sagyo_nengetsu\n" +
                        "                                AND (\n" +
                        "                                        shouhi.shouhi_end_day IS NULL\n" +
                        "                                    OR  to_char(shouhi.shouhi_end_day, 'yyyyMM') >= tjyk.sagyo_nengetsu\n" +
                        "                                    )\n" +
                        "                                LEFT JOIN\n" +
                        "                                    (\n" +
                        "                                        SELECT\n" +
                        "                                            tbshj.bp_shain_no AS shain_no,\n" +
                        "                                            tbshj.anken_no,\n" +
                        "                                            tbshj.edaban,\n" +
                        "                                            mbp.sei_kanji || mbp.mei_kanji AS shain_name,\n" +
                        "                                            tbshj.sankaku_start_day,\n" +
                        "                                            tbshj.sankaku_end_day,\n" +
                        "                                            tbshj.teianjokyo_flag,\n" +
                        "                                            tbshj.shozoku_soshiki_code,\n" +
                        "                                            mbskj.tekiyo_start_day,\n" +
                        "                                            mbskj.tekiyo_end_day,\n" +
                        "                                            '4' AS koyokeitai,\n" +
                        "                                            '2' AS zei_flg,\n" +
                        "                                            mbskj.shozoku_gaisha_code,\n" +
                        "                                            mbskj.seisan_flag AS hachu_seisan_flag,\n" +
                        "                                            mbskj.tanka_kbn AS gennka_tanka_kbn,\n" +
                        "                                            mbskj.tanka AS gennka_tanka,\n" +
                        "                                            mbskj.seisanjikan_start AS hachu_seisanjikan_start,\n" +
                        "                                            mbskj.seisanjikan_end AS hachu_seisanjikan_end,\n" +
                        "                                            CASE\n" +
                        "                                                WHEN mbskj.seisan_flag = '2' THEN '無'\n" +
                        "                                                WHEN mbskj.seisanjikan_start IS NOT NULL\n" +
                        "                                            AND mbskj.seisanjikan_end IS NOT NULL THEN mbskj.seisanjikan_start || 'H~' || mbskj.seisanjikan_end || 'H'\n" +
                        "                                                WHEN mbskj.seisanjikan_start IS NOT NULL THEN '開始:' || mbskj.seisanjikan_start || 'H'\n" +
                        "                                                WHEN mbskj.seisanjikan_end IS NOT NULL THEN '終了:' || mbskj.seisanjikan_end || 'H'\n" +
                        "                                                ELSE '無'\n" +
                        "                                            END AS hachu_seisanjikan,\n" +
                        "                                            mbskj.seisan_jouitannka AS hachu_seisan_chouka,\n" +
                        "                                            mbskj.seisan_kaitannka AS hachu_seisan_koujyo\n" +
                        "                                        FROM\n" +
                        "                                            t_bp_shain_hatchukanren_joho tbshj\n" +
                        "                                            LEFT JOIN\n" +
                        "                                                m_bp_shain_kihon_joho mbp\n" +
                        "                                            ON  tbshj.bp_shain_no = mbp.bp_shain_no\n" +
                        "                                            LEFT JOIN\n" +
                        "                                                m_bp_shain_koyokeiyaku_joho mbskj\n" +
                        "                                            ON  tbshj.bp_shain_no = mbskj.bp_shain_no\n" +
                        "                                            AND tbshj.sankaku_end_day >= mbskj.tekiyo_start_day\n" +
                        "                                            AND (\n" +
                        "                                                    mbskj.tekiyo_end_day IS NULL\n" +
                        "                                                OR  tbshj.sankaku_start_day <= mbskj.tekiyo_end_day\n" +
                        "                                                )\n" +
                        "                                            UNION ALL\n" +
                        "                                            SELECT\n" +
                        "                                                tsaj.shain_no,\n" +
                        "                                                tsaj.anken_no,\n" +
                        "                                                tsaj.edaban,\n" +
                        "                                                ms.sei_kanji || ms.mei_kanji AS shain_name,\n" +
                        "                                                tsaj.sankaku_start_day,\n" +
                        "                                                tsaj.sankaku_end_day,\n" +
                        "                                                tsaj.teianjokyo_flag,\n" +
                        "                                                tsaj.shozoku_soshiki_code,\n" +
                        "                                                mskj.tekiyo_start_day,\n" +
                        "                                                mskj.tekiyo_end_day,\n" +
                        "                                                mskj.koyokeitai,\n" +
                        "                                                mskj.koseinenkin_flag AS zei_flg,\n" +
                        "                                                ms.shozoku_gaisha_code,\n" +
                        "                                                mskj.seisan_flag AS hachu_seisan_flag,\n" +
                        "                                                mskj.tanka_kbn AS gennka_tanka_kbn,\n" +
                        "                                                CASE\n" +
                        "                                                    WHEN mskj.koyokeitai = '3' THEN mskj.gekkyu\n" +
                        "                                                    ELSE mskj.mitsumori_genka\n" +
                        "                                                END AS gennka_tanka,\n" +
                        "                                                mskj.seisanjikan_start AS hachu_seisanjikan_start,\n" +
                        "                                                mskj.seisanjikan_end AS hachu_seisanjikan_end,\n" +
                        "                                                CASE\n" +
                        "                                                    WHEN mskj.seisan_flag = '2' THEN '無'\n" +
                        "                                                    WHEN mskj.seisanjikan_start IS NOT NULL\n" +
                        "                                                AND mskj.seisanjikan_end IS NOT NULL THEN mskj.seisanjikan_start || 'H~' || mskj.seisanjikan_end || 'H'\n" +
                        "                                                    WHEN mskj.seisanjikan_start IS NOT NULL THEN '開始:' || mskj.seisanjikan_start || 'H'\n" +
                        "                                                    WHEN mskj.seisanjikan_end IS NOT NULL THEN '終了:' || mskj.seisanjikan_end || 'H'\n" +
                        "                                                    ELSE '無'\n" +
                        "                                                END AS hachu_seisanjikan,\n" +
                        "                                                mskj.seisan_jouitannka AS hachu_seisan_chouka,\n" +
                        "                                                mskj.seisan_kaitannka AS hachu_seisan_koujyo\n" +
                        "                                            FROM\n" +
                        "                                                t_shain_ankenkanren_joho tsaj\n" +
                        "                                            LEFT JOIN\n" +
                        "                                                m_shain_kihon_joho ms\n" +
                        "                                            ON  tsaj.shain_no = ms.shain_no\n" +
                        "                                            LEFT JOIN\n" +
                        "                                                m_shain_koyokeiyaku_joho mskj\n" +
                        "                                            ON  tsaj.shain_no = mskj.shain_no\n" +
                        "                                            AND tsaj.sankaku_end_day >= mskj.tekiyo_start_day\n" +
                        "                                            AND (\n" +
                        "                                                    mskj.tekiyo_end_day IS NULL\n" +
                        "                                                OR  tsaj.sankaku_start_day <= mskj.tekiyo_end_day\n" +
                        "                                                )\n" +
                        "                                    ) yoin\n" +
                        "                                ON  tjyk.anken_no = yoin.anken_no\n" +
                        "                                AND tjyk.edaban = yoin.edaban\n" +
                        "                                AND tjyk.yoin_no = yoin.shain_no\n" +
                        "                                AND tjyk.sagyo_nengetsu >= to_char(yoin.tekiyo_start_day, 'yyyyMM')\n" +
                        "                                AND (\n" +
                        "                                        yoin.tekiyo_end_day IS NULL\n" +
                        "                                    OR  tjyk.sagyo_nengetsu <= to_char(yoin.tekiyo_end_day, 'yyyyMM')\n" +
                        "                                    )\n" +
                        "                                AND tjyk.sagyo_nengetsu >= to_char(yoin.sankaku_start_day, 'yyyyMM')\n" +
                        "                                AND (\n" +
                        "                                        yoin.sankaku_end_day IS NULL\n" +
                        "                                    OR  tjyk.sagyo_nengetsu <= to_char(yoin.sankaku_end_day, 'yyyyMM')\n" +
                        "                                    )\n" +
                        "                                LEFT JOIN\n" +
                        "                                    t_anken_base_info tabi\n" +
                        "                                ON  tjyk.anken_no = tabi.anken_no\n" +
                        "                                AND tjyk.edaban = tabi.edaban\n" +
                        "                                LEFT JOIN\n" +
                        "                                    m_kaisha mk\n" +
                        "                                ON  mk.kaisha_code = yoin.shozoku_gaisha_code\n" +
                        "                                LEFT JOIN\n" +
                        "                                    m_soshiki mssk\n" +
                        "                                ON  mssk.soshiki_code = yoin.shozoku_soshiki_code\n" +
                        "                                LEFT JOIN\n" +
                        "                                    m_code mc\n" +
                        "                                ON  mc.code_id = 'C0023'\n" +
                        "                                AND mc.edaban = yoin.koyokeitai\n" +
                        "                            WHERE\n" +
                        "                                tjyk.delete_flag IS NULL\n" +
                        "                            AND tabi.juchu_jotai_flag IN('2', '4')\n" +
                        "                            AND (\n" +
                        "                                    tabi.sakujyo_flag IS NULL\n" +
                        "                                OR  tabi.sakujyo_flag = '0'\n" +
                        "                                )\n" +
                        "                            AND tjyk.anken_no = #{params.anken_no}\n" +
                        "                            AND tjyk.edaban = #{params.edaban}\n" +
                        "                            AND #{params.start_day} <= tjyk.sagyo_nengetsu\n" +
                        "                            AND tjyk.sagyo_nengetsu <= #{params.end_day}\n"
                ;

        sqlCommon +=
                " ORDER BY\n" +
                        "                                tjyk.yoin_no,\n" +
                        "                                tjyk.sagyo_nengetsu\n" +
                        "                        ) ordertempa\n" +
                        "                    ) ordertempb\n" +
                        "                ) ordertempb2\n" +
                        "            ) ordertempc\n"
        ;

        return sqlCommon;
    }

    /**
     * 実績金額一覧を作成
     **/
    private void setJisekiKingakuInfo(
            String ankenNo,
            String edaban,
            String[] showMonthList,
            List<Map<String, String>> resultList) {

        // 受注実績金額合計額一覧を取得
        Map<String, Object> toTalJisekiJuchuKingakuMap = getToTalJisekiJuchuKingakuList(
                ankenNo,
                edaban,
                showMonthList);

        // 発注実績金額合計額一覧を取得
        Map<String, Object> toTalJiseHachukiKingakuMap = getToTalJisekiHachuKingakuList(
                ankenNo,
                edaban,
                showMonthList);

        String[] monthJuchuJisekiList;
        String[] sumJuchuGokeiKingakus;
        String[] monthHachuJisekiList;
        String[] sumHachuGokeiKingakus;

        // 受注が存在する場合
        if (toTalJisekiJuchuKingakuMap == null) {
            monthJuchuJisekiList = new String[] { null };
            sumJuchuGokeiKingakus = new String[] { null };
        } else {
            //実績受注見積情報設定
            monthJuchuJisekiList = (String[])toTalJisekiJuchuKingakuMap.get("month_list");
            sumJuchuGokeiKingakus = (String[])toTalJisekiJuchuKingakuMap.get("sum_juchu_gokei_kingaku_list");
        }

        // 発注が存在する場合
        if (toTalJiseHachukiKingakuMap == null) {
            monthHachuJisekiList = new String[] { null };
            sumHachuGokeiKingakus = new String[] { null };
        } else {
            //実績発注見積情報設定
            monthHachuJisekiList = (String[])toTalJiseHachukiKingakuMap.get("month_list");
            sumHachuGokeiKingakus = (String[])toTalJiseHachukiKingakuMap.get("sum_hachu_gokei_kingaku_list");
        }

        // 実績受注高合計
        long jisekiJuchuSum = 0;
        // 実績粗利関連
        long jisekiSoriSum = 0;

        for (int j = 0; j < showMonthList.length; j++) {

            // 実績受注
            int a = monthJuchuJisekiList[0] == null ? -1 : Arrays.binarySearch(monthJuchuJisekiList, showMonthList[j]);

            if (a > -1) {
                if (!StringUtils.isEmpty(sumJuchuGokeiKingakus[a])) {
                    Map<String, String> monthInfo = resultList.get(j);
                    monthInfo.put("jiseki_juchu_value", sumJuchuGokeiKingakus[a]);
                    jisekiJuchuSum += Long.parseLong(sumJuchuGokeiKingakus[a]);
                }
            }

            // 粗利計算
            String strJisekiJuchu = resultList.get(j).get("jiseki_juchu_value");

            // 粗利計算対象は受注と発注額が全部存在する場合
            if (!"　".equals(strJisekiJuchu) || !"　".equals(strJisekiHachu)) {
                long jisekiJuchuValue = Long.parseLong("　".equals(strJisekiJuchu) ? "0" : strJisekiJuchu);
                long jisekiHachuValue = Long.parseLong("　".equals(strJisekiHachu) ? "0" : strJisekiHachu);
                long jisekiSoriValue = jisekiJuchuValue - jisekiHachuValue;

                Map<String, String> monthInfo = resultList.get(j);
                monthInfo.put("jiseki_cal_value", String.valueOf(jisekiSoriValue));

                jisekiSoriSum += jisekiSoriValue;
            }
        }

        // 実績受注合計
        Map<String, String> monthInfo = resultList.get(showMonthList.length);
        String sumJisekiJuchuValue = jisekiJuchuSum == 0 ? "　" : String.valueOf(jisekiJuchuSum);
        monthInfo.put("jiseki_juchu_value", sumJisekiJuchuValue);

        // 実績粗利合計
        String sumJisekiSoriValue = jisekiSoriSum == 0 ? "　" : String.valueOf(jisekiSoriSum);
        monthInfo.put("jiseki_cal_value", sumJisekiSoriValue);
    }

    /**
     * 発注実績金額合計額一覧を取得
     **/
    private Map<String, Object> getToTalJisekiHachuKingakuList(String ankenNo, String edaban, String[] showMonthList) {

        // 発注実績一覧共通SQL文
        String hachuJisekiSql = getOrderCommonSql();

        // 実績金額合計額一覧を取得
        String sql = "SELECT\n" +
                "    array_agg(sagyo_nengetsu ORDER BY sagyo_nengetsu)::text[] AS month_list,\n" +
                "    array_agg(sum_hachu_gokei_kingaku ORDER BY sagyo_nengetsu)::text[] AS sum_hachu_gokei_kingaku_list\n" +
                "FROM\n" +
                "    (\n" +
                "        SELECT\n" +
                "            ordertab.sagyo_nengetsu,\n" +
                "            round(SUM(ordertab.hachu_gokei_kingaku_nuki), 0) AS sum_hachu_gokei_kingaku\n" +
                "        FROM\n" +
                hachuJisekiSql +
                "        WHERE\n" +
                "            ordertab.anken_no = #{params.anken_no}\n" +
                "        AND ordertab.edaban = #{params.edaban}\n" +
                "        AND #{params.start_day} <= ordertab.sagyo_nengetsu\n" +
                "        AND ordertab.sagyo_nengetsu <= #{params.end_day}\n" +
                "        GROUP BY\n" +
                "            ordertab.sagyo_nengetsu\n" +
                "    ) sumkingaku"
                ;
        Map<String, Object> params = new HashMap<>();
        params.put("anken_no", ankenNo);
        params.put("edaban", Short.valueOf(edaban));
        params.put("start_day", showMonthList[0]);
        params.put("end_day", showMonthList[showMonthList.length-1]);

        Map<String, Object> rest  = sqlMapper.get(sql, params);
        return  rest;
    }

    /**
     * 受注実績金額合計額一覧を取得
     **/
    private Map<String, Object> getToTalJisekiJuchuKingakuList(String ankenNo, String edaban, String[] showMonthList) {

        // 実績金額合計額一覧を取得
        String sqlSelect = "SELECT\n" +
                "    array_agg(nohin_month ORDER BY nohin_month)::text[] AS month_list,\n" +
                "    array_agg(sum_juchu_gokei_kingaku ORDER BY nohin_month)::text[] AS sum_juchu_gokei_kingaku_list\n" +
                "FROM\n" +
                "    (\n" +
                "        SELECT\n" +
                "            nohin_month,\n" +
                "            round(SUM(jissekigokei_kingaku), 0) AS sum_juchu_gokei_kingaku\n" +
                "        FROM"
                ;

        // 設備情報一覧データ共通SQL文
        String sqlCommon = getOrderJisekiCommonSql();

        String sqlGroup =
                " GROUP BY\n" +
                        "            nohin_month\n" +
                        "    ) sumkingaku"
                ;
        Map<String, Object> params = new HashMap<>();
        params.put("anken_no", ankenNo);
        params.put("edaban", Short.valueOf(edaban));
        params.put("start_day", showMonthList[0]);
        params.put("end_day", showMonthList[showMonthList.length-1]);

        String sql = sqlSelect + sqlCommon + sqlGroup;

        Map<String, Object> rest  = sqlMapper.get(sql, params);
        return  rest;
    }

    /**
     * 実績金額一覧共通SQL文
     * */
    private String getOrderJisekiCommonSql() {
        // サブ部
        String sqlSelectSub = "(\n" +
                "    SELECT\n" +
                "        ROW_NUMBER() over(partition BY to_char(tujk.nohin_day, 'yyyyMM') ORDER BY tujk.edaban, tujk.nohin_day DESC) rowId,\n" +
                "        to_char(tujk.nohin_day, 'yyyyMM') AS nohin_month,\n" +
                "        tujk.jissekigokei_kingaku\n" +
                "    FROM\n" +
                "        t_uriage_jisseki_kanri tujk\n" +
                "    WHERE\n" +
                "        tujk.anken_no = #{params.anken_no}\n" +
                "    AND tujk.edaban = #{params.edaban}\n" +
                "    AND tujk.nohin_day IS NOT NULL\n" +
                "    AND #{params.start_day} <= to_char(tujk.nohin_day, 'yyyyMM')\n" +
                "    AND to_char(tujk.nohin_day, 'yyyyMM') <= #{params.end_day}\n" +
                ") jiseki\n"
                ;

        return sqlSelectSub;
    }
}
