package com.bestskip.business.web.form.employeeManager;

import lombok.Data;

import javax.validation.constraints.Size;

@Data
public class EmployeeSearchForm {

    /**
     * 勤務状態
     **/
    @Size(max = 5, min = 0)
    private String kinmuJyotai;
    /**
     * 部署コード
     **/
    @Size(max = 6, min = 0)
    private String bushoCode;
    /**
     * 技術者名
     **/
    @Size(max = 96, min = 0)
    private String gijutsuShaMei;
    /**
     * 在職状態
     **/
    private String zaishokuFlag;

    /**
     * 社員番号
     **/
    private String shain_no;
    /**
     * 空予定年月
     **/
    private String freeYM;
    /**
     * 社員区分
     **/
    private String shainKbn;
    /**
     * 案件NO
     **/
    private String ankenNo;
    /**
     * 枝番
     **/
    private String edaban;
    /**
     * 一覧画面追加種類(1:責任者 2:空要員)
     **/
    private String addType;

    /**
     * 表示年度
     **/
    private String memberRegisterSearch_showYm;

    /**
     * 表示フラグ
     **/
    private String memberRegisterSearch_nowFlag;
    /**
     * エラーメッセージ
     **/
    private String errorMsg;
}
