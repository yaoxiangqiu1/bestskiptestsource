package com.bestskip.business.web.form.employeeManager;

import java.util.Date;
import java.util.List;

import com.bestskip.business.web.bean.fileManager.FileInfo;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.web.multipart.MultipartFile;

@Data
public class EmployeeRegisterForm {

	// -------------------社員基本情報----------------------------
	/**
	 * 社員NO
	 *
	 */
	private String shainNo;

	/**
	 * 所属会社コード
	 *
	 */
	private String shozokuGaishaCode;

	/**
	 * 所属組織コード
	 *
	 */
	private String shozokuSoshikiCode;

	/**
	 * 権限コード
	 *
	 */
	@Size(max = 4, min = 4)
	private String kengenCode;

	/**
	 * 社員姓
	 *
	 */
	@NotEmpty
	@Size(max = 32, min = 0)
	private String seiKanji;

	/**
	 * 社員名
	 *
	 */
	@NotEmpty
	@Size(max = 64, min = 0)
	private String meiKanji;

	/**
	 * ひらがな（姓）
	 *
	 */
	@Size(max = 64, min = 0)
	private String seiKana;

	/**
	 * ひらがな（名）
	 *
	 */
	@Size(max = 128, min = 0)
	private String meiKana;

	/**
	 * 英文字（姓）
	 *
	 */
	@Size(max = 64, min = 0)
	private String seiEimoji;

	/**
	 * 英英文字（名）
	 *
	 */
	@Size(max = 128, min = 0)
	private String meiEimoji;

	/**
	 * 性別
	 *
	 */
	@Size(max = 5, min = 0)
	@NotEmpty
	private String sex;

	/**
	 * 国籍
	 *
	 */
	@Size(max = 5, min = 0)
	@NotEmpty
	private String kokusekiFlag;

	/**
	 * 婚姻状況
	 *
	 */
	@Size(max = 5, min = 0)
	private String koninJokyoFlag;

	/**
	 * 生年月日
	 *
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date seinengappi;

	/**
	 * 住所
	 *
	 */
	@Size(max = 512, min = 0)
	private String address;

	/**
	 * 連絡先
	 *
	 */
	@Size(max = 13, min = 0)
	private String renrakusaki;

	/**
	 * 学歴
	 *
	 */
	@Size(max = 5, min = 0)
	private String gakureki;

	/**
	 * 専攻
	 *
	 */
	@Size(max =128, min = 0)
	private String senko;

	/**
	 * 卒業年月
	 *
	 */
	@Size(max = 6, min = 0)
	private String sotsugyoYm;

	/**
	 * 最終卒業校
	 *
	 */
	@Size(max = 128, min = 0)
	private String saishuSotsugyoko;

	/**
	 * 在留カードNO
	 *
	 */
	@Size(max = 12, min = 0)
	private String zairyuCardNo;

	/**
	 * 在留期限
	 *
	 */
	private String zairyuKigen;

	/**
	 * 扶養家族人数
	 *
	 */
	private String fuyokazokuNinzu;

	/**
	 *
	 * @apiNote 書類パス
	 */
	private String uploadfileName;

	/**
	 *
	 * @apiNote 書類パス
	 */
	private List<MultipartFile> uploadfile;

	/**
	 *
	 * @apiNote 書類パス
	 */
	private List<FileInfo> fileInfos;

	/**
	 *
	 * @apiNote 日本語力
	 */
	@Size(max = 5, min = 0)
	private String nihongoLevel;

	/**
	 *
	 * @apiNote 技術力
	 */
	private String[] gijutsuryoku;

	/**
	 * 入社日
	 *
	 */
	@NotNull(message = "入社日を入力してください。")
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date nyushaDay;

	/**
	 * 退職日
	 *
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date taishokuDay;

	/**
	 * メールアドレス
	 *
	 */
	@Size(max = 128, min = 0)
	@Email
	private String mailAddress;

	// -------------------社員雇用契約情報----------------------------

	/**
	 *
	 * 枝番
	 */
	private Short edaban;

	/**
	 *
	 * 適用開始年月日
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date beforeTekiyoStartDay;

	/**
	 *
	 * 適用終了年月日
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date beforeTekiyoEndDay;

	/**
	 *
	 * 適用開始年月日
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date tekiyoStartDay;

	/**
	 *
	 * 適用終了年月日
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date tekiyoEndDay;

	/**
	 * 就業状況
	 *
	 */
	@Size(max = 5, min = 0)
	private String shugyoJokyoFlag;

	/**
	 * 職種
	 *
	 */
	@NotEmpty
	@Size(max = 5, min = 0)
	private String shokushuFlag;

	/**
	 * 役職
	 *
	 */
	@Size(max = 5, min = 0)
	private String yakushokuFlag;

	/**
	 *
	 * 雇用形態
	 */
	@NotEmpty
	@Size(max = 5, min = 0)
	private String koyokeitai;

	/**
	 *
	 * 厚生年金有無
	 */
	@Size(max = 5, min = 0)
	private String koseinenkinFlag;

	/**
	 *
	 * 月給
	 */
	@NumberFormat(style = NumberFormat.Style.CURRENCY)
	private String gekkyu;

	/**
	 *
	 * @apiNote 単価区分
	 */
	private String tankaKbn;

	/**
	 *
	 * 見積原価
	 */
	@NumberFormat(style = NumberFormat.Style.CURRENCY)
	private String mitsumoriGenka;

	/**
	 * 現在就業状況
	 *
	 */
	@Size(max = 5, min = 0)
	private String shugyoJokyoHenkouFlag;

	/**
	 * 契約フラグ
	 *
	 */
	private String keiyakuFlag;

	/**
	 * 氏名
	 */
	private String shainName;

	/**
	 *
	 * 精算時間STR
	 */
	private String seisanjikanStart;

	/**
	 *
	 * 精算時間END
	 */
	private String seisanjikanEnd;

	/**
	 *
	 * 精算下位単価
	 */
	private String seisanKaitannka;

	/**
	 *
	 * 精算上位単価
	 */
	private String seisanJouitannka;

	/**
	 *
	 * 要望
	 */
	@Size(max = 512, min = 0)
	private String yobo;
	/**
	 *
	 * 精算有無
	 */
	@Size(max = 5, min = 0)
	@NotEmpty
	private String seisanFlag;

	/**
	 * 職種
	 *
	 */
	private String beforeShokushuFlag;

	/**
	 * 役職
	 *
	 */
	private String beforeYakushokuFlag;

	/**
	 *
	 * 雇用形態
	 */
	private String beforeKoyokeitai;

	/**
	 *
	 * 厚生年金有無
	 */
	private String beforeKoseinenkinFlag;

	/**
	 *
	 * 単価区分
	 */
	private String beforeTankaKbn;

	/**
	 *
	 * 月給
	 */
	private String beforeGekkyu;

	/**
	 *
	 * 見積原価
	 */
	private String beforeMitsumoriGenka;

	/**
	 *
	 * @apiNote 精算時間（START）
	 */
	private String beforeSeisanjikanStart;

	/**
	 *
	 * @apiNote 精算時間（END）
	 */
	private String beforeSeisanjikanEnd;

	/**
	 *
	 * 精算下位単価
	 */
	private String beforeSeisanKaitannka;

	/**
	 *
	 * 精算上位単価
	 */
	private String beforeSeisanJouitannka;

	/**
	 *
	 * @apiNote 要望
	 */
	private String beforeYobo;

	/**
	 *
	 * 更新回数
	 */
	private Long koshinKaisu;

	/**
	 *
	 * 更新回数
	 */
	private Long koyouKoshinKaisu;

	/**
	 *
	 * エラーメッセージ
	 */
	private String errorMsg;

	/**
	 *
	 *
	 * @apiNote 契約情報状態
	 */
	private String status;

	/**
	 *原価表示フラグ
	 */
	private String genkaShow;

	/**
	 *
	 * 参照モード
	 */
	private String mode;

	private String confirmFlag;
	private String infoMsg;

}
