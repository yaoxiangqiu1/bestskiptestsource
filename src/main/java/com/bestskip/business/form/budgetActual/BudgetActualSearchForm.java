package com.bestskip.business.web.form.budgetActual;

import lombok.Data;

@Data
public class BudgetActualSearchForm {
    /**
     * 部署コード
     **/
    private String bushoCode;
    /**
     * 顧客会社コード
     **/
    private String kokyakuCode;
    /**
     * 所属会社コード
     **/
    private String shozokuCode;
    /**
     * 表示範囲
     **/
    private String hyojihaniFlg;
    /**
     * 案件No
     **/
    private String ankenNo;
    /**
     * 枝番
     **/
    private String edaban;
    /**
     * 現在のページ数
     **/
    private String nowPage;
    /**
     * ボタンアクション
     **/
    private String btnAction;
    /**
     * 表示年度
     **/
    private String showYm;
    /**
     * 現在のみフラグ
     **/
    private String nowFlag = "1";
    /**
     * 更新回数
     **/
    private Long koshinKaisu;
    /**
     * エラーメッセージ
     **/
    private String errorMsg;
    /**
     * 情報メッセージ
     **/
    private String infoMsg;
    /**
     * 作業開始月
     **/
    private String sagyoStartMonth;
    /**
     * 作業終了月
     **/
    private String sagyoEndMonth;
}
