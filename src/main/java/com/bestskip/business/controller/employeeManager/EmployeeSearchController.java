package com.bestskip.business.web.controller.employeeManager;

import com.bestskip.business.web.base.BaseController;
import com.bestskip.business.web.bean.employeeManager.EmployeeSearchBean;
import com.bestskip.business.web.bean.employeeManager.EmployeeSearchResultBean;
import com.bestskip.business.web.common.BusinessException;
import com.bestskip.business.web.common.Constants;
import com.bestskip.business.web.common.UrlConstants;
import com.bestskip.business.web.form.employeeManager.EmployeeSearchForm;
import com.bestskip.business.web.service.common.CommonService;
import com.bestskip.business.web.service.employee.EmployeeRegisterService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(UrlConstants.EMPLOYEE_ROOT)
public class EmployeeSearchController extends BaseController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private EmployeeRegisterService employeeRegisterService;

    @ModelAttribute
    public EmployeeSearchForm searchForm() {
        return new EmployeeSearchForm();
    }

    @Value("${com.bestskip.bussiness.web.app.common.message008}")
    private String checkMsg8;

    /**
     *画面初期化処理
     */
    @GetMapping(value = UrlConstants.EMPLOYEE_SEARCH)
    public String showRegister(
            @ModelAttribute EmployeeSearchForm form,
            HttpSession httpSession,
            HttpServletRequest request,
            Model model) {
        String shainNo = request.getParameter("shainNo");
        EmployeeSearchBean searchBean = new EmployeeSearchBean();

        if(shainNo != null){
            searchBean.setShainNo(shainNo);
        }

        // セッション値設定処理
        String gijutsuShaMei = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY + "gijutsuShaMei");
        String kinmuJyotai = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY + "kinmuJyotai");
        String bushoCode = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY + "bushoCode");
        String zaishokuFlag = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY + "zaishokuFlag");

        // 技術者名
        if (!StringUtils.isEmpty(gijutsuShaMei)) {
            form.setGijutsuShaMei(gijutsuShaMei);
        }

        // 勤務状態
        if (!StringUtils.isEmpty(kinmuJyotai)) {
            form.setKinmuJyotai(kinmuJyotai);
        }

        // 部署選択
        if (!StringUtils.isEmpty(bushoCode)) {
            form.setBushoCode(bushoCode);
        }

        // 在職状態
        if (!StringUtils.isEmpty(zaishokuFlag)) {
            form.setZaishokuFlag(zaishokuFlag);
        } else {
            form.setZaishokuFlag("1");
        }

        // コピー
        BeanUtils.copyProperties(form, searchBean);

        //検索
        List<EmployeeSearchResultBean> resultBeanList = employeeRegisterService.getEmployeeList(
                searchBean,
                getUserInfo());

        Gson gson = new GsonBuilder().serializeNulls().create();
        model.addAttribute("resultBeanList", gson.toJson(resultBeanList));

        return UrlConstants.EMPLOYEE_SEARCH_VIEW;
    }

    /**
     *部署一覧取得
     */
    @ModelAttribute("bushoCodeList")
    public Map<String, String> getBushoCodeList()
    {
        return commonService.getBushoCodeList(getUserInfo());
    }

    /**
     *検索処理
     */
    @PostMapping(value = UrlConstants.EMPLOYEE_SEARCH)
    public String search(@ModelAttribute @Validated EmployeeSearchForm form,
                         HttpSession httpSession,
                         BindingResult bindingResult,
                         Model model) {
        //自動チェック時、エラーある場合、自画面に戻る
        if (bindingResult.hasErrors()) {
            return UrlConstants.EMPLOYEE_SEARCH_VIEW;
        }
        EmployeeSearchBean searchBean = new EmployeeSearchBean();
        BeanUtils.copyProperties(form, searchBean);
        //検索
        List<EmployeeSearchResultBean> resultBeanList = employeeRegisterService.getEmployeeList(
                searchBean,
                getUserInfo());

        Gson gson = new GsonBuilder().serializeNulls().create();
        model.addAttribute("resultBeanList", gson.toJson(resultBeanList));

        //共通検索条件をセッションに保存
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY + "gijutsuShaMei", form.getGijutsuShaMei());
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY + "kinmuJyotai", form.getKinmuJyotai());
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY + "bushoCode", form.getBushoCode());
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY + "zaishokuFlag", form.getZaishokuFlag());

        return UrlConstants.EMPLOYEE_SEARCH_VIEW;
    }

    /**
     *社員情報登録へ（新規）
     */
    @PostMapping("employeeGoRegisterEntry")
    public String goRegister(@ModelAttribute @Validated EmployeeSearchForm form,
                             BindingResult bindingResult,
                             final RedirectAttributes redirectAttributes) {

        redirectAttributes.addAttribute("mode", Constants.INSERT_MODE);

        return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE);
    }

    /**
     *社員情報修正
     */
    @PostMapping("employeeGoChangeEntry")
    public String goChange(@ModelAttribute @Validated EmployeeSearchForm form,
                           BindingResult bindingResult,
                           final RedirectAttributes redirectAttributes) {

        redirectAttributes.addAttribute("shainNo", form.getShain_no());
        redirectAttributes.addAttribute("mode", Constants.CHANGE_MODE);

        return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE_FULL);
}

    /**
     *社員情報表示
     */
    @PostMapping("employeeGoDisplayEntry")
    public String goDisplay(@ModelAttribute @Validated EmployeeSearchForm form,
                            BindingResult bindingResult,
                            final RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("shainNo", form.getShain_no());
        redirectAttributes.addAttribute("mode", Constants.DISPLAY_MODE);

        return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE_CONFIRM_FULL);
    }

    /**
     *空要員登録
     */
    @PostMapping("employeeAddFreeEntry")
    public String addFree(@ModelAttribute @Validated EmployeeSearchForm form,
                          BindingResult bindingResult,
                          final RedirectAttributes redirectAttributes,
                          Model model) {
        try{
            commonService.addFreeYoin(getUserInfo(),form.getShain_no(),form.getShainKbn(),form.getFreeYM());
        }catch (BusinessException e){
            if("nullError".equals(e.getMessage())){
                form.setErrorMsg(form.getFreeYM()+"に有効な契約情報がない為、空要員の登録ができません。");
            }
            if("error".equals(e.getMessage())){
                form.setErrorMsg("空要員の登録が失敗しました。");
            }
        }

        // 空要員体制から遷移の判定
        if ("2".equals(form.getAddType())) {
            redirectAttributes.addAttribute("ankenNo", form.getAnkenNo());
            redirectAttributes.addAttribute("edaban", form.getEdaban());
            // 空要員体制
            return UrlConstants.REDIRECT_1.concat(UrlConstants.MEMBER_VACANT_FULL);
        }
        else {
            EmployeeSearchBean searchBean = new EmployeeSearchBean();
            BeanUtils.copyProperties(form, searchBean);
            //検索
            List<EmployeeSearchResultBean> resultBeanList = employeeRegisterService.getEmployeeList(
                    searchBean,
                    getUserInfo());

            Gson gson = new GsonBuilder().serializeNulls().create();
            model.addAttribute("resultBeanList", gson.toJson(resultBeanList));

            return UrlConstants.EMPLOYEE_SEARCH_VIEW;
        }
    }

    /**
     * 案件責任者追加
     */
    @PostMapping("addSekininshaEntry")
    public String addSekininshaEntry(
            @ModelAttribute EmployeeSearchForm form,
            BindingResult bindingResult,
            final RedirectAttributes redirectAttributes,
            Model model) {

        try {
            // 案件責任者追加
            commonService.addSekininsha(
                    form.getAnkenNo(),
                    form.getEdaban(),
                    form.getShain_no(),
                    form.getShainKbn());

        } catch (BusinessException e){
            //更新件数0件の場合
            if (Constants.ECP_01.equals(e.getErrorCode())) {
                form.setErrorMsg(checkMsg8);
            }
        }

        // 要員管理から遷移の判定
        if ("1".equals(form.getAddType())) {

            redirectAttributes.addAttribute("ankenNo", form.getAnkenNo());
            redirectAttributes.addAttribute("edaban", form.getEdaban());
            redirectAttributes.addAttribute("memberRegisterSearch_showYm", form.getMemberRegisterSearch_showYm());
            redirectAttributes.addAttribute("memberRegisterSearch_nowFlag", form.getMemberRegisterSearch_nowFlag());
            // 要員管理
            return UrlConstants.REDIRECT_1.concat(UrlConstants.MEMBER_REGISTER_FULL);
            // 空要員体制から遷移の判定
        } else {
            EmployeeSearchBean searchBean = new EmployeeSearchBean();
            BeanUtils.copyProperties(form, searchBean);
            //検索
            List<EmployeeSearchResultBean> resultBeanList = employeeRegisterService.getEmployeeList(
                    searchBean,
                    getUserInfo());

            Gson gson = new GsonBuilder().serializeNulls().create();
            model.addAttribute("resultBeanList", gson.toJson(resultBeanList));

            return UrlConstants.EMPLOYEE_SEARCH_VIEW;
        }
    }
	
	/**
     * 給与詳細表示
     */
    @PostMapping("addKyoyoEntry")
    public String addKyoyoEntry(
            @ModelAttribute EmployeeSearchForm form,
            BindingResult bindingResult,
            final RedirectAttributes redirectAttributes,
            Model model) {

        try {
            // 案件責任者追加
            commonService.addSekininsha(
                    form.getAnkenNo(),
                    form.getEdaban(),
                    form.getShain_no(),
                    form.getShainKbn());

        } catch (BusinessException e){
            //更新件数0件の場合
            if (Constants.ECP_01.equals(e.getErrorCode())) {
                form.setErrorMsg(checkMsg8);
            }
        }

        // 要員管理から遷移の判定
        if ("1".equals(form.getAddType())) {

            redirectAttributes.addAttribute("ankenNo", form.getAnkenNo());
            redirectAttributes.addAttribute("edaban", form.getEdaban());
            redirectAttributes.addAttribute("memberRegisterSearch_showYm", form.getMemberRegisterSearch_showYm());
            redirectAttributes.addAttribute("memberRegisterSearch_nowFlag", form.getMemberRegisterSearch_nowFlag());
            // 要員管理
            return UrlConstants.REDIRECT_1.concat(UrlConstants.MEMBER_REGISTER_FULL);
            // 空要員体制から遷移の判定
        } else {
            EmployeeSearchBean searchBean = new EmployeeSearchBean();
            BeanUtils.copyProperties(form, searchBean);
            //検索
            List<EmployeeSearchResultBean> resultBeanList = employeeRegisterService.getEmployeeList(
                    searchBean,
                    getUserInfo());

            Gson gson = new GsonBuilder().serializeNulls().create();
            model.addAttribute("resultBeanList", gson.toJson(resultBeanList));

            return UrlConstants.EMPLOYEE_SEARCH_VIEW;
        }
    }
}
