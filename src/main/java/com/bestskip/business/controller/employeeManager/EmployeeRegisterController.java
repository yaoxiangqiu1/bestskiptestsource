package com.bestskip.business.web.controller.employeeManager;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import com.bestskip.business.web.base.BaseController;
import com.bestskip.business.web.bean.fileManager.FileInfo;
import com.bestskip.business.web.common.Constants;
import com.bestskip.business.web.service.common.CommonService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.util.StringUtils;

import com.bestskip.business.web.common.Common;
import com.bestskip.business.web.common.UrlConstants;
import com.bestskip.business.web.form.employeeManager.EmployeeRegisterForm;
import com.bestskip.business.web.repository.model.MShainKihonJoho;
import com.bestskip.business.web.repository.model.MShainKoyokeiyakuJoho;
import com.bestskip.business.web.service.employee.EmployeeRegisterService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("employee")
public class EmployeeRegisterController extends BaseController {

	@Autowired
	private EmployeeRegisterService employeeRegisterService;
	@Autowired
	private CommonService commonService;

	@ModelAttribute
	public EmployeeRegisterForm employeeRegisterForm() {
		return new EmployeeRegisterForm();
	}

	/**
	 *画面初期化処理
	 */
	@GetMapping(value = UrlConstants.EMPLOYEE)
	public String showRegister(HttpServletRequest request, Model model,
							   @ModelAttribute EmployeeRegisterForm form
							   ) {
		String shainNo = request.getParameter("shainNo");
		String mode = request.getParameter("mode");

		// 単価区分デフォルト設定
		if (StringUtils.isEmpty(form.getTankaKbn())) {
			form.setTankaKbn(Constants.TANKA_KBN_GETSU);
		}

		// 修正モード
		if (!StringUtils.isEmpty(shainNo)
				&& Constants.CHANGE_MODE.equals(mode)) {
			MShainKihonJoho mShainKihonJoho;
			MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho;
			mShainKihonJoho = employeeRegisterService.getShinKihonJoho(shainNo);
			mShainKoyokeiyakuJoho = employeeRegisterService.getShinKoyokeiyakuJoho(shainNo);

			BeanUtils.copyProperties(mShainKoyokeiyakuJoho, form);
			BeanUtils.copyProperties(mShainKihonJoho, form);

			// 不一致する属性の項目の設定処理
			SetDifferentProperties(mShainKihonJoho, mShainKoyokeiyakuJoho, form);

			// 入力無しの場合、デフォルト設定
			if(StringUtils.isEmpty(form.getSeisanjikanStart())){
				form.setSeisanjikanStart(String.valueOf(Constants.SEISAN_START));
			}

			// 入力無しの場合、デフォルト設定
			if(StringUtils.isEmpty(form.getSeisanjikanEnd())) {
				form.setSeisanjikanEnd(String.valueOf(Constants.SEISAN_END));
			}

			// 技術力変換設定
			if (!StringUtils.isEmpty(mShainKihonJoho.getGijutsuryoku())) {
				form.setGijutsuryoku(mShainKihonJoho.getGijutsuryoku().split(","));
			}

			// 雇用契約情報設定処理
			SetOldKeiyakuInfo(form);
		}

		// 給与関連の権限設定処理
		setGenkaAuth(form);

		// 社員契約情報を取得処理
		List<Map<String, Object>> mShainKoyokeiyakuJohoList = employeeRegisterService.getMShainKoyokeiyakuJoho(form.getShainNo());
		model.addAttribute("shainKoyokeiyakuJohoList", mShainKoyokeiyakuJohoList);

		model.addAttribute("employeeRegisterForm", form);
		return UrlConstants.EMPLOYEE_VIEW;
	}

	/**
	 *部署一覧取得
	 */
	@ModelAttribute("bushoCodeList")
	public Map<String, String> getBushoCodeList()
	{
		return commonService.getBushoCodeList(getUserInfo());
	}

	/**
	 * 性別取得
	 */
	@ModelAttribute("sexList")
	public Map<String, String> getSexList() {
		return commonService.getSelectList(Constants.CD_C0002);
	}

	/**
	 * 国籍取得
	 */
	@ModelAttribute("kokusekiList")
	public Map<String, String> getKokusekiList() {
		return commonService.getSelectList(Constants.CD_C0019);
	}

	/**
	 * 日本語力
	 */
	@ModelAttribute("nihongoLevelList")
	public Map<String, String> getNihongoLevelList() {
		return commonService.getSelectList(Constants.CD_K0015);
	}

	/**
	 * 技術力
	 */
	@ModelAttribute("gijutsuryokuList")
	public Map<String, String> getGijutsuryokuList() {
		return commonService.getSelectList(Constants.CD_C0021);
	}

	/**
	 * 雇用形態取得
	 */
	@ModelAttribute("koyokeitaiList")
	public Map<String, String> getKoyokeitaiList() {
		return commonService.getSelectList(Constants.CD_C0001);
	}

	/**
	 * 職種取得
	 */
	@ModelAttribute("shokushuList")
	public Map<String, String> getShokushuList() {
		return commonService.getSelectList(Constants.CD_C0009);
	}

	/**
	 * 役職取得
	 */
	@ModelAttribute("yakushokuList")
	public Map<String, String> getYakushokuList() {

		Map<String, String> result = commonService.getSelectList(Constants.CD_C0010);
		result.remove(Constants.CODE_YAKUSHOKU_HOKA);
		return result;
	}

	/**
	 * 婚姻状態
	 */
	@ModelAttribute("koninJokyoList")
	public Map<String, String> getKoninJokyoList(){
		return commonService.getSelectList(Constants.CD_C0006);
	}

	/**
	 * 学歴
	 */
	@ModelAttribute("gakurekiList")
	public Map<String, String> getGakurekiList(){
		return commonService.getSelectList(Constants.CD_C0007);
	}

	/**
	 * 就業状況
	 */
	@ModelAttribute("shugyoJokyoList")
	public Map<String, String> getShugyoJokyoList(){
		return commonService.getSelectList(Constants.CD_C0011);
	}

	/**
	 * 就業状況変更
	 */
	@ModelAttribute("shugyoJokyoHenkouList")
	public Map<String, String> getShugyoJokyoHenkouList(){
		return commonService.getSelectList(Constants.CD_C0013);
	}

	@Value("${org.hibernate.validator.constraints.NotBlank.message}")
	private String checkMsg0;
	@Value("${com.bestskip.bussiness.web.app.common.message001}")
	private String checkMsg1;
	@Value("${org.hibernate.validator.constraints.Email.message}")
	private String checkMsg3;
	//	英文字で入力してください。
	@Value("${com.bestskip.bussiness.web.app.common.message004}")
	private String checkMsg5;
	//	数字で入力してください。
	@Value("${com.bestskip.bussiness.web.app.common.message005}")
	private String checkMsg6;
	//	漢字で入力してください。
	@Value("${com.bestskip.bussiness.web.app.common.message006}")
	private String checkMsg7;
	//	ひらがなで入力してください
	@Value("${com.bestskip.bussiness.web.app.common.message007}")
	private String checkMsg8;
	@Value("${com.bestskip.bussiness.web.app.common.message002}")
	private String checkMsg4;
	@Value("${com.bestskip.bussiness.web.app.common.message010}")
	private String checkMsg10;
	@Value("${com.bestskip.bussiness.web.app.common.message015}")
	private String checkMsg15;
	@Value("${com.bestskip.bussiness.web.app.common.message030}")
	private String checkMsg30;
	
	@PostMapping("employeeRegisterEntry")
	public String register(@ModelAttribute @Validated EmployeeRegisterForm form,
						   BindingResult bindingResult,
						   final RedirectAttributes redirectAttributes) {

		boolean hasError;

		// 社員チェック
		hasError = inputCheck(form, bindingResult);

		// 自動チェック時、エラーある場合、自画面に戻る
		if (bindingResult.hasErrors() || hasError) {
			return UrlConstants.EMPLOYEE_VIEW;
		}

		//社員姓名重複チェック
		if(!"1".equals(form.getConfirmFlag())) {
			if(!employeeRegisterService.checkName(form.getShainNo(),form.getSeiKanji(),form.getMeiKanji())){
				form.setInfoMsg(MessageFormat.format(checkMsg15, "該当社員姓名"));
				return UrlConstants.EMPLOYEE_VIEW;
			}
		}

		//単価など予算存在チェック
		if(!"1".equals(form.getConfirmFlag())) {

			// 登録ミスの訂正
			if (Constants.KEIYAKU_NAIYOU_UPDATE.equals(form.getKeiyakuFlag())) {

				// 受発注予算管理の件数取得
				Map<String, Object> rest = employeeRegisterService.getYosanJisekiCount(
						form.getShainNo(),
						form.getBeforeTekiyoStartDay());

				// 受発注予算管理が存在する場合
				if(Integer.parseInt(rest.get("count").toString()) > 0 &&
						(!form.getTankaKbn().equals(form.getBeforeTankaKbn())
								|| !form.getMitsumoriGenka().equals(form.getBeforeMitsumoriGenka()))) {
					form.setInfoMsg(MessageFormat.format(checkMsg30, Common.dateToSting(form.getBeforeTekiyoStartDay(), "yyyy年MM月dd日")));
					return UrlConstants.EMPLOYEE_VIEW;
				}

				// 契約期間変更の場合
			} else if (Constants.KEIYAKU_KIKAN_UPDATE.equals(form.getKeiyakuFlag())) {

				// 受発注予算管理の件数取得
				Map<String, Object> rest = employeeRegisterService.getYosanJisekiCount(
						form.getShainNo(),
						form.getTekiyoStartDay());

				// 受発注予算管理が存在する場合
				if(Integer.parseInt(rest.get("count").toString()) > 0 &&
						(!form.getTankaKbn().equals(form.getBeforeTankaKbn())
								|| !form.getMitsumoriGenka().equals(form.getBeforeMitsumoriGenka()))) {
					form.setInfoMsg(MessageFormat.format(checkMsg30, Common.dateToSting(form.getTekiyoStartDay(), "yyyy年MM月dd日")));
					return UrlConstants.EMPLOYEE_VIEW;
				}
			}
		}

		// ファイル内容保存処理
		setFileContent(form);

		// エラーがない場合、確認画面に遷移する
		redirectAttributes.addFlashAttribute("employeeRegisterForm", form);
		return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE_CONFIRM);
	}

	/**
	 * 給与関連の権限設定処理
	 */
	private void setGenkaAuth(EmployeeRegisterForm form) {
		// 権限設定
		form.setGenkaShow("0");

		//部署一覧取得
		Map<String, String> bushoList = getBushoCodeList();
		String yakushoKuCd = getUserInfo().getYakushokuCd();
		String shokushuFlg = getUserInfo().getShokushuFlag();
		String[] yakushoKuCdList = {"1","2","3","4","5","6","7"};

		//総務、経理の場合原価を表示する
		if(Constants.SHOKUSHU_CD_JINJI.equals(shokushuFlg) || Constants.SHOKUSHU_CD_KEIRI.equals(shokushuFlg)){
			form.setGenkaShow("1");
		}

		//課長以上且つ、自部門の社員の場合、原価を表示する
		if(Arrays.asList(yakushoKuCdList).contains(yakushoKuCd)
				&& (bushoList.containsKey(form.getShozokuSoshikiCode()) ||
				form.getShozokuSoshikiCode() == null || form.getShozokuSoshikiCode().isEmpty())){
			form.setGenkaShow("1");
		}
	}

	/**
	 * ファイル内容保存処理
	 */
	private void setFileContent(EmployeeRegisterForm form) {

		List<FileInfo> fileInfos = new ArrayList<>();
		for (MultipartFile file : form.getUploadfile()) {
			if (!StringUtils.isEmpty(file.getOriginalFilename())) {
				FileInfo fileInfo = new FileInfo();
				String fileName = Paths.get(file.getOriginalFilename()).getFileName().toString();
				try {
					fileInfo.setBytes(file.getBytes());
					fileInfo.setName(fileName);
				} catch (IOException e) {
					e.printStackTrace();
				}

				fileInfos.add(fileInfo);
			}
		}

		// 作成したファイル情報を保存
		form.setFileInfos(fileInfos);
	}

	/**
	 * 入力内容チェック
	 */
	private boolean inputCheck(EmployeeRegisterForm form, BindingResult bindingResult) {
		AtomicBoolean hasError = new AtomicBoolean();
		AtomicBoolean isSet = new AtomicBoolean();

		if (!StringUtils.isEmpty(form.getMailAddress())) {
			// 案件一括送信メールアドレスのフォーマットチェック
			List<String> listMail = Arrays.stream(form.getMailAddress().split(";")).map(String::trim)
					.collect(Collectors.toList());
			// メールではない場合、エラーメッセージを設定
			for (String s : listMail) {
				if (!Common.isEmail(s) && !isSet.get()) {
					bindingResult.rejectValue("MailAddress", MessageFormat.format(checkMsg3, "メールアドレス"),
							MessageFormat.format(checkMsg3, "メールアドレス"));
					hasError.set(true);
					isSet.set(true);
					break;
				}
			}
		}

		// 社員姓
		if (!StringUtils.isEmpty(form.getSeiKanji())
				&& !Common.isKanji(form.getSeiKanji())) {
			bindingResult.rejectValue("seiKanji", MessageFormat.format(checkMsg7, "社員姓"),
					MessageFormat.format(checkMsg7, "社員姓"));
			hasError.set(true);
		}

		// 社員名
		if (!StringUtils.isEmpty(form.getMeiKanji())
				&& !Common.isKanji(form.getMeiKanji())) {
			bindingResult.rejectValue("meiKanji", MessageFormat.format(checkMsg7, "社員名"),
					MessageFormat.format(checkMsg7, "社員名"));
			hasError.set(true);
		}

		// ひらかな（姓）
		if (!StringUtils.isEmpty(form.getSeiKana())
				&& !Common.isHirakana(form.getSeiKana())) {
			bindingResult.rejectValue("seiKana", MessageFormat.format(checkMsg8, "ひらがな（姓）"),
					MessageFormat.format(checkMsg8, "ひらがな（姓）"));
			hasError.set(true);
		}

		// ひらかな（名）
		if (!StringUtils.isEmpty(form.getMeiKana())
				&& !Common.isHirakana(form.getMeiKana())) {
			bindingResult.rejectValue("meiKana", MessageFormat.format(checkMsg8, "ひらがな（名）"),
					MessageFormat.format(checkMsg8, "ひらがな（名）"));
			hasError.set(true);
		}

		// 英文字（姓）
		if (!StringUtils.isEmpty(form.getSeiEimoji())
				&& !Common.isEimoji(form.getSeiEimoji())) {
			bindingResult.rejectValue("seiEimoji", MessageFormat.format(checkMsg5, "英文字（姓）"),
					MessageFormat.format(checkMsg5, "英文字（姓）"));
			hasError.set(true);
		}

		// 英文字（名）
		if (!StringUtils.isEmpty(form.getMeiEimoji())
				&& !Common.isEimoji(form.getMeiEimoji())) {
			bindingResult.rejectValue("meiEimoji", MessageFormat.format(checkMsg5, "英文字（名）"),
					MessageFormat.format(checkMsg5, "英文字（名）"));
			hasError.set(true);
		}

		// 正社員、契約社員
		if("1".equals(form.getKoyokeitai()) || "2".equals(form.getKoyokeitai())) {
			// 厚生年金有無
			if (StringUtils.isEmpty(form.getKoseinenkinFlag())) {
				bindingResult.rejectValue("koseinenkinFlag",
						MessageFormat.format(checkMsg0, "厚生年金有無"),
						MessageFormat.format(checkMsg0, "厚生年金有無"));
				hasError.set(true);
			}
			// 個人事業主
		} else if ("3".equals(form.getKoyokeitai())) {
			// 税金計算
			if (StringUtils.isEmpty(form.getKoseinenkinFlag())) {
				bindingResult.rejectValue("koseinenkinFlag",
						MessageFormat.format(checkMsg0, "税金計算"),
						MessageFormat.format(checkMsg0, "税金計算"));
				hasError.set(true);
			}
		}

		// 月給
		if (!StringUtils.isEmpty(form.getGekkyu())
				&& !Common.isNumber(form.getGekkyu())) {
			bindingResult.rejectValue("gekkyu", MessageFormat.format(checkMsg6, "月給"),
					MessageFormat.format(checkMsg6, "月給"));
			hasError.set(true);
		}

		// 見積原価
		if (!StringUtils.isEmpty(form.getMitsumoriGenka())
				&& !Common.isNumber(form.getMitsumoriGenka())) {
			bindingResult.rejectValue("mitsumoriGenka", MessageFormat.format(checkMsg6, "見積原価"),
					MessageFormat.format(checkMsg6, "見積原価"));
			hasError.set(true);
		}

		 //連絡先チェック
        if(!StringUtils.isEmpty(form.getRenrakusaki())
                && !Common.isphoneNo(form.getRenrakusaki())) {
            bindingResult.rejectValue("renrakusaki",
                    MessageFormat.format(checkMsg4,"連絡先"),
                    MessageFormat.format(checkMsg4,"連絡先"));
            hasError.set(true);
        }

		// 清算時間SRT
		if (!StringUtils.isEmpty(form.getSeisanjikanStart())
				&& !Common.isNumber(form.getSeisanjikanStart())) {
			bindingResult.rejectValue("seisanjikanStart",
					MessageFormat.format(checkMsg3,"清算時間SRT"),
					MessageFormat.format(checkMsg3,"清算時間SRT"));
			hasError.set(true);
		}

		// 精算時間END
		if (!StringUtils.isEmpty(form.getSeisanjikanEnd())
				&& !Common.isNumber(form.getSeisanjikanEnd())) {
			bindingResult.rejectValue("seisanjikanEnd",
					MessageFormat.format(checkMsg3,"精算時間END"),
					MessageFormat.format(checkMsg3,"精算時間END"));
			hasError.set(true);
		}

		// 新規の場合
		if (Constants.INSERT_MODE.equals(form.getMode())) {
			// 適用開始日
			if (StringUtils.isEmpty(form.getTekiyoStartDay())) {
				bindingResult.rejectValue("tekiyoStartDay",
						MessageFormat.format(checkMsg0, "適用開始日"),
						MessageFormat.format(checkMsg0, "適用開始日"));
				hasError.set(true);
			}

			// 適用開始日
			if(!StringUtils.isEmpty(form.getTekiyoStartDay())
					&& !StringUtils.isEmpty(form.getNyushaDay())
					&& form.getNyushaDay().after(form.getTekiyoStartDay())) {
				bindingResult.rejectValue("tekiyoStartDay",
						MessageFormat.format(checkMsg1,"適用開始日","入社日"),
						MessageFormat.format(checkMsg1,"適用開始日","入社日"));
				hasError.set(true);
			}

			// 適用終了日
			if(!StringUtils.isEmpty(form.getTekiyoEndDay())
					&& !StringUtils.isEmpty(form.getTekiyoStartDay())
					&& form.getTekiyoStartDay().compareTo(form.getTekiyoEndDay()) >= 0) {
				bindingResult.rejectValue("tekiyoEndDay",
						MessageFormat.format(checkMsg1,"適用終了日","適用開始日"),
						MessageFormat.format(checkMsg1,"適用終了日","適用開始日"));
				hasError.set(true);
			}
			// 変更無し以外の場合
		} else if (!Constants.KEIYAKU_NO_CHANGE.equals(form.getKeiyakuFlag())) {

			// 就業状況によりチャックを行う
			// 退職の場合
			if (Constants.STATUS_SHUGYO_TAISHOKU.equals(form.getShugyoJokyoHenkouFlag())) {

				// 退職日
				if (StringUtils.isEmpty(form.getTaishokuDay())) {
					bindingResult.rejectValue("taishokuDay",
							MessageFormat.format(checkMsg0, "退職日"),
							MessageFormat.format(checkMsg0, "退職日"));
					hasError.set(true);
				}

				if(!StringUtils.isEmpty(form.getTaishokuDay())
						&& !StringUtils.isEmpty(form.getNyushaDay())
						&& form.getNyushaDay().after(form.getTaishokuDay())) {
					bindingResult.rejectValue("taishokuDay",
							MessageFormat.format(checkMsg1,"退職日","入社日"),
							MessageFormat.format(checkMsg1,"退職日","入社日"));
					hasError.set(true);
				}
				// 契約期間変更の場合
			} else if (Constants.KEIYAKU_KIKAN_UPDATE.equals(form.getKeiyakuFlag())) {

				// 適用開始日
				if (StringUtils.isEmpty(form.getTekiyoStartDay())) {
					bindingResult.rejectValue("tekiyoStartDay",
							MessageFormat.format(checkMsg0, "変更後適用開始日"),
							MessageFormat.format(checkMsg0, "変更後適用開始日"));
					hasError.set(true);
				}

				// 適用終了日
				if(!StringUtils.isEmpty(form.getTekiyoEndDay())
						&& !StringUtils.isEmpty(form.getTekiyoStartDay())
						&& form.getTekiyoStartDay().compareTo(form.getTekiyoEndDay())>= 0) {
					bindingResult.rejectValue("tekiyoEndDay",
							MessageFormat.format(checkMsg1,"変更後適用終了日","変更後適用開始日"),
							MessageFormat.format(checkMsg1,"変更後適用終了日","変更後適用開始日"));
					hasError.set(true);
				}

				// 変更有、新規追加とみなす
				if (CheckKoyoKeiyakuInfo(form)) {

					// 変更後適用開始日(変更前適用終了日が自動計算のため、メッセージが開始日として表示)
					if(!StringUtils.isEmpty(form.getBeforeTekiyoEndDay())
							&& !StringUtils.isEmpty(form.getBeforeTekiyoStartDay())
							&& form.getBeforeTekiyoStartDay().compareTo(form.getBeforeTekiyoEndDay()) >= 0) {
						bindingResult.rejectValue("tekiyoStartDay",
								MessageFormat.format(checkMsg1,"変更後適用開始日の前日","変更前適用開始日"),
								MessageFormat.format(checkMsg1,"変更後適用開始日の前日","変更前適用開始日"));
						hasError.set(true);
					}

					// 変更あり
					form.setStatus(Constants.STATUS_HAS_CHANGE);
				} else {

					// 案件実績が存在チェック
					Map<String, Object> rest = employeeRegisterService.getYosanJisekiCount(
							form.getShainNo(),
							form.getBeforeTekiyoStartDay());

					// 始業月を取得
					String sagyoNengetsu = Common.dateToSting(form.getBeforeTekiyoStartDay(), "yyyy年MM月");

					// 案件実績が存在する場合
					if(Integer.parseInt(rest.get("count").toString()) > 0 ) {
						bindingResult.rejectValue("tekiyoStartDay",
								MessageFormat.format(checkMsg10, sagyoNengetsu + "からの案件実績が存在する", "適用開始日変更"),
								MessageFormat.format(checkMsg10, sagyoNengetsu + "からの案件実績が存在する", "適用開始日変更"));
						hasError.set(true);
					}

					// 前のレコードが存在チェック
					// 契約情報の前のレコードを取得
					MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho = employeeRegisterService.getMaeShainKoyokeiyakuJoho(form.getShainNo());
					// 前のレコードが存在する場合
					if (mShainKoyokeiyakuJoho != null) {

						if(!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getTekiyoEndDay())
								&& !StringUtils.isEmpty(form.getTekiyoStartDay())
								&& mShainKoyokeiyakuJoho.getTekiyoEndDay().compareTo(form.getBeforeTekiyoEndDay()) > 0) {

							if (mShainKoyokeiyakuJoho.getTekiyoStartDay().compareTo(form.getBeforeTekiyoEndDay()) >= 0) {
								bindingResult.rejectValue("tekiyoStartDay",
										MessageFormat.format(checkMsg8,"変更後適用開始日", form.getTekiyoStartDay()),
										MessageFormat.format(checkMsg8,"変更後適用開始日", form.getTekiyoStartDay()));
								hasError.set(true);
							}
						}

						// 変更なし（レコードあり）
						form.setStatus(Constants.STATUS_NO_CHANGE_DATA);
					} else {
						// 変更なし（レコードなし）
						form.setStatus(Constants.STATUS_NO_CHANGE_NO_DATA);
					}
				}
			}
		}

		return hasError.get();
	}

	/**
	 * 不一致する属性の項目の設定処理
	 * */
	private void SetDifferentProperties(MShainKihonJoho mShainKihonJoho, MShainKoyokeiyakuJoho mShainKoyokeiyakuJoho, EmployeeRegisterForm form) {

		// 扶養家族人数
		if (!StringUtils.isEmpty(mShainKihonJoho.getFuyokazokuNinzu())) {
			form.setFuyokazokuNinzu(mShainKihonJoho.getFuyokazokuNinzu().toString());
		}

		// 在留期限
		if (!StringUtils.isEmpty(mShainKihonJoho.getZairyuKigen())) {
			form.setZairyuKigen(mShainKihonJoho.getZairyuKigen().toString());
		}

		// 月給
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getGekkyu())) {
			form.setGekkyu(mShainKoyokeiyakuJoho.getGekkyu().toString());
		}

		// 見積原価
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getMitsumoriGenka())) {
			form.setMitsumoriGenka(mShainKoyokeiyakuJoho.getMitsumoriGenka().toString());
		}

		// 更新回数
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getKoshinKaisu())) {
			form.setKoyouKoshinKaisu(mShainKoyokeiyakuJoho.getKoshinKaisu());
		}

		// 適用開始日
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getTekiyoStartDay())) {
			form.setBeforeTekiyoStartDay(mShainKoyokeiyakuJoho.getTekiyoStartDay());
	    }

		// 適用終了年月日
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getTekiyoEndDay())) {
			form.setBeforeTekiyoEndDay(mShainKoyokeiyakuJoho.getTekiyoEndDay());
		}

		// 改修モードの場合
		if (Constants.CHANGE_MODE.equals(form.getMode())) {
			// 適用日クリア
			form.setTekiyoStartDay(null);
			form.setTekiyoEndDay(null);
		}

		// 契約選択状態
		if (StringUtils.isEmpty(form.getKeiyakuFlag())) {
			form.setKeiyakuFlag(Constants.KEIYAKU_NO_CHANGE);
		}

		// 精算時間STR
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanjikanStart())) {
			form.setSeisanjikanStart(mShainKoyokeiyakuJoho.getSeisanjikanStart().toString());
		}

		// 精算時間END
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanjikanEnd())) {
			form.setSeisanjikanEnd(mShainKoyokeiyakuJoho.getSeisanjikanEnd().toString());
		}

		// 控除単価
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanKaitannka())) {
			form.setSeisanKaitannka(mShainKoyokeiyakuJoho.getSeisanKaitannka().toString());
		}

		// 超過単価
		if (!StringUtils.isEmpty(mShainKoyokeiyakuJoho.getSeisanJouitannka())) {
			form.setSeisanJouitannka(mShainKoyokeiyakuJoho.getSeisanJouitannka().toString());
		}
	}

	/**
	 * 雇用契約情報設定処理
	 * */
	private void SetOldKeiyakuInfo(EmployeeRegisterForm form) {
		// 職種
		if (!StringUtils.isEmpty(form.getShokushuFlag())) {
			form.setBeforeShokushuFlag(form.getShokushuFlag());
		}

		// 役職
		if (!StringUtils.isEmpty(form.getYakushokuFlag())) {
			form.setBeforeYakushokuFlag(form.getYakushokuFlag());
		}

		// 厚生年金有無
		if (!StringUtils.isEmpty(form.getKoseinenkinFlag())) {
			form.setBeforeKoseinenkinFlag(form.getKoseinenkinFlag());
		}

		// 雇用形態
		if (!StringUtils.isEmpty(form.getKoyokeitai())) {
			form.setBeforeKoyokeitai(form.getKoyokeitai());
		}

		// 単価区分
		if (!StringUtils.isEmpty(form.getTankaKbn())) {
			form.setBeforeTankaKbn(form.getTankaKbn());
		}

		// 月給
		if (!StringUtils.isEmpty(form.getGekkyu())) {
			form.setBeforeGekkyu(form.getGekkyu());
		}

		// 見積原価
		if (!StringUtils.isEmpty(form.getMitsumoriGenka())) {
			form.setBeforeMitsumoriGenka(form.getMitsumoriGenka());
		}

		// 清算時間SRT
		if (!StringUtils.isEmpty(form.getSeisanjikanStart())) {
			form.setBeforeSeisanjikanStart(form.getSeisanjikanStart());
		}

		// 清算時間END
		if (!StringUtils.isEmpty(form.getSeisanjikanEnd())) {
			form.setBeforeSeisanjikanEnd(form.getSeisanjikanEnd());
		}

		// 控除単価
		if (!StringUtils.isEmpty(form.getSeisanKaitannka())) {
			form.setBeforeSeisanKaitannka(form.getSeisanKaitannka());
		}

		// 超過単価
		if (!StringUtils.isEmpty(form.getSeisanJouitannka())) {
			form.setBeforeSeisanJouitannka(form.getSeisanJouitannka());
		}

		// 本人要望
		if (!StringUtils.isEmpty(form.getYobo())) {
			form.setBeforeYobo(form.getYobo());
		}
	}

	/**
	 * 雇用契約情報いずれも変更された場合
	 * */
	private boolean CheckKoyoKeiyakuInfo(EmployeeRegisterForm form) {
		boolean isChange = false;

		if (Constants.CHANGE_MODE.equals(form.getMode())) {
			if (!StringUtils.isEmpty(form.getShugyoJokyoHenkouFlag())
					|| !form.getShokushuFlag().equals(form.getBeforeShokushuFlag())
					|| !form.getYakushokuFlag().equals(form.getBeforeYakushokuFlag())
					|| !form.getKoseinenkinFlag().equals(form.getBeforeKoseinenkinFlag())
					|| !form.getKoyokeitai().equals(form.getBeforeKoyokeitai())
					|| !form.getTankaKbn().equals(form.getBeforeTankaKbn())
					|| !form.getGekkyu().equals(form.getBeforeGekkyu())
					|| !form.getMitsumoriGenka().equals(form.getBeforeMitsumoriGenka())
					|| !form.getSeisanjikanStart().equals(form.getBeforeSeisanjikanStart())
					|| !form.getSeisanjikanEnd().equals(form.getBeforeSeisanjikanEnd())
					|| !form.getSeisanKaitannka().equals(form.getBeforeSeisanKaitannka())
					|| !form.getSeisanJouitannka().equals(form.getBeforeSeisanJouitannka())
					|| !form.getYobo().equals(form.getBeforeYobo())) {
				isChange = true;
			}
		}

		return isChange;
	}

	/**
	 *戻る
	 */
	@PostMapping("employeeBackEntry")
	public String goBack(@ModelAttribute EmployeeRegisterForm form,
						 HttpServletRequest request,
						 BindingResult bindingResult,
						 final RedirectAttributes redirectAttributes) {

		return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE_SEARCH_FULL);
	}
}
