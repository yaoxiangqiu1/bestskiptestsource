package com.bestskip.business.web.controller.employeeManager;

import java.util.List;
import java.util.Map;

import com.bestskip.business.web.base.BaseController;
import com.bestskip.business.web.common.Constants;
import com.bestskip.business.web.service.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bestskip.business.web.common.UrlConstants;
import com.bestskip.business.web.form.employeeManager.EmployeeRegisterForm;
import com.bestskip.business.web.service.employee.EmployeeRegisterService;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("employee")
public class EmployeeCompletionController extends BaseController {
    @Autowired
    private EmployeeRegisterService employeeRegisterService;
	@Autowired
	private CommonService commonService;

    @ModelAttribute
    public EmployeeRegisterForm employeeRegisterForm() {
        return new EmployeeRegisterForm();
    }

	/**
	 *画面初期化処理
	 */
	@GetMapping(value = UrlConstants.EMPLOYEE_COMPLETION)
	public String showCompletion(HttpServletRequest request, Model model,
								 @ModelAttribute EmployeeRegisterForm form) {

		// 社員契約情報を取得処理
		List<Map<String, Object>> mShainKoyokeiyakuJohoList = employeeRegisterService.getMShainKoyokeiyakuJoho(form.getShainNo());
		model.addAttribute("shainKoyokeiyakuJohoList", mShainKoyokeiyakuJohoList);

		return UrlConstants.EMPLOYEE_COMPLETION_VIEW;
	}

	/**
	 *部署一覧取得
	 */
	@ModelAttribute("bushoCodeList")
	public Map<String, String> getBushoCodeList()
	{
		return commonService.getBushoCodeList(getUserInfo());
	}

	/**
	 * 性別取得
	 */
	@ModelAttribute("sexList")
	public Map<String, String> getSexList() {
		return commonService.getSelectList(Constants.CD_C0002);
	}

	/**
	 * 国籍取得
	 */
	@ModelAttribute("kokusekiList")
	public Map<String, String> getKokusekiList() {
		return commonService.getSelectList(Constants.CD_C0019);
	}

	/**
	 * 日本語力
	 */
	@ModelAttribute("nihongoLevelList")
	public Map<String, String> getNihongoLevelList() {
		return commonService.getSelectList(Constants.CD_K0015);
	}

	/**
	 * 技術力
	 */
	@ModelAttribute("gijutsuryokuList")
	public Map<String, String> getGijutsuryokuList() {
		return commonService.getSelectList(Constants.CD_C0021);
	}

	/**
	 * 雇用形態取得
	 */
	@ModelAttribute("koyokeitaiList")
	public Map<String, String> getKoyokeitaiList() {
		return commonService.getSelectList(Constants.CD_C0001);
	}

	/**
	 * 職種取得
	 */
	@ModelAttribute("shokushuList")
	public Map<String, String> getShokushuList() {
		return commonService.getSelectList(Constants.CD_C0009);
	}

	/**
	 * 役職取得
	 */
	@ModelAttribute("yakushokuList")
	public Map<String, String> getYakushokuList() {

		Map<String, String> result = commonService.getSelectList(Constants.CD_C0010);
		result.remove(Constants.CODE_YAKUSHOKU_HOKA);
		return result;
	}

	/**
	 * 婚姻状態
	 */
	@ModelAttribute("koninJokyoList")
	public Map<String, String> getKoninJokyoList(){
		return commonService.getSelectList(Constants.CD_C0006);
	}

	/**
	 * 学歴
	 */
	@ModelAttribute("gakurekiList")
	public Map<String, String> getGakurekiList(){
		return commonService.getSelectList(Constants.CD_C0007);
	}

	/**
	 * 就業状況
	 */
	@ModelAttribute("shugyoJokyoList")
	public Map<String, String> getShugyoJokyoList(){
		return commonService.getSelectList(Constants.CD_C0011);
	}

	/**
	 * 就業状況変更
	 */
	@ModelAttribute("shugyoJokyoHenkouList")
	public Map<String, String> getShugyoJokyoHenkouList(){
		return commonService.getSelectList(Constants.CD_C0013);
	}

	/**
	 * 社員一覧に遷移
	 */
	@PostMapping("employeeFromComToRegEntry")
	public String goSearch(@ModelAttribute EmployeeRegisterForm form,
						   BindingResult bindingResult,
						   final RedirectAttributes redirectAttributes) {
		return UrlConstants.REDIRECT_1.concat(UrlConstants.EMPLOYEE_SEARCH);
	}

   
}
