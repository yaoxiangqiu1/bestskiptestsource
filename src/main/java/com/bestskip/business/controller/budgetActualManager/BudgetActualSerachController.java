package com.bestskip.business.web.controller.budgetActualManager;

import com.bestskip.business.web.base.BaseController;
import com.bestskip.business.web.common.Common;
import com.bestskip.business.web.common.Constants;
import com.bestskip.business.web.common.UrlConstants;
import com.bestskip.business.web.form.budgetActual.BudgetActualSearchForm;
import com.bestskip.business.web.service.budgetActual.BudgetActualService;
import com.bestskip.business.web.service.common.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@PropertySource(value = {"classpath:ValidationMessages.properties"})
@RequestMapping("budgetActual")
public class BudgetActualSerachController extends BaseController {
    @Autowired
    private CommonService commonService;

    @Autowired
    private BudgetActualService budgetActualService;

    @Value("${com.bestskip.bussiness.web.app.common.message008}")
    private String checkMsg8;

    @Value("${com.bestskip.bussiness.web.app.common.message009}")
    private String checkMsg9;

    @Value("${com.bestskip.bussiness.web.app.common.message010}")
    private String checkMsg10;

    @ModelAttribute
    public BudgetActualSearchForm budgetActualSearchForm() {
        return new BudgetActualSearchForm();
    }

    /**
     *部署一覧取得
     */
    @ModelAttribute("bushoCodeList")
    public Map<String, String> getBushoCodeList()
    {
        return commonService.getBushoCodeList(getUserInfo());
    }

    /**
     *顧客会社一覧取得
     */
    @ModelAttribute("kokyakuCodeList")
    public Map<String, String> getKokyakuCodeList()
    {
        return commonService.getKaishaCodeList("2,3");
    }

    /**
     *所属会社一覧取得
     */
    @ModelAttribute("shozokuCodeList")
    public Map<String, String> getShozokuCodeList()
    {
        return commonService.getKaishaCodeList("1,3");
    }

    /**
     *画面初期化処理
     */
    @GetMapping(value = UrlConstants.BUDGETACTUAL_SEARCH)
    public String showRegister(HttpServletRequest request, HttpSession httpSession, Model model) {
        String bushoCode = request.getParameter("budgetActualSearch_bushoCode");
//        String showYm = request.getParameter(Constants.COMMON_SESSION_KEY+"showYm");
//        String nowFlag = request.getParameter(Constants.COMMON_SESSION_KEY+"nowFlag");
        String showYm = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY+"showYm");
        String nowFlag = (String) httpSession.getAttribute(Constants.COMMON_SESSION_KEY+"nowFlag");

        //初期検索
        List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
        Map<String, String> bushoList = commonService.getBushoCodeList(getUserInfo());

        //表示年月を設定
        String DayFormart ="yyyyMM";
        BudgetActualSearchForm budgetActualSearchForm = new BudgetActualSearchForm();

        if(bushoCode != null || showYm != null || nowFlag != null) {
            budgetActualSearchForm.setBushoCode(bushoCode != null ? bushoCode :
                    bushoList.entrySet().stream().findFirst().get().getKey());
            budgetActualSearchForm.setShowYm(showYm.replace("年度",""));
            budgetActualSearchForm.setNowFlag(nowFlag);
            //表示年月を設定
            //--現在から半年の場合
            if ("1".equals(budgetActualSearchForm.getNowFlag())){
                budgetActualSearchForm.setShowYm(Common.getNow(LocalDate.now(),"yyyy"));
                model.addAttribute("mounthList", Common.getMonthList(Common.getNow(LocalDate.now(),DayFormart),6));
                budgetActualSearchForm.setSagyoStartMonth(null);
                budgetActualSearchForm.setSagyoEndMonth(null);
            }else if (!StringUtils.isEmpty(budgetActualSearchForm.getSagyoStartMonth())){
                long count = Common.monthDiff(budgetActualSearchForm.getSagyoStartMonth()+"/01",
                        budgetActualSearchForm.getSagyoEndMonth()+"/01","yyyy/MM/dd")+1;
                model.addAttribute("mounthList",
                        Common.getMonthList(budgetActualSearchForm.getSagyoStartMonth().
                                replaceAll("/",""),(int) count));
                //表示年度設定
                budgetActualSearchForm.setShowYm(budgetActualSearchForm.getSagyoStartMonth().split("/")[0]);
                budgetActualSearchForm.setNowFlag("0");
            }else{
                model.addAttribute("mounthList", Common.getMonthList(budgetActualSearchForm.getShowYm()+"04",12));
            }
            // 案件一覧取得
            listMap = budgetActualService.getAnkenList(
                    budgetActualSearchForm.getBushoCode(),
                    "",
                    "",
                    budgetActualSearchForm.getHyojihaniFlg(),
                    budgetActualSearchForm.getShowYm().replace("年度",""),
                    budgetActualSearchForm.getNowFlag(),
                    budgetActualSearchForm.getSagyoStartMonth(),
                    budgetActualSearchForm.getSagyoEndMonth());

            model.addAttribute("projectList", listMap);
            model.addAttribute("budgetActualSearchForm", budgetActualSearchForm);

        } else {

            String[] showMonthList = Common.getMonthList(Common.getNow(LocalDate.now(),DayFormart),6);
            model.addAttribute("mounthList", showMonthList);
            //初期年度設定
            budgetActualSearchForm.setShowYm(Common.getNow(LocalDate.now(),"yyyy"));
            budgetActualSearchForm.setNowFlag("1");
            model.addAttribute("budgetActualSearchForm", budgetActualSearchForm);
            //先頭の部署コードを設定
            String bushoCd = bushoList.entrySet().stream().findFirst().get().getKey();

            // 案件一覧取得
            listMap = budgetActualService.getAnkenList(
                    bushoCd,
                    "",
                    "",
                    "1",
                    "",
                    "1",
                    "",
                    "");
        }

        //合計行生成
        List<Map<String,Object>> showlistMap = createSumLine(listMap);
        model.addAttribute("projectList", showlistMap);

        //共通検索条件をセッションに保存
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY+"showYm", budgetActualSearchForm.getShowYm());
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY+"nowFlag", budgetActualSearchForm.getNowFlag());

        return UrlConstants.BUDGETACTUAL_SEARCH_VIEW;
    }

    /**
     *検索処理
     */
    @PostMapping("budgetActualSearchEntry")
    public String search(@ModelAttribute @Validated BudgetActualSearchForm form,
                         HttpSession httpSession,
                         BindingResult bindingResult,
                         Model model) {
        //自動チェック時、エラーある場合、自画面に戻る
        if (bindingResult.hasErrors()) {
            return UrlConstants.BUDGETACTUAL_SEARCH_VIEW;
        }
        //表示年月を設定
        String DayFormart ="yyyyMM";
        if ("1".equals(form.getNowFlag())){
            form.setShowYm(Common.getNow(LocalDate.now(),"yyyy"));
            model.addAttribute("mounthList", Common.getMonthList(Common.getNow(LocalDate.now(),DayFormart),6));
            form.setSagyoStartMonth(null);
            form.setSagyoEndMonth(null);
        }else if (!StringUtils.isEmpty(form.getSagyoStartMonth())){
            long count = Common.monthDiff(form.getSagyoStartMonth()+"/01",
                    form.getSagyoEndMonth()+"/01","yyyy/MM/dd")+1;
            model.addAttribute("mounthList",
                    Common.getMonthList(form.getSagyoStartMonth().
                            replaceAll("/",""),(int) count));
            //表示年度設定
            form.setShowYm(form.getSagyoStartMonth().split("/")[0]);
            form.setNowFlag("0");
        } else {
            model.addAttribute("mounthList", Common.getMonthList(form.getShowYm().replace("年度","")+"04",12));
        }
        //検索結果初期化
        List<Map<String,Object>> listMap;

        listMap = budgetActualService.getAnkenList(form.getBushoCode(),
                form.getKokyakuCode(),
                form.getShozokuCode(),
                form.getHyojihaniFlg(),
                form.getShowYm().replace("年度",""),
                form.getNowFlag(),
                form.getSagyoStartMonth(),
                form.getSagyoEndMonth());

        //合計行生成
        List<Map<String,Object>> showlistMap = createSumLine(listMap);
        model.addAttribute("projectList", showlistMap);

        //共通検索条件をセッションに保存
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY+"showYm",form.getShowYm().replace("年度",""));
        httpSession.setAttribute(Constants.COMMON_SESSION_KEY+"nowFlag",form.getNowFlag());
        return UrlConstants.BUDGETACTUAL_SEARCH_VIEW;
    }

    /**
     *合計行算出
     */
    private  List<Map<String,Object>> createSumLine(List<Map<String,Object>> listMap) {

        List<Map<String,Object>> returnList = new ArrayList<>();
        Map<String,Object> returnMap;

        List<Map<String, String>> countList = (List<Map<String, String>>) listMap.get(0).get("mounthList");
        int count = countList.size();

        //合計（金額）
        List<Map<String, String>> sumList = initSumList(count);
        //小計（金額）
        List<Map<String, String>> ankenSumList = initSumList(count);
        //合計（人数）
        List<Map<String, String>> sumMemberList = initSumList(count);
        //小計（人数）
        List<Map<String, String>> ankenSumMemberList = initSumList(count);

        String oldAnkenNo = listMap.get(0).get("anken_no").toString();

        for(int j = 0;j < listMap.size();j++) {

            String ankenNo = listMap.get(j).get("anken_no").toString();
            Map<String,Object> mapInfo = listMap.get(j);
            List<Map<String, String>> resultList = (List<Map<String, String>>) mapInfo.get("mounthList");

            // 月ごとに繰り返して総合計値を算出
            for(int i=0; i < resultList.size(); i++) {

                // 月毎に合計計算処理
                setAllSumList(
                        resultList.get(i),
                        sumList.get(i),
                        sumMemberList.get(i),
                        ankenSumList.get(i),
                        ankenSumMemberList.get(i),
                        oldAnkenNo,
                        ankenNo);
            }

            if(!oldAnkenNo.equals(ankenNo)) {

                //元の案件Noを再設定
                oldAnkenNo = ankenNo;
                //案件情報リセット
                returnMap = listMap.get(j-1).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99996");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList", ankenSumMemberList);
                returnMap.put("ankenSum","小計（人数）");
                //案件の合計行追加
                returnList.add(returnMap);

                // 粗利率を設定処理
                setSoriRate(ankenSumList);

                //案件情報リセット
                returnMap = listMap.get(j-1).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99997");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList",ankenSumList);
                returnMap.put("ankenSum","小計（金額）");
                //案件の合計行追加
                returnList.add(returnMap);
                //新案件情報追加
                returnList.add(listMap.get(j));
                //小計配列を再設定
                ankenSumList = initSumList(count);
                ankenSumMemberList = initSumList(count);

                // 月ごとに繰り返して総合計値を算出
                for(int k=0; k < resultList.size(); k++) {

                    // 月毎に合計計算処理
                    resetAnkenSumList(
                            resultList.get(k),
                            ankenSumList.get(k),
                            ankenSumMemberList.get(k));
                }

            } else {
                returnList.add(listMap.get(j));
            }

            //最終行の場合、小計行と総合計行を追加する
            if(j == listMap.size() - 1) {
                //案件情報リセット
                returnMap = listMap.get(j).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99996");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList", ankenSumMemberList);
                returnMap.put("ankenSum","小計（人数）");
                //案件の合計行追加
                returnList.add(returnMap);

                // 粗利率を設定処理
                setSoriRate(ankenSumList);

                //案件情報リセット
                returnMap = listMap.get(j).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99997");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList",ankenSumList);
                returnMap.put("ankenSum","小計（金額）");
                //案件の合計行追加
                returnList.add(returnMap);

                //案件情報リセット
                returnMap = listMap.get(j).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99998");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList",sumMemberList);
                returnMap.put("sum","合計（人数）");
                //案件の合計行追加
                returnList.add(returnMap);

                // 粗利率を設定処理
                setSoriRate(sumList);

                //案件情報リセット
                returnMap = listMap.get(j).entrySet().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                returnMap.put("edaban","99999");
                returnMap.put("anken_name","");
                returnMap.put("soshiki_name","");
                returnMap.put("ryakusho","");
                returnMap.put("tantosha_name","");
                returnMap.put("name","");
                returnMap.put("kaisyaryakusho","");
            	returnMap.put("kyuyo","");
                returnMap.put("cell_flg", "0");
                returnMap.put("mounthList",sumList);
                returnMap.put("sum","合計（金額）");
                //案件の合計行追加
                returnList.add(returnMap);
            }
        }

        return  returnList;
    }

    /**
     * 合計計算初期情報設定
     * */
    private List<Map<String, String>> initSumList(int count) {

        List<Map<String, String>> resultList = new ArrayList<>();

        for (int j = 0;j < count; j++) {

            Map<String, String> mapsInfo = new HashMap<>();
            mapsInfo.put("yosan_juchu_value", "　");
            mapsInfo.put("yosan_hachu_value", "　");
            mapsInfo.put("yosan_cal_value", "　");
            mapsInfo.put("jiseki_juchu_value", "　");
            mapsInfo.put("jiseki_hachu_value", "　");
            mapsInfo.put("jiseki_cal_value", "　");
            resultList.add(mapsInfo);
        }

        return resultList;
    }

    /**
     * 粗利率を設定処理
     * */
    private void setSoriRate(List<Map<String, String>> totalList) {

        for (int i = 0; i < totalList.size(); i++) {

            Map<String, String> mapsInfo = totalList.get(i);
            String yosanJuchuValue = mapsInfo.getOrDefault("yosan_juchu_value", "0");
            String yosanCalValue = mapsInfo.getOrDefault("yosan_cal_value", "0");
            String jisekiJuchuValue = mapsInfo.getOrDefault("jiseki_juchu_value", "0");
            String jisekiCalValue = mapsInfo.getOrDefault("jiseki_cal_value", "0");

            // 予算粗利率を算出
            String yosanCalRate;
            if (!StringUtils.isEmpty(yosanJuchuValue)
                    && !"0".equals(replace(yosanJuchuValue))) {
                yosanCalRate = percentInstance(yosanCalValue, yosanJuchuValue);
            } else {
                yosanCalRate = "0%";
            }

            mapsInfo.put("yosan_cal_value", yosanCalValue + "/" + yosanCalRate);

            // 実績粗利率を算出
            String jisekiCalRate;
            if (!StringUtils.isEmpty(jisekiJuchuValue)
                    && !"0".equals(replace(jisekiJuchuValue))) {
                jisekiCalRate = percentInstance(jisekiCalValue, jisekiJuchuValue);
            } else {
                jisekiCalRate = "0%";
            }

            mapsInfo.put("jiseki_cal_value", jisekiCalValue + "/" + jisekiCalRate);

        }
    }

    /**
     * パーセント計算
     * @return
     */
    private String percentInstance(String soriNumber, String juchuNumber) {
        long bfTotalNumber = Long.parseLong(replace(soriNumber));
        long zcTotalNumber = Long.parseLong(replace(juchuNumber));
        double percent = bfTotalNumber * 1.0 / zcTotalNumber ;
        //フォーマット取得
        NumberFormat nt = NumberFormat.getPercentInstance();
        //二桁を保持
        nt.setMinimumFractionDigits(0);
        return nt.format(percent);
    }

    /**
     * パーセント計算
     * @return
     */
    private String replace(String value) {
        return value.replaceAll(",", "").replaceAll(" ","").replaceAll("　","");
    }

    /**
     * 月毎に合計計算処理
     * */
    private void setAllSumList(Map<String, String> kingakuInfo,
                               Map<String, String> sumInfo,
                               Map<String, String> sumMemberInfo,
                               Map<String, String> ankenSumInfo,
                               Map<String, String> ankenSumMemberInfo,
                               String oldAnkenNo,
                               String ankenNo) {

        // 月毎に各項目合計計算処理(予算受注額)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_juchu_value",
                oldAnkenNo,
                ankenNo);

        // 月毎に各項目合計計算処理(予算発注額)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_hachu_value",
                oldAnkenNo,
                ankenNo);

        // 月毎に各項目合計計算処理(予算粗利)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_cal_value",
                oldAnkenNo,
                ankenNo);

        // 月毎に各項目合計計算処理(実績受注額)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_juchu_value",
                oldAnkenNo,
                ankenNo);

        // 月毎に各項目合計計算処理(実績発注額)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_hachu_value",
                oldAnkenNo,
                ankenNo);

        // 月毎に各項目合計計算処理(実績粗利)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_cal_value",
                oldAnkenNo,
                ankenNo);
                               	
        // 月毎に各項目合計計算処理(給与)
        setSumList(kingakuInfo,
                sumInfo,
                sumMemberInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "kyuyo_cal_value",
                oldAnkenNo,
                ankenNo);
    }

    /**
     * 月毎に各項目合計計算処理
     * */
    private void setSumList(Map<String, String> kingakuInfo,
                            Map<String, String> sumInfo,
                            Map<String, String> sumMemberInfo,
                            Map<String, String> ankenSumInfo,
                            Map<String, String> ankenSumMemberInfo,
                            String name,
                            String oldAnkenNo,
                            String ankenNo) {

        String kingakuStrValue = kingakuInfo.getOrDefault(name, "");
        String sumStrValue = sumInfo.getOrDefault(name, "");
        String sumMemberStrValue = sumMemberInfo.getOrDefault(name, "");
        String ankenSumStrValue = ankenSumInfo.getOrDefault(name, "");
        String ankenSumMemberStrValue = ankenSumMemberInfo.getOrDefault(name, "");

        long sumValue = (sumStrValue == null || sumStrValue.equals("") || sumStrValue.equals("　")) ? 0:
                Long.parseLong(sumStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　",""));

        long nowValue = (kingakuStrValue == null || kingakuStrValue.equals("") || kingakuStrValue.equals("　")) ? 0:
                Long.parseLong(kingakuStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　",""));

        sumStrValue = String.valueOf(sumValue + nowValue);

        long sumMemberValue = (sumMemberStrValue == null || sumMemberStrValue.equals("") || sumMemberStrValue.equals("　")) ? 0:
                Long.parseLong(sumMemberStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","").replaceAll("名",""));

        long nowMemberValue =(kingakuStrValue == null || kingakuStrValue.equals("") || kingakuStrValue.equals("　")) ? 0:1;

        sumMemberStrValue =  String.valueOf(sumMemberValue + nowMemberValue);

        //案件番号同じの場合
        if(oldAnkenNo.equals(ankenNo)) {
            long ankenSumValue = (ankenSumStrValue == null || ankenSumStrValue.equals("") || ankenSumStrValue.equals("　")) ? 0:
                    Long.parseLong(ankenSumStrValue.replaceAll(",","")
                            .replaceAll(" ","").replaceAll("　",""));

            ankenSumStrValue =  String.valueOf(ankenSumValue + nowValue);

            long ankenSumMemberValue =  (ankenSumMemberStrValue == null || ankenSumMemberStrValue.equals("") || ankenSumMemberStrValue.equals("　")) ? 0:
                    Long.parseLong(ankenSumMemberStrValue.replaceAll(",", "")
                            .replaceAll(" ","").replaceAll("　", "").replaceAll("名", ""));

            ankenSumMemberStrValue =  String.valueOf(ankenSumMemberValue + nowMemberValue);
        }

        //カンマ表示
        kingakuStrValue =  (kingakuStrValue == null || kingakuStrValue == "" || kingakuStrValue == "　") ? "":
                String.format("%,d", Long.parseLong(kingakuStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","")));
        sumStrValue = (sumStrValue == null || sumStrValue == "" || sumStrValue == "　") ? "0":
                String.format("%,d", Long.parseLong(sumStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","")));
        ankenSumStrValue = (ankenSumStrValue == null || ankenSumStrValue == "" || ankenSumStrValue == "　") ? "0":
                String.format("%, d", Long.parseLong(ankenSumStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","")));
        sumMemberStrValue = (sumMemberStrValue == null || sumMemberStrValue == "" || sumMemberStrValue == "　") ? "0名":
                String.format("%,d", Long.parseLong(sumMemberStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","").replaceAll("名","")))+"名";
        ankenSumMemberStrValue = (ankenSumMemberStrValue == null || ankenSumMemberStrValue == "" || ankenSumMemberStrValue == "　") ? "0":
                String.format("%, d", Long.parseLong(ankenSumMemberStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　","").replaceAll("名","")))+"名";

        kingakuInfo.put(name, kingakuStrValue);
        sumInfo.put(name, sumStrValue);
        sumMemberInfo.put(name, sumMemberStrValue);
        ankenSumInfo.put(name, ankenSumStrValue);
        ankenSumMemberInfo.put(name, ankenSumMemberStrValue);
    }

    /**
     * 月毎に合計計算処理
     * */
    private void resetAnkenSumList(Map<String, String> kingakuInfo,
                                   Map<String, String> ankenSumInfo,
                                   Map<String, String> ankenSumMemberInfo) {

        // 月毎に各項目合計計算処理(予算受注額)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_juchu_value");

        // 月毎に各項目合計計算処理(予算発注額)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_hachu_value");

        // 月毎に各項目合計計算処理(予算粗利)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "yosan_cal_value");

        // 月毎に各項目合計計算処理(実績受注額)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_juchu_value");

        // 月毎に各項目合計計算処理(実績発注額)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_hachu_value");

        // 月毎に各項目合計計算処理(実績粗利)
        setAnkenSumList(kingakuInfo,
                ankenSumInfo,
                ankenSumMemberInfo,
                "jiseki_cal_value");
    }

    /**
     * 月毎に各項目合計計算処理
     * */
    private void setAnkenSumList(Map<String, String> kingakuInfo,
                                 Map<String, String> ankenSumInfo,
                                 Map<String, String> ankenSumMemberInfo,
                                 String name) {

        String kingakuStrValue = kingakuInfo.getOrDefault(name, "");
        String ankenSumStrValue = ankenSumInfo.getOrDefault(name, "");
        String ankenSumMemberStrValue = ankenSumMemberInfo.getOrDefault(name, "");

        long nowValue = (kingakuStrValue == null || kingakuStrValue.equals("") || kingakuStrValue.equals("　")) ? 0:
                Long.parseLong(kingakuStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　",""));

        long nowMemberValue =(kingakuStrValue == null || kingakuStrValue.equals("") || kingakuStrValue.equals("　")) ? 0:1;

        long ankenSumValue = (ankenSumStrValue == null || ankenSumStrValue.equals("") || ankenSumStrValue.equals("　")) ? 0:
                Long.parseLong(ankenSumStrValue.replaceAll(",","")
                        .replaceAll(" ","").replaceAll("　",""));

        ankenSumStrValue =  String.valueOf(ankenSumValue + nowValue);

        long ankenSumMemberValue =  (ankenSumMemberStrValue == null || ankenSumMemberStrValue.equals("") || ankenSumMemberStrValue.equals("　")) ? 0:
                Long.parseLong(ankenSumMemberStrValue.replaceAll(",", "")
                        .replaceAll(" ","").replaceAll("　", "").replaceAll("名", ""));

        ankenSumMemberStrValue =  String.valueOf(ankenSumMemberValue + nowMemberValue);

        ankenSumInfo.put(name, ankenSumStrValue);
        ankenSumMemberInfo.put(name, ankenSumMemberStrValue);
    }
}