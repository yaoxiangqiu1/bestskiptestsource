<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../head.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ja">
<head>
    <script>
        var sessionkey = 'budgetActualSearch_';
        var commonSessionkey = 'commonSearch_';
        var table;
        var monthLength = ${fn:length(mounthList)};
        var monthColumns =[];
        for (var i=6;i<=(6+monthLength*6+1);i++){
            monthColumns.push(i);
        }
        var tableShowValue =localStorage.getItem(sessionkey+'table_show');
        $(document).ready(function () {

            $('#serachBtn').click(function () {
                if(($('#sagyoStartMonth').val() != '' && ($('#sagyoEndMonth').val() == '' || $('#sagyoEndMonth').val() == null)) ||
                    ($('#sagyoEndMonth').val() != '' && ($('#sagyoStartMonth').val() == '' || $('#sagyoStartMonth').val() == null))){
                    alert('作業開始月と作業終了月をセットで設定してください。');
                    return true;
                }
                if($('#sagyoStartMonth').val() != '' && $('#sagyoEndMonth').val() != ''){
                    if ($('#sagyoStartMonth').val().replace('/','') > $('#sagyoEndMonth').val().replace('/','') ){
                        alert('作業終了月は作業開始月以後に設定してください。');
                        return true;
                    }
                }
                localStorage.setItem(sessionkey+'table_show', $('#table_show').attr('class'));
                saveSerachInfo();
                $('#btnAction').val('search');
                if ($('#sagyoStartMonth').val() != '' && $('#sagyoStartMonth').val() != null){
                    $('#nowFlag').val('');
                }
                $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/budgetActual/budgetActualSearchEntry');
                $('#searchForm').submit();
            });

            $('#showYm').datetimepicker({
                dayViewHeaderFormat: 'YYYY年',
                format: 'YYYY年度',
                locale: 'ja'
            });

            // 作業開始月
            $('#sagyoStartMonth').datetimepicker({
                dayViewHeaderFormat: 'YYYY年',
                format: 'YYYY/MM',
                locale: 'ja'
            });

            // 作業終了月
            $('#sagyoEndMonth').datetimepicker({
                dayViewHeaderFormat: 'YYYY年',
                format: 'YYYY/MM',
                locale: 'ja'
            });

            $('#showYm').on('dp.update', function(e){ changeYM('',0); });

            table = $('#myTable')
                .on( 'init.dt', function () {
                    if(tableShowValue != null && tableShowValue !== '' && tableShowValue.indexOf('fa-plus') !== -1){
                        $('#table_show').removeClass('fa-minus');
                        $('#table_show').addClass('fa-plus');
                        $('.showWidth').css('border-right-width','1px');
                        table.column(3).visible(false);
                        table.column(4).visible(false);
                        table.column(5).visible(false);
                        table.column(6).visible(false);
                        table.column(7).visible(false);
                    }
                } )
                .DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Japanese.json"
                    },
                    lengthMenu: [50, 100, 150, 200,500 ],
                    scrollX: true,
                    scrollY: 500,
                    displayLength: 500,
                    scrollCollapse: true,
                    //自動列幅計算
                    bAutoWidth: true,
                    "columnDefs": [
                        { "sortable": false, "targets": monthColumns }
                    ],
                    //列固定
                    fixedColumns: {
                        leftColumns: 8
                    },
                    //列幅定義
                    rowsGroup: [0,1,2,3,4,5],
                    createdRow: function ( row, data, index ) {
                        if(data[0] === '合計（金額）' || data[0] === '合計（人数）'){
                            //$('td', row).eq(0).attr('colspan',"2");
                            //$('td', row).eq(1).remove();
                            $('td', row).eq(0).css('border-right-width','0px');
                            $('td', row).eq(1).css('border-right-width','0px');
                            $('td', row).eq(2).css('border-right-width','0px');
                            $('td', row).eq(1).addClass('showWidth');
                            $('td', row).eq(3).css('border-right-width','0px');
                            $('td', row).eq(4).css('border-right-width','0px');
                            $('td', row).eq(5).css('border-right-width','0px');
                            $('td', row).eq(6).text('');
                        }

                        if(data[2].indexOf("小計") !== -1) {
                            // $('td', row).eq(1).attr('colspan',"2");
                            // $('td', row).eq(2).remove();
                            $('td', row).eq(1).css('border-right-width','0px');
                            $('td', row).eq(2).css('border-right-width','0px');
                            $('td', row).eq(1).addClass('showWidth');
                            $('td', row).eq(3).css('border-right-width','0px');
                            $('td', row).eq(4).css('border-right-width','0px');
                            $('td', row).eq(5).css('border-right-width','0px');
                            $('td', row).eq(1).text('');
                            $('td', row).eq(6).text('');
                        }
                    },
                    dom: 'Blfrtip',
                    buttons: [ {
                        extend: 'excel',
                        text: 'EXCEL出力',
                        className: 'btn btn-secondary btn-sm',
                        filename: '受発注予実情報一覧',
                        sheetName: '一覧',
                        title: '',
                        autoFilter: true,
                        footer:true,
                        exportOptions:{
                            format: {
                                body: function ( data, row, column, node ) {
                                    var index = data.search('<span class');
                                    if(index !== -1) {
                                        return data.substring(0, index).trim();
                                    }
                                    else{
                                        if ('' === data) {
                                            return ' ';
                                        } else {
                                            return data;
                                        }
                                    }
                                }
                            }
                        },
                        customize: function(xlsx) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            //All cells
                            $('row c', sheet).attr('s', '25');
                            //First row
                            $('row:first c', sheet).attr('s', '47');
                        }
                    }]
            });

            //行選択
            // $('#myTable tbody').on('click', 'tr', function() {
            //     var ankenNo = '';
            //     var edaban = '';
            //     var koshinKaisu = 0;
            //     if ($(this).hasClass('selected')) {
            //         $(this).removeClass('selected');
            //         $('#ankenNo').val(ankenNo);
            //         $('#edaban').val(edaban);
            //         $('#koshinKaisu').val(koshinKaisu);
            //     } else {
            //         table.$('tr.selected').removeClass('selected');
            //         $(this).addClass('selected');
            //         ankenNo = $(this).find('.ankenNo').text();
            //         edaban = $(this).find('.edaban').text();
            //         koshinKaisu = $(this).find('.koshinKaisu').text();
            //         $('#ankenNo').val(ankenNo);
            //         $('#edaban').val(edaban);
            //         $('#koshinKaisu').val(koshinKaisu);
            //     }
            // });

            //表示件数変更時
            $('#myTable').on( 'length.dt', function ( e, settings, len ) {
                //alert( 'New page length: '+len );
            } );
            //ページ数変更時
            $('#myTable').on( 'page.dt', function () {
                var info = table.page.info();
                //alert( 'Showing page: '+info.page+' of '+info.pages );
            } );

            //案件情報削除
            $("#deletebtn").click(function() {
                deleteAnken();
            });
        });

        //表示年度変更
        function changeYM(ym,dim) {
            $('#sagyoStartMonth').val('');
            $('#sagyoEndMonth').val('');
            localStorage.setItem(sessionkey+'table_show', $('#table_show').attr('class'));
            $('#showYm').val($('#showYm').val().replace('年度',''));
            $('#nowFlag').val('0');
            $('#btnAction').val('search');
            saveSerachInfo();
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/budgetActual/budgetActualSearchEntry');
            $('#searchForm').submit();
        }

        //該当月に変更
        function changeToNow (){
            $('#sagyoStartMonth').val('');
            $('#sagyoEndMonth').val('');
            localStorage.setItem(sessionkey+'table_show', $('#table_show').attr('class'));
            $('#showYm').val($('#showYm').val().replace('年度',''));
            $('#nowFlag').val('1');
            $('#btnAction').val('search');
            saveSerachInfo();
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/budgetActual/budgetActualSearchEntry');
            $('#searchForm').submit();
        }

        //該当年度に変更
        function changeToThisYm (){
            $('#sagyoStartMonth').val('');
            $('#sagyoEndMonth').val('');
            localStorage.setItem(sessionkey+'table_show', $('#table_show').attr('class'));
            var now = new Date()
            $('#showYm').val(now.getFullYear());
            $('#nowFlag').val('0');
            $('#btnAction').val('search');
            saveSerachInfo();
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/budgetActual/budgetActualSearchEntry');
            $('#searchForm').submit();
        }

        //検索条件保存
        function saveSerachInfo() {
            localStorage.setItem(sessionkey+'bushoCode', $('#bushoCode').val());
            localStorage.setItem(commonSessionkey+'showYm', $('#showYm').val());
            localStorage.setItem(commonSessionkey+'nowFlag', $('#nowFlag').val());
        }

        //表列の表示非表示
        function tableShow() {
            if($('.showTh').hasClass('fa-minus')){
                $('.showTh').removeClass('fa-minus');
                $('.showTh').addClass('fa-plus');
                $('.showWidth').css('border-right-width','1px');
                table.column(3).visible(false);
                table.column(4).visible(false);
                table.column(5).visible(false);
                table.column(6).visible(false);
                table.column(7).visible(false);
            }else{
                $('.showTh').removeClass('fa-plus');
                $('.showTh').addClass('fa-minus');
                $('.showWidth').css('border-right-width','0px');
                table.column(3).visible(true);
                table.column(4).visible(true);
                table.column(5).visible(true);
                table.column(6).visible(true);
                table.column(7).visible(true);
            }
        }

        function sortTh(e,index) {
            var sort = 'desc';
            if($(e).hasClass('sorting_desc')) sort= 'esc';
            table.order.fixed( {
                pre: [ index, sort]
            } );
        }
    </script>
</head>
<body id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <jsp:include page="./../side_bar.jsp"/>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <jsp:include page="./../top_bar.jsp"/>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">受発注予実情報一覧</h1>

                <form:form id="searchForm"
                           action="${pageContext.servletContext.contextPath }/budgetActual/budgetActualSearchEntry"
                           modelAttribute="budgetActualSearchForm" method="post">
                    <div class="form-group row ">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>部署選択：</label>
                            <form:select path="bushoCode" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:options items="${bushoCodeList}"/>
                            </form:select>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>顧客会社：</label>
                            <form:select path="kokyakuCode" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:option value="" label=""/>
                                <form:options items="${kokyakuCodeList}"/>
                            </form:select>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>所属会社：</label>
                            <form:select path="shozokuCode" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:option value="" label=""/>
                                <form:options items="${shozokuCodeList}"/>
                            </form:select>
                        </div>
                        <div class="col-sm-3">
                            <label>作業開始月：</label>
                            <form:input path="sagyoStartMonth" type="text" class="form-control datetimepicker-input"  cssErrorClass="form-control is-invalid" />
                            <form:errors path="sagyoStartMonth" cssClass="errMsg"/>
                        </div>
                        <div class="col-sm-3">
                            <label>作業終了月：</label>
                            <form:input path="sagyoEndMonth" type="text" class="form-control datetimepicker-input"  cssErrorClass="form-control is-invalid" />
                            <form:errors path="sagyoEndMonth" cssClass="errMsg"/>
                        </div>
                    </div>
                    <div class="col-sm-4 mb-3 mb-sm-0 offset-sm-4">
                        <a id="serachBtn" href="#" class="btn btn-primary btn-user btn-block">検索</a>
                    </div>
                    <br>
                    <c:if test="${!empty budgetActualSearchForm.errorMsg}">
                        <p class="errMsg">${budgetActualSearchForm.errorMsg}</p>
                    </c:if>
                    <c:if test="${!empty budgetActualSearchForm.infoMsg}">
                        <p class="infoMsg">${budgetActualSearchForm.infoMsg}</p>
                    </c:if>

                    <div class=" row" style="width: 100%;">
                        <div class="col-sm-5">
                        </div>
                        <div class="col-sm-2 mb-2 mb-sm-0">
                            <form:input path="showYm" type="text" class="form-control datetimepicker-input"
                                        style="color: #4e73df;background-color: transparent;border: 0px;box-shadow: 0 0 0 0rem transparent;text-align:center;font-size: 1rem;padding-bottom: 1em;"/>
                        </div>
                        <div class="col-sm-2" >
                            <c:if test="${budgetActualSearchForm.nowFlag != '1'}">
                                <p style="text-align: right;"><a href="#" onclick="changeToNow()">該当月へ</a></p>
                            </c:if>
                            <c:if test="${budgetActualSearchForm.nowFlag == '1'}">
                                <p style="text-align: right;"><a href="#" onclick="changeToThisYm()">該当年の全年度へ</a></p>
                            </c:if>
                        </div>
                    </div>
                    <br>
                    <div>

                        <br>
                        <table id="myTable" class="table table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th rowspan="4" style="min-width: 80px;max-width: 80px;width: 80px;">案件No</th>
                                <th rowspan="4" style="min-width: 20px;max-width: 20px;width: 20px;">枝番</th>
                                <th rowspan="4" style="min-width: 120px;max-width: 120px;width: 120px;">案件名&nbsp&nbsp&nbsp&nbsp&nbsp<a style="color: blue;"onclick="tableShow()" class="showTh fas fa-minus" id="table_show"></a></th>
                                <th rowspan="4" style="min-width: 40px;max-width: 40px;width: 40px;">所属部署</th>
                                <th rowspan="4" style="min-width: 40px;max-width: 40px;width: 40px;">顧客会社</th>
                                <th rowspan="4" style="min-width: 40px;max-width: 40px;width: 40px;">顧客担当名</th>
                                <th rowspan="4" style="min-width: 40px;max-width: 40px;width: 40px;">氏名</th>
                                <th rowspan="4" style="min-width: 20px;max-width: 40px;width: 20px;">給与</th>
                                <th rowspan="4" style="min-width: 40px;max-width: 40px;width: 40px;">所属会社</th>
                                <th colspan="${fn:length(mounthList)*6+6}">${fn:substring(mounthList[0], 0, 4)}年度</th>
                            </tr>
                            <tr>
                                <c:forEach items="${mounthList}" var="mounth">
                                    <th style="border-top:1px solid Transparent!important;" colspan="6">${fn:substring(mounth, 0, 4)}年${fn:substring(mounth, 4, 6)}月</th>
                                </c:forEach>
                                <th style="border-top:1px solid Transparent!important;" colspan="6">表示年月合計</th>
                            </tr>
                            <tr>
                                <c:forEach items="${mounthList}" var="mounth">
                                    <th style="border-top:1px solid Transparent!important;" colspan="3">予算</th>
                                    <th style="border-top:1px solid Transparent!important;" colspan="3">実績</th>
                                </c:forEach>
                                <th style="border-top:1px solid Transparent!important;" colspan="3">予算</th>
                                <th style="border-top:1px solid Transparent!important;" colspan="3">実績</th>
                            </tr>
                            <tr>
                                <c:forEach items="${mounthList}" var="mounth">
                                    <th style="border-top:1px solid Transparent!important;">受注高</th>
                                    <th style="border-top:1px solid Transparent!important;">原価</th>
                                    <th style="border-top:1px solid Transparent!important;">粗利</th>
                                    <th style="border-top:1px solid Transparent!important;">受注高</th>
                                    <th style="border-top:1px solid Transparent!important;">原価</th>
                                    <th style="border-top:1px solid Transparent!important;">粗利</th>
                                </c:forEach>
                                <th style="border-top:1px solid Transparent!important;">受注高</th>
                                <th style="border-top:1px solid Transparent!important;">原価</th>
                                <th style="border-top:1px solid Transparent!important;">粗利</th>
                                <th style="border-top:1px solid Transparent!important;">受注高</th>
                                <th style="border-top:1px solid Transparent!important;">原価</th>
                                <th style="border-top:1px solid Transparent!important;">粗利</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${projectList}" var="projectInfo">
                                <c:if test="${projectInfo.ankenSum!= null && projectInfo.ankenSum != ''}">
                                <tr style="background-color: #f8ccc8">
                                    <td class="ankenNo" style="min-width: 80px;max-width: 80px;width: 80px;">${projectInfo.anken_no}</td>
                                    <td class="edaban" style="text-align: center;min-width: 20px;max-width: 20px;width: 20px;">${projectInfo.edaban}</td>
                                    <td style="min-width: 120px;max-width: 120px;width: 120px;word-break:break-all;">${projectInfo.ankenSum}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.soshiki_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.ryakusho}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.tantosha_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.kyuyo}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.kaisyaryakusho}</td>
                                </c:if>
                                <c:if test="${projectInfo.sum!= null && projectInfo.sum != ''}">
                                <tr style="background-color: #fadf9b">
                                    <td class="ankenNo"  style="text-align: center;min-width: 80px;max-width: 80px;width: 80px;">${projectInfo.sum}</td>
                                    <td class="edaban" style="text-align: center;min-width: 20px;max-width: 20px;width: 20px;"></td>
                                    <td style="min-width: 120px;max-width: 120px;width: 120px;word-break:break-all;">${projectInfo.anken_name}
                                        <span class="koshinKaisu" style="display: none;">${projectInfo.koshin_kaisu}</span>
                                    </td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.soshiki_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.ryakusho}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.tantosha_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.kyuyo}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.kaisyaryakusho}</td>
                                </c:if>
                                <c:if test="${projectInfo.sum== null && projectInfo.ankenSum == null}">
                                <tr>
                                    <td class="ankenNo" style="min-width: 80px;max-width: 80px;width: 80px;">${projectInfo.anken_no}</td>
                                    <td class="edaban" style="text-align: center;min-width: 20px;max-width: 20px;width: 20px;">${projectInfo.edaban}</td>
                                    <td style="min-width: 120px;max-width: 120px;width: 120px;word-break:break-all;">${projectInfo.anken_name}
                                        <span class="koshinKaisu" style="display: none;">${projectInfo.koshin_kaisu}</span>
                                    </td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.soshiki_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.ryakusho}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.tantosha_name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.name}</td>
                                    <td style="min-width: 40px;max-width: 40px;width: 40px;">${projectInfo.kyuyo}</td>
                                    <td>${projectInfo.kaisyaryakusho}</td>
                                </c:if>
                                <c:forEach items="${projectInfo.mounthList}" var="mounthInfo">
                                    <c:if test="${mounthInfo.yosan_juchu_value.indexOf('-') != -1}">
                                        <td style="color: red;text-align: right;">${mounthInfo.yosan_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.yosan_juchu_value.indexOf('-') == -1}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.yosan_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.yosan_hachu_value.indexOf('-') != -1}">
                                        <td style="color: red;text-align: right;">${mounthInfo.yosan_hachu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.yosan_hachu_value.indexOf('-') == -1}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.yosan_hachu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.yosan_cal_value.indexOf('-') != -1}">
                                        <td style="color: red;text-align: right;">${mounthInfo.yosan_cal_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.yosan_cal_value.indexOf('-') == -1}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.yosan_cal_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_juchu_value.indexOf('-') != -1 && projectInfo.cell_flg == '1'}">
                                        <td style="color: red;text-align: right;border-top:1px solid Transparent!important;">${mounthInfo.jiseki_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_juchu_value.indexOf('-') != -1 && projectInfo.cell_flg != '1'}">
                                        <td style="color: red;text-align: right;">${mounthInfo.jiseki_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_juchu_value.indexOf('-') == -1 && projectInfo.cell_flg == '1'}">
                                        <td style="color: blue;text-align: right;border-top:1px solid Transparent!important;">${mounthInfo.jiseki_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_juchu_value.indexOf('-') == -1 && projectInfo.cell_flg != '1'}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.jiseki_juchu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_hachu_value.indexOf('-') != -1 && mounthInfo.jiseki_hachu_flg_value != '1'}">
                                        <td style="color: red;text-align: right;">${mounthInfo.jiseki_hachu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_hachu_value.indexOf('-') == -1 && mounthInfo.jiseki_hachu_flg_value != '1'}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.jiseki_hachu_value}</td>
                                    </c:if>
                                    
                                    <c:if test="${mounthInfo.jiseki_hachu_value.indexOf('-') != -1 && mounthInfo.jiseki_hachu_flg_value == '1'}">
                                        <td class="auto-cal-td" style="color: red;text-align: right;">${mounthInfo.jiseki_hachu_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_hachu_value.indexOf('-') == -1 && mounthInfo.jiseki_hachu_flg_value == '1'}">
                                        <td class="auto-cal-td" style="color: blue;text-align: right;">${mounthInfo.jiseki_hachu_value}</td>
                                    </c:if>

                                    <c:if test="${mounthInfo.jiseki_cal_value.indexOf('-') != -1 && projectInfo.cell_flg == '1'}">
                                        <td style="color: red;text-align: right;border-top:1px solid Transparent!important;">${mounthInfo.jiseki_cal_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_cal_value.indexOf('-') != -1 && projectInfo.cell_flg != '1'}">
                                        <td style="color: red;text-align: right;">${mounthInfo.jiseki_cal_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_cal_value.indexOf('-') == -1 && projectInfo.cell_flg == '1'}">
                                        <td style="color: blue;text-align: right;border-top:1px solid Transparent!important;">${mounthInfo.jiseki_cal_value}</td>
                                    </c:if>
                                    <c:if test="${mounthInfo.jiseki_cal_value.indexOf('-') == -1 && projectInfo.cell_flg != '1'}">
                                        <td style="color: blue;text-align: right;">${mounthInfo.jiseki_cal_value}</td>
                                    </c:if>
                                </c:forEach>
                            </tr>
                            </c:forEach>

                            </tbody>
                        </table>
                        <!-- /.container-fluid -->
                    <!-- confirmPopup -->
                    <jsp:include page="./../modal/confirmPopup.jsp"/>
                    <form:input path="nowPage" type="hidden"/>
                    <form:input path="btnAction" type="hidden"/>
                    <form:input path="ankenNo" type="hidden"/>
                    <form:input path="edaban" type="hidden"/>
                    <form:input path="koshinKaisu" type="hidden"/>
                    <form:input path="nowFlag" type="hidden"/>

                </form:form>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <jsp:include page="./../foot.jsp"/>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
    <style>
        #myTable th {
            white-space: nowrap;     /* 連続する半角スペース・タブ・改行を、1つの半角スペースとして表示 */
            overflow: hidden;        /* はみ出た部分を表示しない */
            text-overflow: ellipsis; /* はみ出た場合の表示方法(ellipsis:省略記号) */
        }
    </style>

</body>
<style>
    .table thead th{
        text-align: center;
    }
    .show-none{
        display: none;
    }
</style>
</html>
