<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="../head.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="ja">
<head>
<script>
	var fileMsg;
	var infoMsg = '${employeeRegisterForm.infoMsg}';
	var genkaShow = '${employeeRegisterForm.genkaShow}';

	$(document).ready(function() {
		$('#oldShozokuSoshikiCode').val('${employeeRegisterForm.shozokuSoshikiCode}');
		$('#entryBtn').click(function() {

			// エラーある場合
			if (fileMsg) {
				return;
			}

			$('#gekkyu').val(delFigure($('#gekkyu').val()));
			$('#mitsumoriGenka').val(delFigure($('#mitsumoriGenka').val()));
			$('#seisanKaitannka').val(delFigure($('#seisanKaitannka').val()));
			$('#seisanJouitannka').val(delFigure($('#seisanJouitannka').val()));

			if ($('.custom-file-label').html() !=='') {
				$('#uploadfileName').val($('.custom-file-label').html());
			}

			$('#registerForm').submit();
		});

		if(infoMsg != null && infoMsg != ''){
			$('#confirmMsg').text(infoMsg);
			$('#confirm-delete').modal("show");
		}

		$('#backBtn').click(function () {
			$('#registerForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeBackEntry');
			$('#registerForm').submit();
		});

		// 重複無視、登録する場合
		$("#deletebtn").click(function() {
			$('#confirmFlag').val('1');
			// エラーある場合
			if (fileMsg) {
				return;
			}

			$('#gekkyu').val(delFigure($('#gekkyu').val()));
			$('#mitsumoriGenka').val(delFigure($('#mitsumoriGenka').val()));
			$('#seisanKaitannka').val(delFigure($('#seisanKaitannka').val()));
			$('#seisanJouitannka').val(delFigure($('#seisanJouitannka').val()));

			if ($('.custom-file-label').html() !=='') {
				$('#uploadfileName').val($('.custom-file-label').html());
			}
			$('#registerForm').submit();
		});

		//職種変更
		$('#shokushuFlag').on('change',function () {
			changeShokushu();
		});

		$('#gekkyu').val(formatNumber($('#gekkyu').val()));
		$('#mitsumoriGenka').val(formatNumber($('#mitsumoriGenka').val()));
		$('#seisanKaitannka').val(formatNumber($('#seisanKaitannka').val()));
		$('#seisanJouitannka').val(formatNumber($('#seisanJouitannka').val()));
		$('#gekkyu').change(function ( ) {
			var genka = formatNumber($('#gekkyu').val());
			$('#gekkyu').val(genka);
			$('#mitsumoriGenka').val(genka);

			// 正社員、契約社員
			if($('#koyokeitai').val() ==='1' || $('#koyokeitai').val() ==='2') {
				//厚生年金有
				if($('#koseinenkinFlag').val() === '1') {
					genka = delFigure($('#gekkyu').val());
					var mitsumoriGenka = Math.round(genka * 1.15);
					$('#mitsumoriGenka').val(formatNumber(mitsumoriGenka));
				}
				// 個人事業主
			} else if ($('#koyokeitai').val() ==='3') {
				// 税込みとなった場合
				if($('#koseinenkinFlag').val() === '1') {
					genka = delFigure($('#gekkyu').val());
					var mitsumoriGenka = Math.round(genka * 0.9);
					$('#mitsumoriGenka').val(formatNumber(mitsumoriGenka));
				}
			}

		});

		$('#mitsumoriGenka').change(function ( ) {
			var mitsumoriGenka = formatNumber($('#mitsumoriGenka').val());
			$('#mitsumoriGenka').val(mitsumoriGenka);
		});


		// 雇用形態
		$('#koyokeitai').change(function ( ) {
			showKoyokeitaiInfo(false);
		});

		// 厚生年金有無
		$('#koseinenkinFlag').change(function ( ) {
			$('#mitsumoriGenka').val($('#gekkyu').val());

			var genka;
			var mitsumoriGenka;
			// 正社員、契約社員
			if($('#koyokeitai').val() ==='1' || $('#koyokeitai').val() ==='2') {
				//厚生年金有
				if($('#koseinenkinFlag').val() === '1' && $('#gekkyu').val() !=='' && $('#gekkyu').val() != null){
					genka = delFigure($('#gekkyu').val());
					mitsumoriGenka = Math.round(genka * 1.15);
					$('#mitsumoriGenka').val(formatNumber(mitsumoriGenka));
				}
				// 個人事業主
			} else if ($('#koyokeitai').val() ==='3') {
				// 税込みとなった場合
				if($('#koseinenkinFlag').val() === '1' && $('#gekkyu').val() !=='' && $('#gekkyu').val() != null){
					genka = delFigure($('#gekkyu').val());
					mitsumoriGenka = Math.round(genka * 0.9);
					$('#mitsumoriGenka').val(formatNumber(mitsumoriGenka));
				}
			}
		});

		//月給
		$('#gekkyu,#seisanjikanStart, #seisanjikanEnd').change(function ( ) {
			if($('input[name="tankaKbn"]:checked').val() === '1') return true;
			if($('#gekkyu').val() == null || $('#gekkyu').val() == '') return true;
			//精算時間STRの値がある場合
			if($('#seisanjikanStart').val() != null && $('#seisanjikanStart').val() != ''){
				//下位単価を設定
				$('#seisanKaitannka').val(formatNumber(Math.floor(delFigure($('#gekkyu').val())/10/$('#seisanjikanStart').val())*10));
			}

			//精算時間ENDの値がある場合
			if($('#seisanjikanEnd').val() != null && $('#seisanjikanEnd').val() != ''){
				//上位単価を設定
				$('#seisanJouitannka').val(formatNumber(Math.floor(delFigure($('#gekkyu').val())/10/$('#seisanjikanEnd').val())*10));
			}
		});

		$("input:radio[name='keiyakuFlag']").click(function() {
			showShugyoInfo();
		});

		// 単価区分イベント
		$("input:radio[name='tankaKbn']").change(function() {
			showTankaInfo();
		});

		$('#nyushaDay').on('dp.change', function(e){
			if ($('#mode').val() === '1') {
				$('#tekiyoStartDay').val($('#nyushaDay').val());
			}
		});

		$('#tekiyoStartDay').on('dp.change', function(e) {
			if ($('#mode').val() === '2') {
				var strdate = dateAdd($('#tekiyoStartDay').val(), -1);
				$('#beforeTekiyoEndDay').val(strdate);
			}
		});

		// ファイルアップロード
		$('.custom-file-input').on('change', handleFileSelect);
		function handleFileSelect(evt) {
			var files = evt.target.files;

			var output = [];
			var length = files.length;
			for (var i = 0; i < length; i++) {
				if (i === (length - 1)) {
					output.push(files[i].name);
				} else {
					output.push(files[i].name, ', ');
				}
			}
			$(this).next('.custom-file-label').html(output.join(''));

			checkUploadFileSize(files);
		}

		//ファイルの取消
		$('.reset').click(function(){
			$(this).parent().prev().children('.custom-file-label').html('ファイル選択...');
			$('.custom-file-input').val('');
			initErrorDisplay();
		});

		// 社員氏名漢字
		$('#seiKanji, #meiKanji').change(function () {
			// 全部全角半角スペース置換関数
			removeInputTextAllSpace(this);
		});

		// 就業関連処理
		showShugyoInfo();
		changeShokushu();

		// 権限により給与表示設定
		if(genkaShow !== '1') {
			$('.genkaShow').css('display','none');
		} else {
			$('.genkaShow').css('display','');

			// 雇用形態関連処理
			showKoyokeitaiInfo(true);
			// 社員単価情報切替表示関連処理
			showTankaInfo();
		}

		// 退職初期表示処理
		//setTaishokuInitDisplay();
	});
	
	$(function() {
		$('#nyushaDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});
		$('#taishokuDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});
		$('#seinengappi').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});

		$('#beforeTekiyoStartDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});
		$('#beforeTekiyoEndDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});
		$('#tekiyoStartDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});
		$('#tekiyoEndDay').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYY/MM/DD',
			locale : 'ja'
		});

		$('#sotsugyoYm').datetimepicker({
			dayViewHeaderFormat : 'YYYY年 MMMM',
			format : 'YYYYMM',
			locale : 'ja'
		});
	});

	/**
	 * 社員単価情報切替表示関連処理
	 */
	function showTankaInfo() {

		// 時間単価の場合
		if ($('input[name="tankaKbn"]:checked').val() === '1') {
			$('#lblKyuyo').text("時給：");
			$('.getuTankaInfo').hide();
			$('.jikanTankaInfo').show();

			// 個人事業主
			if($('#koyokeitai').val() ==='3') {
				$('.kojinKyuyoInfo').show();
			}
		} else {
			$('#lblKyuyo').text("月給：");
			$('.jikanTankaInfo').hide();
			$('.getuTankaInfo').show();
		}

		if($('input[name="tankaKbn"]:checked').val() === '2'
				&& $('input[name="seisanFlag"]:checked').val() === '1') {
			$('#seisanShow').show();
		}else{
			$('#seisanShow').hide();
		}
	}

	/**
	 * 精算有無変更
	 */
	function changeSeisanFlag(e) {
		//　精算有の場合
		if($(e).val() === '1' && $('input[name="tankaKbn"]:checked').val() === '2'){
			$('#seisanShow').css('display','');
		}else{
			$('#seisanShow').css('display','none');
		}
	}

	/**
	 * 職種変更時処理
	 */
	function changeShokushu(){
		//技術の場合、所属部署を非表示
		if($('#shokushuFlag').val() =='3'){
			$('#shozokuSoshikiCode').attr('disabled','disabled');
			$('#shozokuSoshikiCode').val($('#oldShozokuSoshikiCode').val());
		}else{
			$('#shozokuSoshikiCode').removeAttr('disabled');
		}
	}

	/**
	 * 就業関連処理
	 */
	function showShugyoInfo() {
		// 退職申し込みの場合
		if($('#shugyoJokyoFlag').val() === '2'
				|| ($('#shugyoJokyoHenkouFlag').val() === undefined && $('#taishokuDay').val() !== '')
				|| $('#shugyoJokyoHenkouFlag').val() === '2') {

			$('.changeInfo').each(function () {
				$(this).hide();
			});

			// 契約内容更新の場合
			if ($('input[name="keiyakuFlag"]:checked').val() === '2') {
				$('.shugyoInfo').hide();
				$('.changeTaishokuInfo').hide();

				$('#keiyakuInfo').collapse('show');
			} else if ($('input[name="keiyakuFlag"]:checked').val() === '1') {
				$('.shugyoInfo').show();
				$('.changeTaishokuInfo').show();

				$('#keiyakuInfo').collapse('show');
			} else {
				$('#keiyakuInfo').collapse('hide');
			}
		} else {

			$('.changeTaishokuInfo').hide();

			if ($('#mode').val() === '2') {
				// 契約内容更新の場合
				if ($('input[name="keiyakuFlag"]:checked').val() === '2') {
					$('.shugyoInfo').hide();
					$('.changeInfo').each(function () {
						$(this).hide();
					});

					$('#keiyakuInfo').collapse('show');

				} else if ($('input[name="keiyakuFlag"]:checked').val() === '1') {
					$('.shugyoInfo').show();
					$('.changeInfo').each(function () {
						$(this).show();
					});

					$('#keiyakuInfo').collapse('show');
				} else {
					$('#keiyakuInfo').collapse('hide');
				}
			}
		}
	}

	/**
	 * 雇用形態関連処理
	 */
	function showKoyokeitaiInfo(isInitFlg) {
		var selectValue;
		// 個人事業主
		if($('#koyokeitai').val() ==='3') {

			selectValue = $('#koseinenkinFlag').val();
			$('#lblKoyokeitai').text("税金計算：");

			$('#koseinenkinFlag').empty();
			$('#koseinenkinFlag').append("<option value=''></option>");
			$('#koseinenkinFlag').append("<option value='1'>税込み</option>");
			$('#koseinenkinFlag').append("<option value='2'>税抜き</option>");

			// 初期化のみ
			if (isInitFlg) {
				$('#koseinenkinFlag').find("option[value='"+ selectValue +"']").attr("selected", true);
			}

			// 選択を活性になる
			$("#koseinenkinFlag").removeAttr("disabled");

			$('.koyokeitaiInfo').show();

			// 初期以外かつ時間単価の場合
			if (!isInitFlg && $('input[name="tankaKbn"]:checked').val() === '1') {
				$('#lblKyuyo').text("時給：");
				$('.kojinKyuyoInfo').show();
			}

			// 正社員、契約社員
		} else if($('#koyokeitai').val() ==='1' || $('#koyokeitai').val() ==='2') {

			selectValue = $('#koseinenkinFlag').val();
			$('#lblKoyokeitai').text("厚生年金有無：");

			$('#koseinenkinFlag').empty();
			$('#koseinenkinFlag').append("<option value=''></option>");
			$('#koseinenkinFlag').append("<option value='1' selected>有</option>");
			$('#koseinenkinFlag').append("<option value='2'>無</option>");

			// 初期化のみ
			$("#koseinenkinFlag").attr("disabled", "disabled");

			$('.koyokeitaiInfo').show();

			// 初期以外かつ時間単価の場合
			if (!isInitFlg && $('input[name="tankaKbn"]:checked').val() === '1') {
				$('#lblKyuyo').text("時給：");
				$('.kojinKyuyoInfo').hide();
			}

		} else {
			$('.koyokeitaiInfo').hide();

		}
	}

	/**
	 * 退職初期表示処理
	 */
	function setTaishokuInitDisplay() {
		// 退職の場合
		if($('#shugyoJokyoFlag').val() === '2') {
			$("#registerForm input").prop("disabled", true);
			$("#registerForm select").attr("disabled", true);
			$("#registerForm textarea").attr("disabled", true);
			$('#registerForm .btn').addClass('disabled');
			$('#registerForm #backBtn').removeClass('disabled');
		}
	}

	function checkUploadFileSize(files) {
		fileMsg = checkFileSize(files);
		if (fileMsg) {
			setErrorDisplay(fileMsg);
		} else {
			initErrorDisplay();
		}
	}

	function checkFileSize(files) {
		// 50M
		var maxsize = 50 * 1024 * 1024;

		var filesize = 0;
		var length = files.length;
		for (var i = 0; i < length; i++) {
			filesize += files[i].size;
		}

		if (filesize === -1) {
			return '選択したファイルのアップロードが失敗しました';
		} else if (filesize > maxsize) {
			return '選択したファイルのサイズが最大サイズ（50M）を超えるため、アップロードできません';
		} else {
			return null;
		}
	}

	function initErrorDisplay() {
		fileMsg = null;
		$('#fileErr').text('');
		$('#fileErr').attr('display','none');
	}

	function setErrorDisplay(errmsg) {
		$('#fileErr').text(errmsg);
		$('#fileErr').removeAttr('display');
	}

</script>
</head>
<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<jsp:include page="./../side_bar.jsp"/>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<jsp:include page="./../top_bar.jsp"/>
				<!-- End of Topbar -->
				<!-- Begin Page Content -->
				<div class="container">
					<!-- Page Heading -->

					<c:if test="${!(employeeRegisterForm.mode == '1')}">
						<h1 class="h3 mb-2 text-gray-800 ">社員基本情報変更</h1>
					</c:if>
					<c:if test="${employeeRegisterForm.mode == '1'}">
						<h1 class="h3 mb-2 text-gray-800">社員基本情報登録</h1>
					</c:if>

					<form:form id="registerForm" action="${pageContext.servletContext.contextPath }/employee/employeeRegisterEntry" modelAttribute="employeeRegisterForm" method="post" enctype="multipart/form-data">
						<h5 class="mb-3 text-gray-900">社員基本情報</h5>

						<div class="card">
							<div class="card-header" role="tab" id="headingBase" style="display: none">
								<a class="text-body d-block p-3 m-n3" data-toggle="collapse" href="#baseEmpInfo" role="button" aria-expanded="true" aria-controls="baseEmpInfo">
								</a>
							</div><!-- /.card-header -->
							<div id="baseEmpInfo" class="collapse show" role="tabpanel" aria-labelledby="headingBase" data-parent="#accordion">
								<div class="card-body">
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label  class="required">入社日：</label>
											<form:input path="nyushaDay" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
											<form:errors path="nyushaDay" cssClass="errMsg" />
										</div>

										<div class="col-sm-6">
											<label >所属部署：</label>
											<form:select path="shozokuSoshikiCode" class="custom-select" cssErrorClass="form-control is-invalid">
												<form:option value="" label=""/>
												<form:options items="${bushoCodeList}" />
											</form:select>
											<form:errors path="shozokuSoshikiCode" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row ">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label  class="required">社員姓：</label>
											<form:input path="seiKanji" type="text" class="form-control " placeholder="社員姓" maxlength="32" cssErrorClass="form-control is-invalid" />
											<form:errors path="seiKanji" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label  class="required">社員名：</label>
											<form:input path="meiKanji" type="text" class=" form-control" placeholder="社員名" maxlength="64" cssErrorClass="form-control is-invalid" />
											<form:errors path="meiKanji" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label>ひらがな（姓）：</label>
											<form:input path="seiKana" type="text" class="form-control" maxlength="64" cssErrorClass="form-control is-invalid" />
											<form:errors path="seiKana" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label>ひらがな（名）：</label>
											<form:input path="meiKana" type="text" class=" form-control" maxlength="128" cssErrorClass="form-control is-invalid" />
											<form:errors path="meiKana" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label>英文字（姓）：</label>
											<form:input path="seiEimoji" type="text" class="form-control " maxlength="64" cssErrorClass="form-control is-invalid" />
											<form:errors path="seiEimoji" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label >英文字（名）：</label>
											<form:input path="meiEimoji" type="text" class=" form-control" maxlength="128" cssErrorClass="form-control is-invalid" />
											<form:errors path="meiEimoji" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label class="required">性別：</label>
											<form:select path="sex" class="custom-select" cssErrorClass="form-control is-invalid">
												<form:option value="" label=""/>
												<form:options items="${sexList}" />
											</form:select>
											<form:errors path="sex" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label class="required">国籍：</label>
											<form:select path="kokusekiFlag" class="custom-select" cssErrorClass="form-control is-invalid">
												<form:option value="" label=""/>
												<form:options items="${kokusekiList}" />
											</form:select>
											<form:errors path="kokusekiFlag" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label for="sex">婚姻状況：</label>
											<form:select path="koninJokyoFlag" name="sex" id="sex" class="custom-select" cssErrorClass="form-control is-invalid">
												<form:option value="" label=""/>
												<form:options items="${koninJokyoList }" />
											</form:select>
											<form:errors path="koninJokyoFlag" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label >生年月日：</label>
											<form:input path="seinengappi" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
											<form:errors path="seinengappi" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label >住所：</label>
											<form:input path="address" type="text" maxlength="512"
														class="form-control " cssErrorClass="form-control is-invalid" />
											<form:errors path="address" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label >連絡先：</label>
											<form:input path="renrakusaki" type="text" maxlength="13" class="form-control " cssErrorClass="form-control is-invalid" placeholder="xx-xxxx-xxxx又はxxx-xxxx-xxxx"/>
											<form:errors path="renrakusaki" cssClass="errMsg" />
										</div>

										<div class="col-sm-6">
											<label >日本語力：</label>
											<form:select path="nihongoLevel" class="custom-select " cssErrorClass="form-control is-invalid">
												<form:options items="${nihongoLevelList}" />
											</form:select>
											<form:errors path="nihongoLevel" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-sm-12 mb-3 mb-sm-0">
											<label>開発言語:</label>
											<form:checkboxes items="${gijutsuryokuList}" path="gijutsuryoku"  cssErrorClass="form-control is-invalid"/>
											<form:errors path="gijutsuryoku" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label for="sex">学歴：</label>
											<form:select path="gakureki" class="custom-select" cssErrorClass="form-control is-invalid">
												<form:option value="" label=""/>
												<form:options items="${gakurekiList }" />
											</form:select>
											<form:errors path="gakureki" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label >専攻：</label>
											<form:input path="senko" type="text" class="form-control " maxlength="128" cssErrorClass="form-control is-invalid" />
											<form:errors path="senko" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label for="sex">最終卒業校：</label>
											<form:input path="saishuSotsugyoko" class="form-control" maxlength="128" cssErrorClass="form-control is-invalid" />
											<form:errors path="saishuSotsugyoko" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label >卒業年月：</label>
											<form:input path="sotsugyoYm" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
											<form:errors path="sotsugyoYm" cssClass="errMsg" />
										</div>
									</div>
									<div class=" form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label >在留カードNo：</label>
											<form:input path="zairyuCardNo" type="text" class="form-control " maxlength="12" cssErrorClass="form-control is-invalid" />
											<form:errors path="zairyuCardNo" cssClass="errMsg" />
										</div>
										<div class="col-sm-6">
											<label >在留期限：</label>
											<form:input path="zairyuKigen" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
											<form:errors path="zairyuKigen" cssClass="errMsg" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label >扶養家族人数：</label>
											<form:input path="fuyokazokuNinzu" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
											<form:errors path="fuyokazokuNinzu" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row">
										<div class="col-sm-6 mb-3 mb-sm-0">
											<label >メールアドレス：</label>
											<form:input path="mailAddress" type="text" class="form-control" maxlength="128" cssErrorClass="form-control is-invalid" />
											<form:errors path="mailAddress" cssClass="errMsg" />
										</div>
									</div>

									<div class="form-group row ml-0">
										<label for="file">ファイル（複数選択可）</label>
										<div id="file" class="input-group">
											<div class="custom-file">
												<input type="file" id="uploadfile" class="custom-file-input" name="uploadfile" multiple />
												<label class="custom-file-label" for="uploadfile" data-browse="参照">ファイル選択...</label>
											</div>
											<div class="input-group-append">
												<button type="button" class="btn btn-outline-secondary reset">取消</button>
											</div>
										</div>
										<p id="fileErr" class="errMsg" display="none"></p>
									</div>
								</div><!-- /.card-body -->
							</div><!-- /.collapse -->
						</div><!-- /.card -->

						<h5 class="mt-3 mb-3 text-gray-900">社員雇用契約情報</h5>

						<c:if test="${shainKoyokeiyakuJohoList!= null && fn:length(shainKoyokeiyakuJohoList) != 0}">
							<table id="empTable" class="table table-bordered" style="width:100%">
								<thead class="thead-light">
								<tr>
									<th>枝番</th>
									<th>適用開始年月日</th>
									<th>適用終了年月日</th>
									<th>雇用形態</th>
									<th>役職</th>
									<th>職種</th>
									<th>就業状況</th>
									<th>単価区分</th>
									<th>月給</th>
									<th>計算原価</th>
									<th>厚生年金有無</th>
									<th>税金計算</th>
									<th>精算有無</th>
									<th>精算時間</th>
									<th>控除単価</th>
									<th>超過単価</th>
									<th>本人要望</th>
								</tr>
								</thead>
								<tbody>
								<c:forEach items="${shainKoyokeiyakuJohoList}" var="shainKoyokeiyakuJoho">
									<tr>
										<td>${shainKoyokeiyakuJoho.edaban}</td>
										<td>${shainKoyokeiyakuJoho.tekiyo_start_day}</td>
										<td>${shainKoyokeiyakuJoho.tekiyo_end_day}</td>
										<td>${shainKoyokeiyakuJoho.koyokeitai_name}</td>
										<td>${shainKoyokeiyakuJoho.yakushokumei}</td>
										<td>${shainKoyokeiyakuJoho.shokushuname}</td>
										<td>${shainKoyokeiyakuJoho.shugyoname}</td>
										<td>${shainKoyokeiyakuJoho.tanka_kbn_name}</td>
										<td>${shainKoyokeiyakuJoho.gekkyu}</td>
										<td>${shainKoyokeiyakuJoho.mitsumori_genka}</td>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai == '3' && shainKoyokeiyakuJoho.koseinenkin_flag == '1'}">
										<td>-</td>
										<td>税込み</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai == '3' && shainKoyokeiyakuJoho.koseinenkin_flag != '1'}">
										<td>-</td>
										<td>税抜き</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai != '3' && shainKoyokeiyakuJoho.koseinenkin_flag == '1'}">
										<td>有</td>
										<td>-</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai != '3' && shainKoyokeiyakuJoho.koseinenkin_flag != '1'}">
										<td>無</td>
										<td>-</td>
										</c:if>
										<td>${shainKoyokeiyakuJoho.seisan_name}</td>
										<td>${shainKoyokeiyakuJoho.seisanjikan}</td>
										<td>${shainKoyokeiyakuJoho.seisan_kaitannka}</td>
										<td>${shainKoyokeiyakuJoho.seisan_jouitannka}</td>
										<td>${shainKoyokeiyakuJoho.yobo}</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
						</c:if>

						<c:if test="${employeeRegisterForm.mode == '2'}">
							<div class="bs-example">
								<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-1" value="0" />
								<label class="radio-inline_label btn-outline-info" for="item-1">
									変更無し
								</label>
								<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-2" value="2" />
								<label class="radio-inline_label btn-outline-info" for="item-2">
									登録ミスによる訂正
								</label>
								<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-3" value="1" />
								<label class="radio-inline_label btn-outline-info" for="item-3">
									契約内容変更による期間再設定
								</label>
							</div>
						</c:if>

						<div class="accordion mb-3" id="accordion" role="tablist" aria-multiselectable="true">
							<div class="card">
								<div class="card-header" role="tab" id="headingOne" style="display: none">
									<a class="text-body d-block p-3 m-n3" data-toggle="collapse" href="#keiyakuInfo" role="button" aria-expanded="true" aria-controls="keiyakuInfo">
									</a>
								</div><!-- /.card-header -->
								<div id="keiyakuInfo" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<c:if test="${employeeRegisterForm.mode == '2'}">
											<div class="form-group row changeInfo">
												<div class="col-sm-2 mb-3 mb-sm-0 ">
													<label>変更前</label>
												</div>
											</div>
											<div class="form-group row changeInfo">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label>適用開始日：</label>
													<form:input path="beforeTekiyoStartDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true" />
													<form:errors path="beforeTekiyoStartDay" cssClass="errMsg" />
												</div>
												<div class="col-sm-6">
													<label >適用終了日：</label>
													<form:input path="beforeTekiyoEndDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true" />
													<form:errors path="beforeTekiyoEndDay" cssClass="errMsg" />
												</div>
											</div>

											<div class="form-group row changeInfo">
												<div class="col-sm-2 mb-3 mb-sm-0">
													<label>変更後</label>
												</div>
											</div>
										</c:if>

										<div class="form-group row changeInfo">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label class="required">適用開始日：</label>
												<form:input path="tekiyoStartDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
												<form:errors path="tekiyoStartDay" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label>適用終了日：</label>
												<form:input path="tekiyoEndDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="tekiyoEndDay" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row changeTaishokuInfo">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label class="required">退職日：</label>
												<form:input path="taishokuDay" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
												<form:errors path="taishokuDay" cssClass="errMsg" />
											</div>
										</div>

										<c:if test="${employeeRegisterForm.mode == '2'}">
											<div class="form-group row shugyoInfo">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label >現在就業状況：</label>
													<form:select path="shugyoJokyoFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
														<form:option value="" label=""/>
														<form:options items="${shugyoJokyoList}" />
													</form:select>
													<form:errors path="shugyoJokyoFlag" cssClass="errMsg" />
												</div>
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label >就業状況変更：</label>
													<form:select path="shugyoJokyoHenkouFlag" class="custom-select" cssErrorClass="form-control is-invalid" onchange="showShugyoInfo();">
														<form:option value="" label=""/>
														<form:options items="${shugyoJokyoHenkouList}" />
													</form:select>
													<form:errors path="shugyoJokyoHenkouFlag" cssClass="errMsg" />
												</div>
											</div>
										</c:if>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label class="required">職種：</label>
												<form:select path="shokushuFlag" class="custom-select" cssErrorClass="form-control is-invalid">
													<form:option value="" label=""/>
													<form:options items="${shokushuList}" />
												</form:select>
												<form:errors path="shokushuFlag" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >役職：</label>
												<form:select path="yakushokuFlag" class="custom-select" cssErrorClass="form-control is-invalid">
													<form:option value="" label=""/>
													<form:options items="${yakushokuList}" />
												</form:select>
												<form:errors path="yakushokuFlag" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label class="required">雇用形態：</label>
												<form:select path="koyokeitai" class="custom-select" cssErrorClass="form-control is-invalid">
													<form:option value="" label=""/>
													<form:options items="${koyokeitaiList}" />
												</form:select>
												<form:errors path="koyokeitai" cssClass="errMsg" />
											</div>
											<div class="col-sm-6 koyokeitaiInfo genkaShow">
												<label id="lblKoyokeitai" class="required">厚生年金有無：</label>
												<form:select path="koseinenkinFlag" class="custom-select" cssErrorClass="form-control is-invalid">
													<form:option value="" label=""/>
													<form:option value="1" label="有" />
													<form:option value="2" label="無" />
												</form:select>
												<form:errors path="koseinenkinFlag" cssClass="errMsg" />
											</div>
										</div>

										<div class="genkaShow">
											<div class="form-group row">
												<div class="col-sm-2 mb-3 mb-sm-0">
													<label>単価：</label>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<form:radiobutton path="tankaKbn"  value="1"  label="時間単価"/>
													<form:radiobutton path="tankaKbn"  value="2"  label="月単価"/>
												</div>
											</div>

											<div class="form-group row getuTankaInfo kojinKyuyoInfo">
												<div class="col-sm-2">
													<label id="lblKyuyo">月給：</label>
												</div>
												<div class="col-sm-5 mb-3 mb-sm-0">
													<form:input path="gekkyu" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
													<form:errors path="gekkyu" cssClass="errMsg" />
												</div>
											</div>

											<div class="form-group row">
												<div class="col-sm-2">
													<label >計算原価：</label>
												</div>
												<div class="col-sm-5 mb-3 mb-sm-0">
													<form:input path="mitsumoriGenka" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
													<form:errors path="mitsumoriGenka" cssClass="errMsg" />
												</div>
												<div class="col-sm-1 getuTankaInfo">
													<span class="input-group-append">（円） 月</span>
												</div>
												<div class="col-sm-1 jikanTankaInfo">
													<span class="input-group-append">（円） 時間</span>
												</div>
											</div>

											<div class="form-group row">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<laber>精算有無：
													</laber>
													<form:radiobutton path="seisanFlag"  value="1"  label="有" onclick="changeSeisanFlag(this)" checked="checked"/>
													&nbsp;&nbsp;&nbsp;
													<form:radiobutton path="seisanFlag"  value="2"  label="無" onclick="changeSeisanFlag(this)" />
												</div>
											</div>
											<div id="seisanShow">
												<div class="form-group row">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<label >精算時間STR：</label>
														<form:input path="seisanjikanStart" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
														<form:errors path="seisanjikanStart" cssClass="errMsg" />
													</div>
													<div class="col-sm-6">
														<label >精算時間END：</label>
														<form:input path="seisanjikanEnd" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
														<form:errors path="seisanjikanEnd" cssClass="errMsg" />
													</div>
												</div>
												<div class="form-group row">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<label >控除単価：</label>
														<form:input path="seisanKaitannka" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
														<form:errors path="seisanKaitannka" cssClass="errMsg" />
													</div>
													<div class="col-sm-6">
														<label >超過単価：</label>
														<form:input path="seisanJouitannka" type="text" class="form-control " cssErrorClass="form-control is-invalid" />
														<form:errors path="seisanJouitannka" cssClass="errMsg" />
													</div>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-12 mb-3 mb-sm-0">
												<label>要望：</label>
												<form:textarea path="yobo" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" />
												<form:errors path="yobo" cssClass="errMsg" />
											</div>
										</div>
									</div><!-- /.card-body -->
								</div><!-- /.collapse -->
							</div><!-- /.card -->
						</div><!-- /#accordion -->

						<a id="backBtn" href="#" class="btn btn-warning col-sm-4 offset-sm-1">戻る</a>
						<c:if test="${employeeRegisterForm.mode == '2'}">
							<a id="entryBtn" href="#" class="btn btn-success col-sm-4 offset-sm-1" style="width: 30%">変更</a>
						</c:if>
						<c:if test="${employeeRegisterForm.mode != '2'}">
							<a id="entryBtn" href="#" class="btn btn-primary col-sm-4 offset-sm-1" style="width: 30%">登録</a>
						</c:if>

						<jsp:include page="./../modal/confirmPopup.jsp"/>
						<form:input path="koshinKaisu" type="hidden"/>
						<form:input path="koyouKoshinKaisu" type="hidden"/>
						<form:input path="shainNo" type="hidden"/>
						<form:input path="edaban" type="hidden"/>
						<form:input path="mode" type="hidden"/>
						<form:input path="uploadfileName" type="hidden"/>

						<form:input path="beforeShokushuFlag" type="hidden"/>
						<form:input path="beforeYakushokuFlag" type="hidden"/>
						<form:input path="beforeKoyokeitai" type="hidden"/>
						<form:input path="beforeKoseinenkinFlag" type="hidden"/>
						<form:input path="beforeTankaKbn" type="hidden"/>
						<form:input path="beforeGekkyu" type="hidden"/>
						<form:input path="beforeMitsumoriGenka" type="hidden"/>
						<form:input path="beforeSeisanjikanStart" type="hidden"/>
						<form:input path="beforeSeisanjikanEnd" type="hidden"/>
						<form:input path="beforeSeisanKaitannka" type="hidden"/>
						<form:input path="beforeSeisanJouitannka" type="hidden"/>
						<form:input path="beforeYobo" type="hidden"/>
						<form:input path="confirmFlag" type="hidden"/>
						<form:input path="genkaShow" type="hidden"/>

						<input id="oldShozokuSoshikiCode" type="hidden"/>
					</form:form>
				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footbar -->
			<jsp:include page="./../foot.jsp"/>
			<!-- End of Footbar -->


		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->


</body>


</html>