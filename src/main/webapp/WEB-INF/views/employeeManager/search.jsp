<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../head.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="ja">
<head>
    <script>
        $(document).ready(function () {
            $('#serachBtn').click(function () {
                $('#searchForm').submit();
            });

            var table = $('#myTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Japanese.json"
                },
                "data": ${resultBeanList},
                lengthMenu: [10, 20, 30, 50, 100],
                scrollX: true,
                scrollY: 500,
                displayLength: 100,
                scrollCollapse: true,
                //自動列幅計算
                bAutoWidth: true,
                "columns": [
                    {"data": "gijutsuShaMei", render: $.fn.dataTable.render.text()},
                    {"data": "koyoKeitai", render: $.fn.dataTable.render.text()},
                    {"data": "sohikiName", render: $.fn.dataTable.render.text()},
                    {"data": "kinmuKeitai", render: $.fn.dataTable.render.text()},
                    {"data": "nihongoLevel", render: $.fn.dataTable.render.text()},
                    {"data": "gijutsuryoku", render: $.fn.dataTable.render.text()},
                    {"data": "uploadFlg", render: $.fn.dataTable.render.text()},
                    {"data": "yakushokuMei", render: $.fn.dataTable.render.text()},
                    {"data": "shokushuName", render: $.fn.dataTable.render.text()},
                    {"data": "genka", render: $.fn.dataTable.render.text()},
                    {"data": "kyuyo", render: $.fn.dataTable.render.text()},
                    {"data": "taishoku_day", render: $.fn.dataTable.render.text()},
                    {"data": "shainNo", visible: false},
                    {"data": "koyokeitai_cd", visible: false}
                ]
            });

            //行選択
            $('#myTable tbody').on('click', 'tr', function () {
                console.log(table.row(this).data());
                var uploadFlg = '';
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                    $('#shain_no').val('');
                    $('#shainName').val('');
                    $('#shainKbn').val('');
                } else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    var shainNo = table.row(this).data().shainNo;
                    uploadFlg = table.row(this).data().uploadFlg;
                    $('#shain_no').val(shainNo);
                    $('#shainName').val(table.row(this).data().gijutsuShaMei);
                    $('#shainKbn').val(table.row(this).data().koyokeitai_cd);
                }
                if (table.rows('.selected').data().length !== 0) {
                    var show_kbn = table.row(this).data().show_kbn;
                    $('.buttontran').removeClass('disabled');
                    if(show_kbn == '1'){
                        $('.shainshow').removeClass('disabled');
                    }else{
                        $('.shainshow').addClass('disabled');
                    }
                    if (uploadFlg === '有') {
                        $('.buttonrireki').removeClass('disabled');
                    } else {
                        $('.buttonrireki').addClass('disabled');
                    }
                } else {
                    $('.shainshow').addClass('disabled');
                    $('.buttontran').addClass('disabled');
                    $('.buttonrireki').addClass('disabled');
                }

            });

            // 技術者名
            $('#gijutsuShaMei').change(function () {
                // 全部全角半角スペース置換関数
                removeInputTextAllSpace(this);
            });
        });
        //案件情報新規登録
        function goRegister() {
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeGoRegisterEntry');
            $('#searchForm').submit();
        }

        //案件情報更新
        function goChange() {
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeGoChangeEntry');
            $('#searchForm').submit();
        }

        //案件情報表示
        function goDisplay() {
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeGoDisplayEntry');
            $('#searchForm').submit();
        }
        //
        function openFreePersonnel() {
            $('#freePersonnelPopup').modal("show");
        }
        function freePersonneRegist() {
            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeAddFreeEntry');
            $('#searchForm').submit();
        }

        // ダウンロードを開く
        function openFileOperation() {
            $('#popupShainName').val($('#shainName').val());
            $('#popupShainNo').val($('#shain_no').val());
            $('#popupShainKbn').val($('#shainKbn').val());
            $('#fileOperationPopup').modal("show");
        }

        // 案件責任者追加
        function addSekininsha() {

            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/addSekininshaEntry');
            $('#searchForm').submit();

        }
        
        // 給与詳細表示
        function addSekininsha() {

            $('#searchForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/addKyoyoEntry');
            $('#searchForm').submit();

        }
    </script>
</head>
<body id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <jsp:include page="./../side_bar.jsp"/>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <jsp:include page="./../top_bar.jsp"/>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container">

                <!-- Page Heading -->
                <h1 class="h3 mb-4 text-gray-800">社員一覧</h1>

                <form:form id="searchForm"
                           action="${pageContext.servletContext.contextPath }/employee/employeeSearch"
                           modelAttribute="employeeSearchForm" method="post">
                    <div class="form-group row ">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>技術者名：</label>
                            <form:input path="gijutsuShaMei" type="text" class="form-control "
                                        cssErrorClass="form-control is-invalid" placeholder="技術者名（漢字）"/>
                            <form:errors path="gijutsuShaMei" cssClass="errMsg"/>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>勤務状態：</label>
                            <form:select path="kinmuJyotai" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:option value="">全件</form:option>
                            </form:select>
                            <form:errors path="kinmuJyotai" cssClass="errMsg"/>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>部署選択：</label>
                            <form:select path="bushoCode" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:option value="">全部署</form:option>
                                <form:options items="${bushoCodeList}"/>
                            </form:select>
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>在職状態：</label>
                            <form:select path="zaishokuFlag" class="form-control "
                                         cssErrorClass="form-control is-invalid">
                                <form:option value="1">在職者のみ</form:option>
                                <form:option value="2">退職者を含む</form:option>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-sm-4 mb-3 mb-sm-0 offset-sm-4">
                        <a id="serachBtn" href="#" class="btn btn-primary btn-user btn-block">検索</a>
                    </div>
                    <c:if test="${!empty employeeSearchForm.errorMsg}">
                        <p class="errMsg">${employeeSearchForm.errorMsg}</p>
                    </c:if>
                    <sec:authorize access="hasAnyAuthority('k_shain2')">
                        <br>
                    <!-- /.container-fluid -->
                    <div>
                        <c:if test="${empty employeeSearchForm.addType}">
                            <a href="#"
                               class=" btn btn-primary btn-icon-split" onclick="goRegister()">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                <span class="text">追加登録へ</span>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-split disabled shainshow" onclick="goChange()">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                    </span>
                                <span class="text">基本情報更新</span>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-split disabled">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                    </span>
                                <span class="text">受注情報更新へ</span>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-split disabled">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                    </span>
                                <span class="text">受注情報表示</span>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-split disabled shainshow" onclick="goDisplay()">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                    </span>
                                <span class="text">社員基本情報表示</span>
                            </a>
                            <a href="#" class="btn btn-success btn-icon-split disabled buttonrireki" onclick="openFileOperation();">
                                    <span class="icon text-white-50">
                                    <i class="fas fa-arrow-right"></i>
                                    </span>
                                <span class="text">経歴書ダウンロード</span>
                            </a>
                        </c:if>
                        <c:if test="${empty employeeSearchForm.addType || employeeSearchForm.addType == '2'}">
                        <a href="#"
                           class=" btn btn-primary btn-icon-split disabled buttontran" onclick="openFreePersonnel()">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                            <span class="text">空要員追加登録</span>
                        </a>
                        </c:if>
                        <c:if test="${employeeSearchForm.addType == '1'}">
                            <a href="#"
                               class=" btn btn-primary btn-icon-split disabled buttontran" onclick="addSekininsha()">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">案件責任者追加</span>
                            </a>
                        </c:if>
                    </div>
                </sec:authorize>
                    <br>
                    <table id="myTable" class="table table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>技術者名</th>
                            <th>雇用形態</th>
                            <th>現所属</th>
                            <th>勤務形態</th>
                            <th>日本語能力</th>
                            <th>技術力</th>
                            <th>経歴書有無</th>
                            <th>役職</th>
                            <th>職種</th>
                            <th>原価</th>
                            <th>給与</th>
                            <th>退職日</th>
                        </tr>
                        </thead>
                    </table>
                    <form:input path="shain_no" type="hidden"/>
                    <form:input path="freeYM" type="hidden"/>
                    <form:input path="shainKbn" type="hidden"/>
                    <form:input path="ankenNo" type="hidden"/>
                    <form:input path="edaban" type="hidden"/>
                    <form:input path="addType" type="hidden"/>
                    <form:input path="memberRegisterSearch_showYm" type="hidden"/>
                    <form:input path="memberRegisterSearch_nowFlag" type="hidden"/>

                    <jsp:include page="./../modal/freePersonnelPopup.jsp"/>
                    <jsp:include page="./../modal/fileOperationPopup.jsp"/>
                </form:form>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <jsp:include page="./../foot.jsp"/>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<style>
    #myTable th {
        white-space: nowrap;     /* 連続する半角スペース・タブ・改行を、1つの半角スペースとして表示 */
        overflow: hidden;        /* はみ出た部分を表示しない */
        text-overflow: ellipsis; /* はみ出た場合の表示方法(ellipsis:省略記号) */
    }
</style>

</body>

</html>
