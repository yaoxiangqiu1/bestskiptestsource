<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="../head.jsp"%>
<html lang="ja">
<head>
<script>
	var genkaShow = '${employeeRegisterForm.genkaShow}';
	$(document).ready(function() {
		$('#gekkyu').val(formatNumber($('#gekkyu').val()));
		$('#mitsumoriGenka').val(formatNumber($('#mitsumoriGenka').val()));

		$('#backBtn').click(function () {
			$('#completionForm').attr('action','${pageContext.servletContext.contextPath }'+'/employee/employeeFromComToRegEntry')
			$('#completionForm').submit();
		});

		// 就業関連処理
		showShugyoInfo();

		// 権限により給与表示設定
		if(genkaShow !== '1') {
			$('.genkaShow').css('display','none');
		}　else　{
			$('.genkaShow').css('display','');

			// 雇用形態関連処理
			showKoyokeitaiInfo();
			// 社員単価情報切替表示関連処理
			showTankaInfo();
		}
	});

	/**
	 * 就業関連処理
	 */
	function showShugyoInfo() {
		// 退職申し込みの場合
		if($('#shugyoJokyoFlag').val() === '2'
				|| ($('#shugyoJokyoHenkouFlag').val() === undefined && $('#taishokuDay').val() !== '')
				|| $('#shugyoJokyoHenkouFlag').val() === '2') {

			$('.changeInfo').each(function () {
				$(this).hide();
			});

			// 契約内容更新の場合
			if ($('input[name="keiyakuFlag"]:checked').val() === '2') {
				$('.shugyoInfo').hide();
				$('.changeTaishokuInfo').hide();

				$('#keiyakuInfo').collapse('show');
			} else if ($('input[name="keiyakuFlag"]:checked').val() === '1') {
				$('.shugyoInfo').show();
				$('.changeTaishokuInfo').show();

				$('#keiyakuInfo').collapse('show');
			} else {
				$('#keiyakuInfo').collapse('hide');
			}
		} else {

			$('.changeTaishokuInfo').hide();

			if ($('#mode').val() === '2') {
				// 契約内容更新の場合
				if ($('input[name="keiyakuFlag"]:checked').val() === '2') {
					$('.shugyoInfo').hide();
					$('.changeInfo').each(function () {
						$(this).hide();
					});

					$('#keiyakuInfo').collapse('show');

				} else if ($('input[name="keiyakuFlag"]:checked').val() === '1') {
					$('.shugyoInfo').show();
					$('.changeInfo').each(function () {
						$(this).show();
					});

					$('#keiyakuInfo').collapse('show');
				} else {
					$('#keiyakuInfo').collapse('hide');
				}
			}
		}
	}

	/**
	 * 雇用形態関連処理
	 */
	function showKoyokeitaiInfo() {
		var selectValue;
		// 個人事業主
		if($('#koyokeitai').val() ==='3') {

			selectValue = $('#koseinenkinFlag').val();
			$('#lblKoyokeitai').text("税金計算：");

			$('#koseinenkinFlag').empty();
			$('#koseinenkinFlag').append("<option value=''></option>");
			$('#koseinenkinFlag').append("<option value='1'>税込み</option>");
			$('#koseinenkinFlag').append("<option value='2'>税抜き</option>");

			$('#koseinenkinFlag').find("option[value='"+ selectValue +"']").attr("selected",true);

			$('.koyokeitaiInfo').show();

			// 正社員、契約社員
		} else if($('#koyokeitai').val() ==='1' || $('#koyokeitai').val() ==='2') {

			selectValue = $('#koseinenkinFlag').val();
			$('#lblKoyokeitai').text("厚生年金有無：");

			$('#koseinenkinFlag').empty();
			$('#koseinenkinFlag').append("<option value=''></option>");
			$('#koseinenkinFlag').append("<option value='1'>有</option>");
			$('#koseinenkinFlag').append("<option value='2'>無</option>");

			$('#koseinenkinFlag').find("option[value='"+ selectValue +"']").attr("selected",true);

			$('.koyokeitaiInfo').show();

		} else {
			$('.koyokeitaiInfo').hide();

		}
	}

	/**
	 * 社員単価情報切替表示関連処理
	 */
	function showTankaInfo() {

		// 時間単価の場合
		if ($('input[name="tankaKbn"]:checked').val() === '1') {
			$('#lblKyuyo').text("時給：");
			$('.getuTankaInfo').hide();
			$('.jikanTankaInfo').show();

			// 個人事業主
			if($('#koyokeitai').val() ==='3') {
				$('.kojinKyuyoInfo').show();
			}
		} else {
			$('#lblKyuyo').text("月給：");
			$('.jikanTankaInfo').hide();
			$('.getuTankaInfo').show();
		}

		if($('input[name="tankaKbn"]:checked').val() === '2'
				&& $('input[name="seisanFlag"]:checked').val() === '1') {
			$('#seisanShow').show();
		}else{
			$('#seisanShow').hide();
		}
	}
</script>
</head>
<body id="page-top">
	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<jsp:include page="./../side_bar.jsp" />
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<jsp:include page="./../top_bar.jsp" />
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					<h1 class="h3 mb-2 text-gray-800">社員基本情報登録完了</h1>
					<form:form id="completionForm" action="${pageContext.servletContext.contextPath }/employee/employeeCompletionEntry" modelAttribute="employeeRegisterForm" method="post">
						<h1 style="color: green">
							<spring:message code="com.bestskip.bussiness.web.app.employee.completion.message" />
						</h1>
						<div class="panel-body">
							<h5 class="mb-3 text-gray-900">社員基本情報</h5>

							<div class="card">
								<div class="card-header" role="tab" id="headingBase" style="display: none">
									<a class="text-body d-block p-3 m-n3" data-toggle="collapse" href="#baseEmpInfo" role="button" aria-expanded="true" aria-controls="baseEmpInfo">
									</a>
								</div><!-- /.card-header -->
								<div id="baseEmpInfo" class="collapse show" role="tabpanel" aria-labelledby="headingBase" data-parent="#accordion">
									<div class="card-body">
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label>入社日：</label>
												<form:input path="nyushaDay" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="nyushaDay" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >所属部署：</label>
												<form:select path="shozokuSoshikiCode" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
													<form:option value="" label=""/>
													<form:options items="${bushoCodeList}" />
												</form:select>
												<form:errors path="shozokuSoshikiCode" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row ">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label>社員姓：</label>
												<form:input path="seiKanji" type="text" class="form-control " placeholder="社員姓" maxlength="32" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="seiKanji" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label>社員名：</label>
												<form:input path="meiKanji" type="text" class=" form-control" placeholder="社員名" maxlength="64" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="meiKanji" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label>ひらがな（姓）：</label>
												<form:input path="seiKana" type="text" class="form-control" maxlength="64" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="seiKana" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label>ひらがな（名）：</label>
												<form:input path="meiKana" type="text" class=" form-control" maxlength="128" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="meiKana" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label>英文字（姓）：</label>
												<form:input path="seiEimoji" type="text" class="form-control " maxlength="64" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="seiEimoji" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >英文字（名）：</label>
												<form:input path="meiEimoji" type="text" class=" form-control" maxlength="128" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="meiEimoji" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label>性別：</label>
												<form:select path="sex" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
													<form:option value="" label=""/>
													<form:options items="${sexList}" />
												</form:select>
												<form:errors path="sex" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label>国籍：</label>
												<form:select path="kokusekiFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
													<form:option value="" label=""/>
													<form:options items="${kokusekiList}" />
												</form:select>
												<form:errors path="kokusekiFlag" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label for="sex">婚姻状況：</label>
												<form:select path="koninJokyoFlag" name="sex" id="sex" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
													<form:option value="" label=""/>
													<form:options items="${koninJokyoList }" />
												</form:select>
												<form:errors path="koninJokyoFlag" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >生年月日：</label>
												<form:input path="seinengappi" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="seinengappi" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label >住所：</label>
												<form:input path="address" type="text" maxlength="512" class="form-control " cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="address" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label >連絡先：</label>
												<form:input path="renrakusaki" type="text" maxlength="13" class="form-control " cssErrorClass="form-control is-invalid" placeholder="xx-xxxx-xxxx又はxxx-xxxx-xxxx" disabled="true"/>
												<form:errors path="renrakusaki" cssClass="errMsg" />
											</div>

											<div class="col-sm-6">
												<label >日本語力：</label>
												<form:select path="nihongoLevel" class="custom-select " cssErrorClass="form-control is-invalid" disabled="true">
													<form:options items="${nihongoLevelList}" />
												</form:select>
												<form:errors path="nihongoLevel" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-12 mb-3 mb-sm-0">
												<label>開発言語:</label>
												<form:checkboxes items="${gijutsuryokuList}" path="gijutsuryoku"  cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="gijutsuryoku" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label for="sex">学歴：</label>
												<form:select path="gakureki" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
													<form:option value="" label=""/>
													<form:options items="${gakurekiList }" />
												</form:select>
												<form:errors path="gakureki" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >専攻：</label>
												<form:input path="senko" type="text" class="form-control " maxlength="128" cssErrorClass="form-control is-invalid" disabled="true" />
												<form:errors path="senko" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label for="sex">最終卒業校：</label>
												<form:input path="saishuSotsugyoko" class="form-control" maxlength="128" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="saishuSotsugyoko" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >卒業年月：</label>
												<form:input path="sotsugyoYm" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="sotsugyoYm" cssClass="errMsg" />
											</div>
										</div>
										<div class=" form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label >在留カードNo：</label>
												<form:input path="zairyuCardNo" type="text" class="form-control " maxlength="12" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="zairyuCardNo" cssClass="errMsg" />
											</div>
											<div class="col-sm-6">
												<label >在留期限：</label>
												<form:input path="zairyuKigen" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="zairyuKigen" cssClass="errMsg" />
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label >扶養家族人数：</label>
												<form:input path="fuyokazokuNinzu" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="fuyokazokuNinzu" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row">
											<div class="col-sm-6 mb-3 mb-sm-0">
												<label >メールアドレス：</label>
												<form:input path="mailAddress" type="text" class="form-control" maxlength="128" cssErrorClass="form-control is-invalid" disabled="true"/>
												<form:errors path="mailAddress" cssClass="errMsg" />
											</div>
										</div>

										<div class="form-group row disabled">
											<label>ファイル（複数選択可）</label>
											<form:input path="uploadfileName" type="text" class="form-control" cssErrorClass="form-control is-invalid" disabled="true" />
										</div>
									</div><!-- /.card-body -->
								</div><!-- /.collapse -->
							</div><!-- /.card -->

							<h5 class="mt-3 mb-3 text-gray-900">社員雇用契約情報</h5>

							<c:if test="${shainKoyokeiyakuJohoList!= null && fn:length(shainKoyokeiyakuJohoList) != 0}">
								<table id="empTable" class="table table-bordered" style="width:100%">
									<thead class="thead-light">
									<tr>
										<th>枝番</th>
										<th>適用開始年月日</th>
										<th>適用終了年月日</th>
										<th>雇用形態</th>
										<th>役職</th>
										<th>職種</th>
										<th>就業状況</th>
										<th>単価区分</th>
										<th>月給</th>
										<th>計算原価</th>
										<th>厚生年金有無</th>
										<th>税金計算</th>
										<th>精算有無</th>
										<th>精算時間</th>
										<th>控除単価</th>
										<th>超過単価</th>
										<th>本人要望</th>
									</tr>
									</thead>
									<tbody>
									<c:forEach items="${shainKoyokeiyakuJohoList}" var="shainKoyokeiyakuJoho">
									<tr>
										<td>${shainKoyokeiyakuJoho.edaban}</td>
										<td>${shainKoyokeiyakuJoho.tekiyo_start_day}</td>
										<td>${shainKoyokeiyakuJoho.tekiyo_end_day}</td>
										<td>${shainKoyokeiyakuJoho.koyokeitai_name}</td>
										<td>${shainKoyokeiyakuJoho.yakushokumei}</td>
										<td>${shainKoyokeiyakuJoho.shokushuname}</td>
										<td>${shainKoyokeiyakuJoho.shugyoname}</td>
										<td>${shainKoyokeiyakuJoho.tanka_kbn_name}</td>
										<td>${shainKoyokeiyakuJoho.gekkyu}</td>
										<td>${shainKoyokeiyakuJoho.mitsumori_genka}</td>

										<c:if test="${shainKoyokeiyakuJoho.koyokeitai == '3' && shainKoyokeiyakuJoho.koseinenkin_flag == '1'}">
										<td>-</td>
										<td>税込み</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai == '3' && shainKoyokeiyakuJoho.koseinenkin_flag != '1'}">
										<td>-</td>
										<td>税抜き</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai != '3' && shainKoyokeiyakuJoho.koseinenkin_flag == '1'}">
										<td>有</td>
										<td>-</td>
										</c:if>
										<c:if test="${shainKoyokeiyakuJoho.koyokeitai != '3' && shainKoyokeiyakuJoho.koseinenkin_flag != '1'}">
										<td>無</td>
										<td>-</td>
										</c:if>
										<td>${shainKoyokeiyakuJoho.seisan_name}</td>
										<td>${shainKoyokeiyakuJoho.seisanjikan}</td>
										<td>${shainKoyokeiyakuJoho.seisan_kaitannka}</td>
										<td>${shainKoyokeiyakuJoho.seisan_jouitannka}</td>
										<td>${shainKoyokeiyakuJoho.yobo}</td>
									</tr>
									</c:forEach>
									</tbody>
								</table>
							</c:if>

							<c:if test="${employeeRegisterForm.mode == '2'}">
								<div class="bs-example">
									<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-1" value="0" disabled="true"/>
									<label class="radio-inline_label" for="item-1">
										変更無し
									</label>
									<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-2" value="2" disabled="true"/>
									<label class="radio-inline_label" for="item-2">
										登録ミスによる訂正
									</label>
									<form:radiobutton path="keiyakuFlag" class="radio-inline_input"  id="item-3" value="1" disabled="true"/>
									<label class="radio-inline_label" for="item-3">
										契約内容変更による期間再設定
									</label>
								</div>
							</c:if>

							<div class="accordion mb-3" id="accordion" role="tablist" aria-multiselectable="true">
								<div class="card">
									<div class="card-header" role="tab" id="headingOne" style="display: none">
										<a class="text-body d-block p-3 m-n3" data-toggle="collapse" href="#keiyakuInfo" role="button" aria-expanded="true" aria-controls="keiyakuInfo">
										</a>
									</div><!-- /.card-header -->
									<div id="keiyakuInfo" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
										<div class="card-body">

											<c:if test="${employeeRegisterForm.mode == '2'}">
												<div class="form-group row changeInfo">
													<div class="col-sm-2 mb-3 mb-sm-0 ">
														<label>変更前</label>
													</div>
												</div>
												<div class="form-group row changeInfo">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<label >適用開始日：</label>
														<form:input path="beforeTekiyoStartDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
														<form:errors path="beforeTekiyoStartDay" cssClass="errMsg" />
													</div>
													<div class="col-sm-6">
														<label >適用終了日：</label>
														<form:input path="beforeTekiyoEndDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
														<form:errors path="beforeTekiyoEndDay" cssClass="errMsg" />
													</div>
												</div>

												<div class="form-group row changeInfo">
													<div class="col-sm-2 mb-3 mb-sm-0">
														<label>変更後</label>
													</div>
												</div>
											</c:if>

											<div class="form-group row changeInfo">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label >適用開始日：</label>
													<form:input path="tekiyoStartDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
													<form:errors path="tekiyoStartDay" cssClass="errMsg" />
												</div>
												<div class="col-sm-6">
													<label >適用終了日：</label>
													<form:input path="tekiyoEndDay" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
													<form:errors path="tekiyoEndDay" cssClass="errMsg" />
												</div>
											</div>

											<div class="form-group row changeTaishokuInfo">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label>退職日：</label>
													<form:input path="taishokuDay" type="text" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
													<form:errors path="taishokuDay" cssClass="errMsg" />
												</div>
											</div>


											<c:if test="${employeeRegisterForm.mode == '2'}">
												<div class="form-group row shugyoInfo">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<label >現在就業状況：</label>
														<form:select path="shugyoJokyoFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
															<form:option value="" label=""/>
															<form:options items="${shugyoJokyoList}" />
														</form:select>
														<form:errors path="shugyoJokyoFlag" cssClass="errMsg" />
													</div>
													<div class="col-sm-6 mb-3 mb-sm-0">
														<label >就業状況変更：</label>
														<form:select path="shugyoJokyoHenkouFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
															<form:option value="" label=""/>
															<form:options items="${shugyoJokyoHenkouList}" />
														</form:select>
														<form:errors path="shugyoJokyoHenkouFlag" cssClass="errMsg" />
													</div>
												</div>
											</c:if>

											<div class="form-group row">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label >職種：</label>
													<form:select path="shokushuFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
														<form:option value="" label=""/>
														<form:options items="${shokushuList}" />
													</form:select>
													<form:errors path="shokushuFlag" cssClass="errMsg" />
												</div>
												<div class="col-sm-6">
													<label >役職：</label>
													<form:select path="yakushokuFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
														<form:option value="" label=""/>
														<form:options items="${yakushokuList}" />
													</form:select>
													<form:errors path="yakushokuFlag" cssClass="errMsg" />
												</div>
											</div>

											<div class="form-group row">
												<div class="col-sm-6 mb-3 mb-sm-0">
													<label >雇用形態：</label>
													<form:select path="koyokeitai" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
														<form:option value="" label=""/>
														<form:options items="${koyokeitaiList}" />
													</form:select>
													<form:errors path="koyokeitai" cssClass="errMsg" />
												</div>
												<div class="col-sm-6 koyokeitaiInfo genkaShow">
													<label id="lblKoyokeitai">厚生年金有無：</label>
													<form:select path="koseinenkinFlag" class="custom-select" cssErrorClass="form-control is-invalid" disabled="true">
														<form:option value="" label=""/>
														<form:option value="1" label="有" />
														<form:option value="2" label="無" />
													</form:select>
													<form:errors path="koseinenkinFlag" cssClass="errMsg" />
												</div>
											</div>
											<div class="genkaShow">
												<div class="form-group row">
													<div class="col-sm-2 mb-3 mb-sm-0">
														<label>単価：</label>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<form:radiobutton path="tankaKbn"  value="1"  label="時間単価" disabled="true"/>
														<form:radiobutton path="tankaKbn"  value="2"  label="月単価" disabled="true"/>
													</div>
												</div>

												<div class="form-group row getuTankaInfo kojinKyuyoInfo">
													<div class="col-sm-2">
														<label id="lblKyuyo">月給：</label>
													</div>
													<div class="col-sm-5 mb-3 mb-sm-0">
														<form:input path="gekkyu" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true" />
														<form:errors path="gekkyu" cssClass="errMsg" />
													</div>
												</div>

												<div class="form-group row">
													<div class="col-sm-2">
														<label >計算原価：</label>
													</div>
													<div class="col-sm-5 mb-3 mb-sm-0">
														<form:input path="mitsumoriGenka" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true" />
														<form:errors path="mitsumoriGenka" cssClass="errMsg" />
													</div>
													<div class="col-sm-1 getuTankaInfo">
														<span class="input-group-append">（円） 月</span>
													</div>
													<div class="col-sm-1 jikanTankaInfo">
														<span class="input-group-append">（円） 時間</span>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-sm-6 mb-3 mb-sm-0">
														<laber>精算有無：
														</laber>
														<form:radiobutton path="seisanFlag"  value="1"  label="有" disabled="true"/>
														&nbsp;&nbsp;&nbsp;
														<form:radiobutton path="seisanFlag"  value="2"  label="無" disabled="true"/>
													</div>
												</div>
												<div id="seisanShow">
													<div class="form-group row">
														<div class="col-sm-6 mb-3 mb-sm-0">
															<label >精算時間STR：</label>
															<form:input path="seisanjikanStart" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
															<form:errors path="seisanjikanStart" cssClass="errMsg" />
														</div>
														<div class="col-sm-6">
															<label >精算時間END：</label>
															<form:input path="seisanjikanEnd" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
															<form:errors path="seisanjikanEnd" cssClass="errMsg" />
														</div>
													</div>
													<div class="form-group row">
														<div class="col-sm-6 mb-3 mb-sm-0">
															<label >控除単価：</label>
															<form:input path="seisanKaitannka" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
															<form:errors path="seisanKaitannka" cssClass="errMsg" />
														</div>
														<div class="col-sm-6">
															<label >超過単価：</label>
															<form:input path="seisanJouitannka" type="text" class="form-control " cssErrorClass="form-control is-invalid" disabled="true"/>
															<form:errors path="seisanJouitannka" cssClass="errMsg" />
														</div>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<div class="col-sm-12 mb-3 mb-sm-0">
													<label>要望：</label>
													<form:textarea path="yobo" type="text" name="birthday" class="form-control datetimepicker-input" cssErrorClass="form-control is-invalid" disabled="true"/>
													<form:errors path="yobo" cssClass="errMsg" />
												</div>
											</div>

										</div><!-- /.card-body -->
									</div><!-- /.collapse -->
								</div><!-- /.card -->
							</div><!-- /#accordion -->

							<form:input path="koshinKaisu" type="hidden"/>
							<form:input path="koyouKoshinKaisu" type="hidden"/>
							<form:input path="shainNo" type="hidden"/>
							<form:input path="edaban" type="hidden"/>
							<form:input path="mode" type="hidden"/>
							<form:input path="genkaShow" type="hidden"/>
						</div>

					</form:form>


					<div class="form-group row loginbutton">
						<div class="col-sm-6 mb-3 m-sm-0">
							<a href="#" id="backBtn" class="btn btn-primary btn-user btn-block">TOP画面へ</a>
						</div>
						<div class="col-sm-6">
							<a href="#" class="btn btn-primary btn-user btn-block disabled">計画案件画面へ</a>
						</div>
					</div>

				</div>
				<!-- End of Main Content -->
			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<jsp:include page="./../foot.jsp" />
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->





</body>

</html>
